package net.ihe.gazelle.framework.modelvalidator.mock.polymorphism.structure;

import java.util.List;

public  abstract class AbstractParent {

    private String abstractParentAttr;
    private List<NestedAbstractParent> nestedList;
    private SecondChildOfAbstract secondChild;


    public String getAbstractParentAttr() {
        return abstractParentAttr;
    }

    public AbstractParent setAbstractParentAttr(String abstractParentAttr) {
        this.abstractParentAttr = abstractParentAttr;
        return this;
    }

    public SecondChildOfAbstract getSecondChild() {
        return secondChild;
    }

    public AbstractParent setSecondChild(SecondChildOfAbstract secondChild) {
        this.secondChild = secondChild;
        return this;
    }

    public List<NestedAbstractParent> getNestedList() {
        return nestedList;
    }

    public AbstractParent setNestedList(List<NestedAbstractParent> nestedList) {
        this.nestedList = nestedList;
        return this;
    }

    public boolean isParentAttrValid() {
        return abstractParentAttr != null;
    }

    public boolean isNestedListValid() {
        return nestedList != null && !nestedList.isEmpty();
    }

    public boolean isSecondChildValid() {
        return secondChild != null;
    }


}
