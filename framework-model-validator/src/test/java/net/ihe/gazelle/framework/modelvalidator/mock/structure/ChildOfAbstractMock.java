package net.ihe.gazelle.framework.modelvalidator.mock.structure;

public class ChildOfAbstractMock extends AbstractParentMock {

    private String childOfAbstractAttribute;

    public String getChildOfAbstractAttribute() {
        return childOfAbstractAttribute;
    }

    public void setChildOfAbstractAttribute(String childOfAbstractAttribute) {
        this.childOfAbstractAttribute = childOfAbstractAttribute;
    }

    public boolean isChildOfAbstractAttributeValid() {
        return childOfAbstractAttribute != null;
    }
}
