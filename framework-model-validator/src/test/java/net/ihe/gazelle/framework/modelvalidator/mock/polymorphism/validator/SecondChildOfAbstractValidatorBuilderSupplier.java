package net.ihe.gazelle.framework.modelvalidator.mock.polymorphism.validator;

import net.ihe.gazelle.framework.modelvalidator.domain.AbstractValidatorBuilderSupplier;
import net.ihe.gazelle.framework.modelvalidator.domain.ValidatorBuilder;
import net.ihe.gazelle.framework.modelvalidator.domain.ValidatorBuilderFactory;
import net.ihe.gazelle.framework.modelvalidator.mock.polymorphism.structure.SecondChildOfAbstract;

public class SecondChildOfAbstractValidatorBuilderSupplier extends AbstractValidatorBuilderSupplier<SecondChildOfAbstract> {

    public SecondChildOfAbstractValidatorBuilderSupplier(ValidatorBuilderFactory validatorBuilderFactory) {
        super(validatorBuilderFactory);
    }

    @Override
    protected Class<? extends SecondChildOfAbstract> getType() {
        return SecondChildOfAbstract.class;
    }

    @Override
    protected ValidatorBuilder<SecondChildOfAbstract> supplyRulesAndMembers(ValidatorBuilder<SecondChildOfAbstract> validatorBuilder) {
        return validatorBuilder
                .addRule("secondChildAttrValid", SecondChildOfAbstract::isSecondChildAttrValid, "secondChildAttr must be valid");
    }
}
