package net.ihe.gazelle.framework.modelvalidator.mock.validator;

import net.ihe.gazelle.framework.modelvalidator.domain.AbstractValidatorBuilderSupplier;
import net.ihe.gazelle.framework.modelvalidator.domain.ValidatorBuilder;
import net.ihe.gazelle.framework.modelvalidator.domain.ValidatorBuilderFactory;
import net.ihe.gazelle.framework.modelvalidator.mock.structure.NestedMockOne;
import net.ihe.gazelle.framework.modelvalidator.mock.structure.NestedMockTwo;

public class NestedMockOneValidatorBuilderSupplier extends AbstractValidatorBuilderSupplier<NestedMockOne> {

    public NestedMockOneValidatorBuilderSupplier(ValidatorBuilderFactory validatorBuilderFactory) {
        super(validatorBuilderFactory);
    }

    @Override
    protected Class<? extends NestedMockOne> getType() {
        return NestedMockOne.class;
    }

    @Override
    protected ValidatorBuilder<NestedMockOne> supplyRulesAndMembers(ValidatorBuilder<NestedMockOne> validatorBuilder) {
        return validatorBuilder
                .addRule("oneAttributeValid", NestedMockOne::isOneAttrValid, "One attribute must be valid")
                .addMember("nestedMockOneFromOne", NestedMockOne::getNestedMockOneFromOne, NestedMockOne.class)
                .addMember("nestedMockTwoFromOne", NestedMockOne::getNestedMockTwoFromOne, NestedMockTwo.class)
                ;
    }
}
