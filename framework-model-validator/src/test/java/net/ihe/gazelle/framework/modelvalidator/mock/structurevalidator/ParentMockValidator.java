package net.ihe.gazelle.framework.modelvalidator.mock.structurevalidator;

import net.ihe.gazelle.framework.modelvalidator.domain.RootValidatorBuilder;
import net.ihe.gazelle.framework.modelvalidator.domain.StructureValidator;
import net.ihe.gazelle.framework.modelvalidator.domain.Validator;
import net.ihe.gazelle.framework.modelvalidator.domain.ValidatorBuilderFactory;
import net.ihe.gazelle.framework.modelvalidator.domain.result.ObjectResult;
import net.ihe.gazelle.framework.modelvalidator.mock.builder.visitors.ParentMockBuilderVisitor;
import net.ihe.gazelle.framework.modelvalidator.mock.structure.ParentMock;

public class ParentMockValidator implements StructureValidator<ParentMock> {

    private final Validator<ParentMock> validator;

    public ParentMockValidator(ValidatorBuilderFactory validatorBuilderFactory) {
        RootValidatorBuilder<ParentMock> rootValidatorBuilder = new RootValidatorBuilder<>(ParentMock.class);
        ParentMockBuilderVisitor.INSTANCE.visit(rootValidatorBuilder, validatorBuilderFactory);
        validator = rootValidatorBuilder.build();
    }

    @Override
    public ObjectResult validate(ParentMock object) {
        return validator.validate(object);
    }

    @Override
    public void assertValid(ParentMock object) {
        validator.assertValid(object);
    }

    @Override
    public boolean isValid(ParentMock object) {
        return validator.isValid(object);
    }
}
