package net.ihe.gazelle.framework.modelvalidator.mock.polymorphism.structure;


public abstract class NestedAbstractParent{

    private String nestedParentAttr;

    public String getNestedParentAttr() {
        return nestedParentAttr;
    }

    public NestedAbstractParent setNestedParentAttr(String nestedParentAttr) {
        this.nestedParentAttr = nestedParentAttr;
        return this;
    }

    public boolean isNestedParentAttrValid() {
        return nestedParentAttr != null;
    }


}
