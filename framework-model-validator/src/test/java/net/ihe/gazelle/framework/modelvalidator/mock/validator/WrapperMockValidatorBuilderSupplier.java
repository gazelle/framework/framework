package net.ihe.gazelle.framework.modelvalidator.mock.validator;

import net.ihe.gazelle.framework.modelvalidator.domain.AbstractValidatorBuilderSupplier;
import net.ihe.gazelle.framework.modelvalidator.domain.ValidatorBuilder;
import net.ihe.gazelle.framework.modelvalidator.domain.ValidatorBuilderFactory;
import net.ihe.gazelle.framework.modelvalidator.mock.structure.*;

public class WrapperMockValidatorBuilderSupplier extends AbstractValidatorBuilderSupplier<WrapperMock> {

    public WrapperMockValidatorBuilderSupplier(ValidatorBuilderFactory validatorBuilderFactory) {
        super(validatorBuilderFactory);
    }

    @Override
    protected Class<? extends WrapperMock> getType() {
        return WrapperMock.class;
    }

    @Override
    protected ValidatorBuilder<WrapperMock> supplyRulesAndMembers(ValidatorBuilder<WrapperMock> validatorBuilder) {
        return validatorBuilder
                .addRule("parentMocksValid", WrapperMock::isParentMocksValid, "Wrapper attribute must be valid")
                .addRule("subElementMock", WrapperMock::isSubElementAttributeValid, "Wrapper attribute must be valid")
                .addMember("subElementMock", WrapperMock::getSubElementMock, SubElementMock.class)
                .addMember("parentMocks", WrapperMock::getParentMocks, ParentMock.class)
                ;
    }
}
