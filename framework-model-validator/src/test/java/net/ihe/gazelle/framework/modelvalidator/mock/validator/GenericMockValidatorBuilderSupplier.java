package net.ihe.gazelle.framework.modelvalidator.mock.validator;

import net.ihe.gazelle.framework.modelvalidator.domain.AbstractValidatorBuilderSupplier;
import net.ihe.gazelle.framework.modelvalidator.domain.ValidatorBuilder;
import net.ihe.gazelle.framework.modelvalidator.domain.ValidatorBuilderFactory;
import net.ihe.gazelle.framework.modelvalidator.mock.structure.GenericMock;

public class GenericMockValidatorBuilderSupplier extends AbstractValidatorBuilderSupplier<GenericMock> {

    public GenericMockValidatorBuilderSupplier(ValidatorBuilderFactory validatorBuilderFactory) {
        super(validatorBuilderFactory);
    }

    @Override
    protected Class<? extends GenericMock> getType() {
        return GenericMock.class;
    }

    @Override
    protected ValidatorBuilder<GenericMock> supplyRulesAndMembers(ValidatorBuilder<GenericMock> validatorBuilder) {
        return validatorBuilder
                .addRule("genericAttributeValid", GenericMock::isGenericAttributeValid, "Generic attribute must be valid");
    }
}
