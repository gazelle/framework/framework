package net.ihe.gazelle.framework.modelvalidator.mock.validator;

import net.ihe.gazelle.framework.modelvalidator.domain.AbstractValidatorBuilderSupplier;
import net.ihe.gazelle.framework.modelvalidator.domain.ValidatorBuilder;
import net.ihe.gazelle.framework.modelvalidator.domain.ValidatorBuilderFactory;
import net.ihe.gazelle.framework.modelvalidator.mock.structure.AbstractParentMock;

public class AbstractParentMockValidatorBuilderSupplier extends AbstractValidatorBuilderSupplier<AbstractParentMock> {

    public AbstractParentMockValidatorBuilderSupplier(ValidatorBuilderFactory validatorBuilderFactory) {
        super(validatorBuilderFactory);
    }

    @Override
    protected Class<? extends AbstractParentMock> getType() {
        return AbstractParentMock.class;
    }

    @Override
    protected ValidatorBuilder<AbstractParentMock> supplyRulesAndMembers(ValidatorBuilder<AbstractParentMock> validatorBuilder) {
        return validatorBuilder
                .addRule("parentAttributeValid", AbstractParentMock::isAbstractParentAttributeValid, "Parent attribute must be valid");
    }
}
