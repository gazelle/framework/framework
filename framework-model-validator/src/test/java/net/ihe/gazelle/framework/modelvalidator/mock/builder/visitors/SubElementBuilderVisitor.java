package net.ihe.gazelle.framework.modelvalidator.mock.builder.visitors;

import net.ihe.gazelle.framework.modelvalidator.domain.BuilderVisitor;
import net.ihe.gazelle.framework.modelvalidator.domain.RootValidatorBuilder;
import net.ihe.gazelle.framework.modelvalidator.domain.ValidatorBuilderFactory;
import net.ihe.gazelle.framework.modelvalidator.mock.structure.SubElementMock;
import net.ihe.gazelle.framework.modelvalidator.mock.validator.SubElementMockValidatorBuilderSupplier;

public class SubElementBuilderVisitor implements BuilderVisitor {

    public static SubElementBuilderVisitor INSTANCE = new SubElementBuilderVisitor();

    @Override
    public void visit(RootValidatorBuilder<?> rootValidatorBuilder, ValidatorBuilderFactory validatorBuilderFactory) {
        rootValidatorBuilder.addValidatorBuilder(SubElementMock.class, new SubElementMockValidatorBuilderSupplier(validatorBuilderFactory));
    }
}
