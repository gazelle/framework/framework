package net.ihe.gazelle.framework.modelvalidator.mock.factory;

import net.ihe.gazelle.framework.modelvalidator.application.StructureValidatorFactory;
import net.ihe.gazelle.framework.modelvalidator.domain.StructureValidator;
import net.ihe.gazelle.framework.modelvalidator.interlay.adapter.BValidatorBuilderFactory;
import net.ihe.gazelle.framework.modelvalidator.mock.structure.ParentMock;
import net.ihe.gazelle.framework.modelvalidator.mock.structurevalidator.ParentMockValidator;

public class ParentMockJsonFactory implements StructureValidatorFactory<ParentMock>{

    @Override
    public StructureValidator<ParentMock> createStructureValidator() {
        return new ParentMockValidator(new BValidatorBuilderFactory());
    }

    @Override
    public Class<ParentMock> getType() {
        return ParentMock.class;
    }

    @Override
    public String getName() {
        return "parentMock";
    }
}
