package net.ihe.gazelle.framework.modelvalidator.mock.structure;

public class AbstractParentMock {

    private String abstractParentAttribute;

    public AbstractParentMock setAbstractParentAttribute(String abstractParentAttribute) {
        this.abstractParentAttribute = abstractParentAttribute;
        return this;
    }

    public String getAbstractParentAttribute() {
        return abstractParentAttribute;
    }

    public boolean isAbstractParentAttributeValid() {
        return abstractParentAttribute != null;
    }
}
