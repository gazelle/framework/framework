package net.ihe.gazelle.framework.modelvalidator.mock.builder;

import net.ihe.gazelle.framework.modelvalidator.domain.AbstractBuilder;
import net.ihe.gazelle.framework.modelvalidator.domain.BuilderVisitor;
import net.ihe.gazelle.framework.modelvalidator.domain.ValidatorBuilderFactory;
import net.ihe.gazelle.framework.modelvalidator.mock.builder.visitors.NestedMockTwoBuilderVisitor;
import net.ihe.gazelle.framework.modelvalidator.mock.structure.NestedMockOne;
import net.ihe.gazelle.framework.modelvalidator.mock.structure.NestedMockTwo;

public class NestedMockTwoBuilder extends AbstractBuilder<NestedMockTwo> {


    private NestedMockOne nestedMockOneFromTwo;

    private NestedMockTwo nestedMockTwoFromTwo;

    private String twoAttr;

    public NestedMockTwoBuilder(ValidatorBuilderFactory validatorBuilderFactory) {
        super(validatorBuilderFactory);
    }


    @Override
    protected BuilderVisitor getBuilderVisitor() {
        return NestedMockTwoBuilderVisitor.INSTANCE;
    }

    @Override
    protected Class<? extends NestedMockTwo> getParameterClass() {
        return NestedMockTwo.class;
    }

    public NestedMockTwoBuilder setNestedMockOneFromTwo(NestedMockOne nestedMockOneFromTwo) {
        this.nestedMockOneFromTwo = nestedMockOneFromTwo;
        return this;
    }

    public NestedMockTwoBuilder setNestedMockTwoFromTwo(NestedMockTwo nestedMockTwoFromTwo) {
        this.nestedMockTwoFromTwo = nestedMockTwoFromTwo;
        return this;
    }

    public NestedMockTwoBuilder setTwoAttr(String twoAttr) {
        this.twoAttr = twoAttr;
        return this;
    }


    @Override
    protected void make() {
        super.getObjectToBuild()
                .setNestedMockOneFromTwo(nestedMockOneFromTwo)
                .setNestedMockTwoFromTwo(nestedMockTwoFromTwo)
                .setTwoAttr(twoAttr)
                ;
    }
}
