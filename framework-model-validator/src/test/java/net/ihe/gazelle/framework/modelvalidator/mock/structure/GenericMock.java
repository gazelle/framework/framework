package net.ihe.gazelle.framework.modelvalidator.mock.structure;

public class GenericMock<T> {

    T attr;

    public T getAttr() {
        return attr;
    }

    public GenericMock<T> setAttr(T attr) {
        this.attr =  attr;
        return this;
    }

    public boolean isGenericAttributeValid() {
        return attr != null;
    }
}
