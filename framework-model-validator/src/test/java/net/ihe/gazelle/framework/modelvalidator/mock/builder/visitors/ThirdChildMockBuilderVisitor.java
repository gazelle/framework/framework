package net.ihe.gazelle.framework.modelvalidator.mock.builder.visitors;

import net.ihe.gazelle.framework.modelvalidator.domain.BuilderVisitor;
import net.ihe.gazelle.framework.modelvalidator.domain.RootValidatorBuilder;
import net.ihe.gazelle.framework.modelvalidator.domain.ValidatorBuilderFactory;
import net.ihe.gazelle.framework.modelvalidator.mock.structure.ThirdChildMock;
import net.ihe.gazelle.framework.modelvalidator.mock.validator.ThirdChildMockValidatorBuilderSupplier;

public class ThirdChildMockBuilderVisitor extends SecondChildMockBuilderVisitor {

    public static ThirdChildMockBuilderVisitor INSTANCE = new ThirdChildMockBuilderVisitor();

    @Override
    public void visit(RootValidatorBuilder<?> rootValidatorBuilder, ValidatorBuilderFactory validatorBuilderFactory) {
        super.visit(rootValidatorBuilder, validatorBuilderFactory);
        rootValidatorBuilder.addValidatorBuilder(ThirdChildMock.class, new ThirdChildMockValidatorBuilderSupplier(validatorBuilderFactory));
    }
}
