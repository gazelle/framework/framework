package net.ihe.gazelle.framework.modelvalidator.mock.builder;

import net.ihe.gazelle.framework.modelvalidator.domain.AbstractBuilder;
import net.ihe.gazelle.framework.modelvalidator.domain.BuilderVisitor;
import net.ihe.gazelle.framework.modelvalidator.domain.ValidatorBuilderFactory;
import net.ihe.gazelle.framework.modelvalidator.mock.builder.visitors.ParentMockBuilderVisitor;
import net.ihe.gazelle.framework.modelvalidator.mock.structure.ParentMock;

public class ParentMockBuilder extends AbstractBuilder<ParentMock> {

    protected String parentAttribute;


    public ParentMockBuilder(ValidatorBuilderFactory validatorBuilderFactory) {
        super(validatorBuilderFactory);
    }

    @Override
    protected Class<? extends ParentMock> getParameterClass() {
        return ParentMock.class;
    }


    @Override
    protected BuilderVisitor getBuilderVisitor() {
        return ParentMockBuilderVisitor.INSTANCE;
    }

    public ParentMockBuilder setParentAttribute(String parentAttribute) {
        this.parentAttribute = parentAttribute;
        return this;
    }


    protected void make() {
        getObjectToBuild()
                .setParentAttribute(parentAttribute);
    }

}