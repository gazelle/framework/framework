package net.ihe.gazelle.framework.modelvalidator.mock.extension;

import net.ihe.gazelle.framework.modelvalidator.domain.ValidatorBuilder;
import net.ihe.gazelle.framework.modelvalidator.domain.ValidatorBuilderFactory;
import net.ihe.gazelle.framework.modelvalidator.mock.structure.ParentMock;
import net.ihe.gazelle.framework.modelvalidator.mock.validator.ParentMockValidatorBuilderSupplier;

public class FourthChildMockValidatorBuilderSupplier extends ParentMockValidatorBuilderSupplier {

    public FourthChildMockValidatorBuilderSupplier(ValidatorBuilderFactory validatorBuilderFactory) {
        super(validatorBuilderFactory);
    }

    @Override
    protected Class<? extends ParentMock> getType() {
        return FourthChildMock.class;
    }

    @Override
    protected ValidatorBuilder<ParentMock> supplyRulesAndMembers(ValidatorBuilder<ParentMock> validatorBuilder) {
        return super.supplyRulesAndMembers(validatorBuilder)
                .addRule("fourthChildAttributeValid", fcm -> ((FourthChildMock) fcm).isFourthChildAttributeValid(), "Fourth child attribute must be valid")
                ;
    }
}
