package net.ihe.gazelle.framework.modelvalidator.mock.validator;

import net.ihe.gazelle.framework.modelvalidator.domain.ValidatorBuilder;
import net.ihe.gazelle.framework.modelvalidator.domain.ValidatorBuilderFactory;
import net.ihe.gazelle.framework.modelvalidator.mock.structure.AbstractParentMock;
import net.ihe.gazelle.framework.modelvalidator.mock.structure.ChildOfAbstractMock;

public class ChildOfAbstractMockValidatorBuilderSupplier extends AbstractParentMockValidatorBuilderSupplier {

    public ChildOfAbstractMockValidatorBuilderSupplier(ValidatorBuilderFactory validatorBuilderFactory) {
        super(validatorBuilderFactory);
    }

    @Override
    protected Class<? extends AbstractParentMock> getType() {
        return ChildOfAbstractMock.class;
    }

    @Override
    protected ValidatorBuilder<AbstractParentMock> supplyRulesAndMembers(ValidatorBuilder<AbstractParentMock> validatorBuilder) {
        return super
                .supplyRulesAndMembers(validatorBuilder)
                .addRule("childAttributeValid", c -> ((ChildOfAbstractMock) c).isChildOfAbstractAttributeValid(), "Child attribute must be valid");
    }

}
