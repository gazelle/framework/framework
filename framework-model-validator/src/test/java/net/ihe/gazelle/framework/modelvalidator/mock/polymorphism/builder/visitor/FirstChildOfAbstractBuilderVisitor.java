package net.ihe.gazelle.framework.modelvalidator.mock.polymorphism.builder.visitor;

import net.ihe.gazelle.framework.modelvalidator.domain.RootValidatorBuilder;
import net.ihe.gazelle.framework.modelvalidator.domain.ValidatorBuilderFactory;
import net.ihe.gazelle.framework.modelvalidator.mock.polymorphism.structure.FirstChildOfAbstract;
import net.ihe.gazelle.framework.modelvalidator.mock.polymorphism.validator.FirstChildOfAbstractValidatorBuilderSupplier;

public class FirstChildOfAbstractBuilderVisitor extends AbstractParentBuilderVisitor {

    public static FirstChildOfAbstractBuilderVisitor INSTANCE = new FirstChildOfAbstractBuilderVisitor();

    @Override
    public void visit(RootValidatorBuilder<?> rootValidatorBuilder, ValidatorBuilderFactory validatorBuilderFactory) {
        super.visit(rootValidatorBuilder, validatorBuilderFactory);
        rootValidatorBuilder.addValidatorBuilder(FirstChildOfAbstract.class, new FirstChildOfAbstractValidatorBuilderSupplier(validatorBuilderFactory));
    }
}
