package net.ihe.gazelle.framework.modelvalidator.domain;

import net.ihe.gazelle.framework.modelvalidator.domain.StructureValidator;
import net.ihe.gazelle.framework.modelvalidator.domain.ValidationException;
import net.ihe.gazelle.framework.modelvalidator.domain.ValidatorBuilderFactory;
import net.ihe.gazelle.framework.modelvalidator.domain.result.ObjectResult;
import net.ihe.gazelle.framework.modelvalidator.domain.result.RuleResult;
import net.ihe.gazelle.framework.modelvalidator.interlay.adapter.BValidatorBuilderFactory;
import net.ihe.gazelle.framework.modelvalidator.mock.structure.FirstChildMock;
import net.ihe.gazelle.framework.modelvalidator.mock.structure.ParentMock;
import net.ihe.gazelle.framework.modelvalidator.mock.structure.SubElementMock;
import net.ihe.gazelle.framework.modelvalidator.mock.structurevalidator.FirstChildMockValidator;
import net.ihe.gazelle.framework.modelvalidator.mock.structurevalidator.ParentMockValidator;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class StructureValidatorTest {

    private final ValidatorBuilderFactory validatorBuilderFactory = new BValidatorBuilderFactory();

    private final StructureValidator<ParentMock> parentStructureValidator = new ParentMockValidator(validatorBuilderFactory);

    private final StructureValidator<FirstChildMock> firstChildStructureValidator = new FirstChildMockValidator(validatorBuilderFactory);

    @Test
    public void testValidateValid() {
        ObjectResult objectResult = parentStructureValidator.validate(
                new ParentMock()
                        .setParentAttribute("parentAttribute")
        );
        assertTrue(objectResult.isValid());
        assertEquals("ParentMock [parentAttributeValid] Parent attribute must be valid => valid\n", objectResult.toString());
        assertEquals(1, objectResult.getRuleResults().size());
        assertEquals("parentAttributeValid", objectResult.getRuleResults().get(0).getId());
        assertEquals("Parent attribute must be valid", objectResult.getRuleResults().get(0).getDescription());
        assertTrue(objectResult.getRuleResults().get(0).isValid());
    }

    @Test
    public void testValidateInvalid() {
        ObjectResult objectResult = parentStructureValidator.validate(
                new ParentMock()
                        .setParentAttribute(null)
        );
        assertFalse(objectResult.isValid());
        assertEquals("ParentMock [parentAttributeValid] Parent attribute must be valid => invalid\n", objectResult.toString());
    }

    @Test
    public void testAssertValid() {
        assertDoesNotThrow(() -> parentStructureValidator.assertValid(
                new ParentMock()
                        .setParentAttribute("parentAttribute")
        ));
    }

    @Test
    public void testAssertInvalid() {
        assertThrows(ValidationException.class, () -> parentStructureValidator.assertValid(
                new ParentMock()
                        .setParentAttribute(null)
        ));
    }

    @Test
    public void testAssertIsValid() {
        assertTrue(parentStructureValidator.isValid(
                new ParentMock()
                        .setParentAttribute("parentAttribute")
        ));
    }

    @Test
    public void testAssertIsInvalid() {
        assertFalse(parentStructureValidator.isValid(
                new ParentMock()
                        .setParentAttribute(null)
        ));
    }

    @Test
    public void testValidateComposed(){
        ObjectResult result = firstChildStructureValidator.validate(
                new FirstChildMock()
                        .setParentAttribute("parentAttribute")
                        .setFirstChildAttribute("firstChildAttribute")
                        .setSubElementMock(new SubElementMock()
                                .setSubElementAttribute("subElementAttribute")
                        )
        );
        assertTrue(result.isValid());
        assertEquals("FirstChildMock [parentAttributeValid] Parent attribute must be valid => valid\n" +
                "FirstChildMock [firstChildAttributeValid] First child attribute must be valid => valid\n" +
                "FirstChildMock [subElementMockValid] SubElementMock must be valid => valid\n" +
                "FirstChildMock.subElementMock [subElementAttributeValid] SubElement attribute must be valid => valid\n", result.toString());

    }

    @Test
    public void testValidateComposedInvalid(){
        ObjectResult result = firstChildStructureValidator.validate(
                new FirstChildMock()
                        .setParentAttribute(null)
                        .setFirstChildAttribute("firstChildAttribute")
                        .setSubElementMock(null)
        );
        assertFalse(result.isValid());
        assertEquals("FirstChildMock [parentAttributeValid] Parent attribute must be valid => invalid\n" +
                "FirstChildMock [firstChildAttributeValid] First child attribute must be valid => valid\n" +
                "FirstChildMock [subElementMockValid] SubElementMock must be valid => invalid\n", result.toString());

    }

    @Test
    public void testValidateComposedInvalid2(){
        ObjectResult result = firstChildStructureValidator.validate(
                new FirstChildMock()
                        .setParentAttribute("parentAttribute")
                        .setFirstChildAttribute("firstChildAttribute")
                        .setSubElementMock(new SubElementMock()
                                .setSubElementAttribute(null)
                        )
        );
        assertFalse(result.isValid());
        assertEquals("FirstChildMock [parentAttributeValid] Parent attribute must be valid => valid\n" +
                "FirstChildMock [firstChildAttributeValid] First child attribute must be valid => valid\n" +
                "FirstChildMock [subElementMockValid] SubElementMock must be valid => valid\n" +
                "FirstChildMock.subElementMock [subElementAttributeValid] SubElement attribute must be valid => invalid\n", result.toString());
    }


}
