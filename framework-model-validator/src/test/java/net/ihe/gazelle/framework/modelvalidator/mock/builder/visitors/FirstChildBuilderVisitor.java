package net.ihe.gazelle.framework.modelvalidator.mock.builder.visitors;

import net.ihe.gazelle.framework.modelvalidator.domain.BuilderVisitor;
import net.ihe.gazelle.framework.modelvalidator.domain.RootValidatorBuilder;
import net.ihe.gazelle.framework.modelvalidator.domain.ValidatorBuilderFactory;
import net.ihe.gazelle.framework.modelvalidator.mock.structure.FirstChildMock;
import net.ihe.gazelle.framework.modelvalidator.mock.validator.FirstChildMockValidatorBuilderSupplier;

public class FirstChildBuilderVisitor extends ParentMockBuilderVisitor {

    public static FirstChildBuilderVisitor INSTANCE = new FirstChildBuilderVisitor();

    @Override
    public void visit(RootValidatorBuilder<?> rootValidatorBuilder, ValidatorBuilderFactory validatorBuilderFactory) {
        super.visit(rootValidatorBuilder, validatorBuilderFactory);
        rootValidatorBuilder.addValidatorBuilder(FirstChildMock.class, new FirstChildMockValidatorBuilderSupplier(validatorBuilderFactory));
        SubElementBuilderVisitor.INSTANCE.visit(rootValidatorBuilder, validatorBuilderFactory);
    }
}
