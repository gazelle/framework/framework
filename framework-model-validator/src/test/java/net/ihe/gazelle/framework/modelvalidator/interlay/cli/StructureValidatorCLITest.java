package net.ihe.gazelle.framework.modelvalidator.interlay.cli;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.kohsuke.args4j.CmdLineException;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.net.URL;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

import static org.awaitility.Awaitility.await;
import static org.junit.jupiter.api.Assertions.*;


public class StructureValidatorCLITest {

    private Logger logger = Logger.getLogger(StructureValidatorCLITest.class.getName());

    private ByteArrayOutputStream baos;

    @BeforeEach
    public void setUp() {
        baos = new ByteArrayOutputStream();
        PrintStream customOut = new PrintStream(baos);
        System.setOut(customOut);

    }

    @Test
    public void testWithFile() {
        URL url = StructureValidatorCLITest.class.getClassLoader().getResource("parent-mock.json");
        String path = url.getPath();
        new StructureValidatorCLI().execute(new String[]{"-f", path, "-n", "parentMock"});
        await()
                .atMost(10, TimeUnit.SECONDS)
                .until(() -> baos.toString().contains("Validation result"));
        assertEquals("Validation result: \u001B[32mVALID\u001B[0m\n" +
                "ParentMock [parentAttributeValid] Parent attribute must be valid => valid\n" +
                "\n", baos.toString());
    }

    @Test
    public void testWithStdin(){
        String json = "{\"parentAttribute\": \"attribute\"}";
        System.setIn(new java.io.ByteArrayInputStream(json.getBytes()));
        new StructureValidatorCLI().execute(new String[]{"-n", "parentMock"});
        await()
                .atMost(10, TimeUnit.SECONDS)
                .until(() -> baos.toString().contains("Validation result"));
        assertEquals("Validation result: \u001B[32mVALID\u001B[0m\n" +
                "ParentMock [parentAttributeValid] Parent attribute must be valid => valid\n" +
                "\n", baos.toString());
    }

    @Test
    public void testWithNoSource() {
        new StructureValidatorCLI().execute(new String[]{"-n", "parentMock"});
        await()
                .atMost(10, TimeUnit.SECONDS)
                .until(() -> baos.toString().contains("No structure provided in STDIN"));

        assertEquals("\u001B[31mNo structure provided in STDIN\u001B[0m\n" +
                " --file (-f) VAL : Absolute path to validation report to validate\n" +
                " --help (-h)     : print this message\n" +
                " --name (-n) VAL : The name of the structure to validate if the class name was\n" +
                "                   not provided\n" +
                " --type (-t) VAL : The full canonical name of the structure to validate\n", baos.toString());
    }

    @Test
    public void testWithFullClassName(){
        String json = "{\"parentAttribute\": \"attribute\"}";
        System.setIn(new java.io.ByteArrayInputStream(json.getBytes()));
        new StructureValidatorCLI().execute(new String[]{"-t", "net.ihe.gazelle.framework.modelvalidator.mock.structure.ParentMock"});
        await()
                .atMost(10, TimeUnit.SECONDS)
                .until(() -> baos.toString().contains("Validation result"));
        assertEquals("Validation result: \u001B[32mVALID\u001B[0m\n" +
                "ParentMock [parentAttributeValid] Parent attribute must be valid => valid\n" +
                "\n", baos.toString());
    }

    @Test
    public void testWithNoType(){
        String json = "{\"parentAttribute\": \"attribute\"}";
        System.setIn(new java.io.ByteArrayInputStream(json.getBytes()));
        new StructureValidatorCLI().execute(new String[]{});
        await()
                .atMost(10, TimeUnit.SECONDS)
                .until(() -> baos.toString().contains("No structure type provided"));

        assertEquals("\u001B[31mNo structure type provided\u001B[0m\n" +
                " --file (-f) VAL : Absolute path to validation report to validate\n" +
                " --help (-h)     : print this message\n" +
                " --name (-n) VAL : The name of the structure to validate if the class name was\n" +
                "                   not provided\n" +
                " --type (-t) VAL : The full canonical name of the structure to validate\n", baos.toString());
    }

    @Test
    public void testWithWrongOption(){

        Throwable throwable = assertThrows(RuntimeException.class, () ->new StructureValidatorCLI().execute(new String[]{"-x", "net.ihe.gazelle.framework.modelvalidator.mock.structure.ParentMock"}));
        assertEquals("org.kohsuke.args4j.CmdLineException: \"-x\" is not a valid option", throwable.getMessage());
        assertInstanceOf(CmdLineException.class, throwable.getCause());

    }

    @Test
    public void testUnknownName(){
        String json = "{\"parentAttribute\": \"attribute\"}";
        System.setIn(new java.io.ByteArrayInputStream(json.getBytes()));
        Throwable throwable = assertThrows(RuntimeException.class , ()->new StructureValidatorCLI().execute(new String[]{"-n", "unknownName"}));
        assertEquals("java.lang.RuntimeException: java.lang.IllegalArgumentException:" +
                " No StructureValidatorFactory found with name unknownName", throwable.toString());
    }

    @Test
    public void testUnknownType(){
        String json = "{\"parentAttribute\": \"attribute\"}";
        System.setIn(new java.io.ByteArrayInputStream(json.getBytes()));
        Throwable throwable = assertThrows(RuntimeException.class , ()->new StructureValidatorCLI().execute(new String[]{"-t", "unknownType"}));
        assertEquals("java.lang.RuntimeException: java.lang.ClassNotFoundException: unknownType", throwable.toString());
    }

    @Test
    public void testHelp(){
        new StructureValidatorCLI().execute(new String[]{"-h"});
        await()
                .atMost(10, TimeUnit.SECONDS)
                .until(() -> baos.toString().contains(" --file (-f) VAL : Absolute path to validation report to validate\n" +
                        " --help (-h)     : print this message\n" +
                        " --name (-n) VAL : The name of the structure to validate if the class name was\n" +
                        "                   not provided\n" +
                        " --type (-t) VAL : The full canonical name of the structure to validate\n"));
    }

    @Test
    public void testWithMain(){
        String json = "{\"parentAttribute\": \"attribute\"}";
        System.setIn(new java.io.ByteArrayInputStream(json.getBytes()));
        StructureValidatorCLI.main(new String[]{"-n", "parentMock"});
        await()
                .atMost(10, TimeUnit.SECONDS)
                .until(() -> baos.toString().contains("Validation result"));
        assertEquals("Validation result: \u001B[32mVALID\u001B[0m\n" +
                "ParentMock [parentAttributeValid] Parent attribute must be valid => valid\n" +
                "\n", baos.toString());
    }

    @Test
    public void testNotExistFactory(){
       Throwable throwable = assertThrows(RuntimeException.class,() -> new StructureValidatorCLI().execute(new String[]{"-t", "net.ihe.gazelle.framework.modelvalidator.mock.structure.SecondChildMock"}));
       assertEquals("No StructureValidatorFactory found for type class net.ihe.gazelle.framework.modelvalidator.mock.structure.SecondChildMock", throwable.getCause().getMessage());



    }





}
