package net.ihe.gazelle.framework.modelvalidator.mock.polymorphism.builder;

import net.ihe.gazelle.framework.modelvalidator.domain.BuilderVisitor;
import net.ihe.gazelle.framework.modelvalidator.domain.ValidatorBuilderFactory;
import net.ihe.gazelle.framework.modelvalidator.mock.polymorphism.builder.visitor.FirstChildOfAbstractBuilderVisitor;
import net.ihe.gazelle.framework.modelvalidator.mock.polymorphism.structure.AbstractParent;
import net.ihe.gazelle.framework.modelvalidator.mock.polymorphism.structure.FirstChildOfAbstract;
import net.ihe.gazelle.framework.modelvalidator.mock.polymorphism.structure.NestedAbstractParent;

import java.util.List;

public class FirstChildOfAbstractBuilder extends AbstractParentBuilder {


    private String firstChildOfAbstractAttr;

    public FirstChildOfAbstractBuilder(ValidatorBuilderFactory validatorBuilderFactory) {
        super(validatorBuilderFactory);
    }

    public FirstChildOfAbstractBuilder setFirstChildOfAbstractAttr(String firstChildOfAbstractAttr) {
        this.firstChildOfAbstractAttr = firstChildOfAbstractAttr;
        return this;
    }

    @Override
    public FirstChildOfAbstractBuilder setAbstractParentAttr(String parentAttr) {
         super.setAbstractParentAttr(parentAttr);
            return this;
    }

    public FirstChildOfAbstractBuilder setNestedList(List<NestedAbstractParent> nestedList) {
        super.setNestedList(nestedList);
        return this;
    }

    @Override
    protected BuilderVisitor getBuilderVisitor() {
        return FirstChildOfAbstractBuilderVisitor.INSTANCE;
    }

    @Override
    protected Class<? extends AbstractParent> getParameterClass() {
        return FirstChildOfAbstract.class;
    }

    @Override
    protected void make() {
        super.make();
        super.<FirstChildOfAbstract>getObjectToBuild()
                .setFirstChildAttr(firstChildOfAbstractAttr);

    }
}
