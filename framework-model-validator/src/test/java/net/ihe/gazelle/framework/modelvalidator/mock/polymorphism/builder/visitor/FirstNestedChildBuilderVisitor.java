package net.ihe.gazelle.framework.modelvalidator.mock.polymorphism.builder.visitor;

import net.ihe.gazelle.framework.modelvalidator.domain.RootValidatorBuilder;
import net.ihe.gazelle.framework.modelvalidator.domain.ValidatorBuilderFactory;
import net.ihe.gazelle.framework.modelvalidator.mock.polymorphism.structure.FirstNestedChild;
import net.ihe.gazelle.framework.modelvalidator.mock.polymorphism.validator.FirstNestedChildValidatorBuilderSupplier;

public class FirstNestedChildBuilderVisitor extends NestedAbstractParentBuilderVisitor {

    public static FirstNestedChildBuilderVisitor INSTANCE = new FirstNestedChildBuilderVisitor();


    @Override
    public void visit(RootValidatorBuilder<?> rootValidatorBuilder, ValidatorBuilderFactory validatorBuilderFactory) {
        super.visit(rootValidatorBuilder, validatorBuilderFactory);
        rootValidatorBuilder.addValidatorBuilder(FirstNestedChild.class, new FirstNestedChildValidatorBuilderSupplier(validatorBuilderFactory));
    }
}
