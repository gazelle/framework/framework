package net.ihe.gazelle.framework.modelvalidator.mock.polymorphism.builder;

import net.ihe.gazelle.framework.modelvalidator.domain.BuilderVisitor;
import net.ihe.gazelle.framework.modelvalidator.domain.ValidatorBuilderFactory;
import net.ihe.gazelle.framework.modelvalidator.mock.polymorphism.builder.visitor.SecondChildOfAbstractBuilderVisitor;
import net.ihe.gazelle.framework.modelvalidator.mock.polymorphism.structure.AbstractParent;
import net.ihe.gazelle.framework.modelvalidator.mock.polymorphism.structure.SecondChildOfAbstract;

public class SecondChildOfAbstractBuilder extends AbstractParentBuilder {

    private String secondChildAttr;

    public SecondChildOfAbstractBuilder(ValidatorBuilderFactory validatorBuilderFactory) {
        super(validatorBuilderFactory);
    }

    @Override
    protected Class<? extends AbstractParent> getParameterClass() {
        return SecondChildOfAbstract.class;
    }

    @Override
    protected BuilderVisitor getBuilderVisitor() {
        return SecondChildOfAbstractBuilderVisitor.INSTANCE;
    }

    @Override
    protected void make() {
        super.make();
        super.<SecondChildOfAbstract>getObjectToBuild()
                .setSecondChildAttr(secondChildAttr);
    }


}
