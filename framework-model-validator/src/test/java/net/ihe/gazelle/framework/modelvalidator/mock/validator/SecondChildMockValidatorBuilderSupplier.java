package net.ihe.gazelle.framework.modelvalidator.mock.validator;

import net.ihe.gazelle.framework.modelvalidator.domain.ValidatorBuilder;
import net.ihe.gazelle.framework.modelvalidator.domain.ValidatorBuilderFactory;
import net.ihe.gazelle.framework.modelvalidator.mock.structure.ParentMock;
import net.ihe.gazelle.framework.modelvalidator.mock.structure.SecondChildMock;

public class SecondChildMockValidatorBuilderSupplier extends ParentMockValidatorBuilderSupplier {

    public SecondChildMockValidatorBuilderSupplier(ValidatorBuilderFactory validatorBuilderFactory) {
        super(validatorBuilderFactory);
    }

    @Override
    protected Class<? extends ParentMock> getType() {
        return SecondChildMock.class;
    }

    @Override
    protected ValidatorBuilder<ParentMock> supplyRulesAndMembers(ValidatorBuilder<ParentMock> validatorBuilder) {
        return super.supplyRulesAndMembers(validatorBuilder)
                .addRule("secondChildAttributeValid", scm -> ((SecondChildMock) scm).isSecondChildAttributeValid(), "Second child attribute must be valid");
    }

}
