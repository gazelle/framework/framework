package net.ihe.gazelle.framework.modelvalidator.mock.structure;

public class SubElementMock {

    private String subElementAttribute;

    public SubElementMock setSubElementAttribute(String subElementAttribute) {
        this.subElementAttribute = subElementAttribute;
        return this;
    }

    public String getSubElementAttribute() {
        return subElementAttribute;
    }

    public boolean isSubElementAttributeValid() {
        return subElementAttribute != null;
    }
}
