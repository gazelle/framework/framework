package net.ihe.gazelle.framework.modelvalidator.mock.extension;

import net.ihe.gazelle.framework.modelvalidator.mock.structure.ParentMock;

public class FourthChildMock extends ParentMock {

    private String fourthChildAttribute;


    public String getFourthChildAttribute() {
        return fourthChildAttribute;
    }

    public FourthChildMock setFourthChildAttribute(String fourthChildAttribute) {
        this.fourthChildAttribute = fourthChildAttribute;
        return this;
    }

    @Override
    public FourthChildMock setParentAttribute(String parentAttribute) {
        super.setParentAttribute(parentAttribute);
        return this;
    }


    public boolean isFourthChildAttributeValid(){
        return fourthChildAttribute != null && fourthChildAttribute.length() > 0;
    }
}
