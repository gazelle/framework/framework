package net.ihe.gazelle.framework.modelvalidator.mock.polymorphism.structure;

public class FirstNestedChild extends NestedAbstractParent{

    public String firstNestedChildAttr;

    public String getFirstNestedChildAttr() {
        return firstNestedChildAttr;
    }

    public FirstNestedChild setFirstNestedChildAttr(String firstNestedChildAttr) {
        this.firstNestedChildAttr = firstNestedChildAttr;
        return this;
    }

    @Override
    public FirstNestedChild setNestedParentAttr(String nestedParentAttr) {
        super.setNestedParentAttr(nestedParentAttr);
        return this;
    }
    public boolean isFirstNestedChildAttrValid() {
        return firstNestedChildAttr != null;
    }

}
