package net.ihe.gazelle.framework.modelvalidator.mock.polymorphism.validator;

import net.ihe.gazelle.framework.modelvalidator.domain.ValidatorBuilder;
import net.ihe.gazelle.framework.modelvalidator.domain.ValidatorBuilderFactory;
import net.ihe.gazelle.framework.modelvalidator.mock.polymorphism.structure.FirstNestedChild;
import net.ihe.gazelle.framework.modelvalidator.mock.polymorphism.structure.NestedAbstractParent;

public class FirstNestedChildValidatorBuilderSupplier extends NestedAbstractParentValidatorBuilderSupplier {


    public FirstNestedChildValidatorBuilderSupplier(ValidatorBuilderFactory validatorBuilderFactory) {
        super(validatorBuilderFactory);
    }

    @Override
    protected Class<? extends FirstNestedChild> getType() {
        return FirstNestedChild.class;
    }

    @Override
    protected ValidatorBuilder<NestedAbstractParent> supplyRulesAndMembers(ValidatorBuilder<NestedAbstractParent> validatorBuilder) {
        return super
                .supplyRulesAndMembers(validatorBuilder)
                .addRule("firstNestedChildAttrValid", c -> ((FirstNestedChild) c).isFirstNestedChildAttrValid(), "firstNestedChildAttr must be valid");
    }
}