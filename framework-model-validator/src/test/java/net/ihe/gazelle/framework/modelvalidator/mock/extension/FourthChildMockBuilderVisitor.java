package net.ihe.gazelle.framework.modelvalidator.mock.extension;

import net.ihe.gazelle.framework.modelvalidator.domain.BuilderVisitor;
import net.ihe.gazelle.framework.modelvalidator.domain.RootValidatorBuilder;
import net.ihe.gazelle.framework.modelvalidator.domain.ValidatorBuilderFactory;

public class FourthChildMockBuilderVisitor implements BuilderVisitor {


    public static FourthChildMockBuilderVisitor INSTANCE = new FourthChildMockBuilderVisitor();

    @Override
    public void visit(RootValidatorBuilder<?> rootValidatorBuilder, ValidatorBuilderFactory validatorBuilderFactory) {
        rootValidatorBuilder.addValidatorBuilder(FourthChildMock.class, new FourthChildMockValidatorBuilderSupplier(validatorBuilderFactory));
    }
}
