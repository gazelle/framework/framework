package net.ihe.gazelle.framework.modelvalidator.mock.polymorphism.builder.visitor;

import net.ihe.gazelle.framework.modelvalidator.domain.BuilderVisitor;
import net.ihe.gazelle.framework.modelvalidator.domain.RootValidatorBuilder;
import net.ihe.gazelle.framework.modelvalidator.domain.ValidatorBuilderFactory;
import net.ihe.gazelle.framework.modelvalidator.mock.polymorphism.structure.NestedAbstractParent;
import net.ihe.gazelle.framework.modelvalidator.mock.polymorphism.validator.NestedAbstractParentValidatorBuilderSupplier;

public class NestedAbstractParentBuilderVisitor implements BuilderVisitor {

    public static final NestedAbstractParentBuilderVisitor INSTANCE = new NestedAbstractParentBuilderVisitor();


    @Override
    public void visit(RootValidatorBuilder<?> rootValidatorBuilder, ValidatorBuilderFactory validatorBuilderFactory) {
        rootValidatorBuilder.addValidatorBuilder(NestedAbstractParent.class, new NestedAbstractParentValidatorBuilderSupplier(validatorBuilderFactory));
    }
}
