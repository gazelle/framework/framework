package net.ihe.gazelle.framework.modelvalidator.mock.validator;

import net.ihe.gazelle.framework.modelvalidator.domain.AbstractValidatorBuilderSupplier;
import net.ihe.gazelle.framework.modelvalidator.domain.ValidatorBuilder;
import net.ihe.gazelle.framework.modelvalidator.domain.ValidatorBuilderFactory;
import net.ihe.gazelle.framework.modelvalidator.mock.structure.ParentMock;

public class ParentMockValidatorBuilderSupplier extends AbstractValidatorBuilderSupplier<ParentMock> {

    public ParentMockValidatorBuilderSupplier(ValidatorBuilderFactory validatorBuilderFactory) {
        super(validatorBuilderFactory);
    }



    @Override
    protected Class<? extends ParentMock> getType() {
        return ParentMock.class;
    }


    @Override
    protected ValidatorBuilder<ParentMock> supplyRulesAndMembers(ValidatorBuilder<ParentMock> validatorBuilder) {
        return validatorBuilder
                .addRule("parentAttributeValid", ParentMock::isParentAttributeValid, "Parent attribute must be valid");
    }
}
