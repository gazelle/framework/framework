package net.ihe.gazelle.framework.modelvalidator.mock.builder;

import net.ihe.gazelle.framework.modelvalidator.domain.BuilderVisitor;
import net.ihe.gazelle.framework.modelvalidator.domain.ValidatorBuilderFactory;
import net.ihe.gazelle.framework.modelvalidator.mock.builder.visitors.ThirdChildMockBuilderVisitor;
import net.ihe.gazelle.framework.modelvalidator.mock.structure.ParentMock;
import net.ihe.gazelle.framework.modelvalidator.mock.structure.ThirdChildMock;

public class ThirdChildMockBuilder extends SecondChildMockBuilder {

    private String thirdChildAttribute;

    public ThirdChildMockBuilder(ValidatorBuilderFactory validatorBuilderFactory) {
        super(validatorBuilderFactory);
    }

    public ThirdChildMockBuilder setThirdChildAttribute(String thirdChildAttribute) {
        this.thirdChildAttribute = thirdChildAttribute;
        return this;
    }

    @Override
    public ThirdChildMockBuilder setSecondChildAttribute(String secondChildAttribute) {
        super.setSecondChildAttribute(secondChildAttribute);
        return this;
    }

    @Override
    public ThirdChildMockBuilder setParentAttribute(String parentAttribute) {
        super.setParentAttribute(parentAttribute);
        return this;
    }

    @Override
    protected BuilderVisitor getBuilderVisitor() {
        return ThirdChildMockBuilderVisitor.INSTANCE;
    }

    @Override
    protected Class<? extends ParentMock> getParameterClass() {
        return ThirdChildMock.class;
    }

    @Override
    protected void make() {
        super.make();
        super.<ThirdChildMock>getObjectToBuild()
                .setThirdChildAttribute(thirdChildAttribute);
    }
}