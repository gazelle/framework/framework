package net.ihe.gazelle.framework.modelvalidator.mock.validator;

import net.ihe.gazelle.framework.modelvalidator.domain.ValidatorBuilder;
import net.ihe.gazelle.framework.modelvalidator.domain.ValidatorBuilderFactory;
import net.ihe.gazelle.framework.modelvalidator.mock.structure.FirstChildMock;
import net.ihe.gazelle.framework.modelvalidator.mock.structure.ParentMock;
import net.ihe.gazelle.framework.modelvalidator.mock.structure.SubElementMock;

public class FirstChildMockValidatorBuilderSupplier extends ParentMockValidatorBuilderSupplier {



    public FirstChildMockValidatorBuilderSupplier(ValidatorBuilderFactory validatorBuilderFactory) {
        super(validatorBuilderFactory);
    }

    @Override
    protected Class<? extends ParentMock> getType() {
        return FirstChildMock.class;
    }

    @Override
    protected ValidatorBuilder<ParentMock> supplyRulesAndMembers(ValidatorBuilder<ParentMock> validatorBuilder) {
        return super
                .supplyRulesAndMembers(validatorBuilder)
                .addRule("firstChildAttributeValid", fcm -> ((FirstChildMock)fcm).isFirstChildAttributeValid(), "First child attribute must be valid")
                .addRule("subElementMockValid", fcm -> ((FirstChildMock) fcm).isSubElementAttributeValid(), "SubElementMock must be valid")
                .addMember("subElementMock", fcm -> ((FirstChildMock) fcm).getSubElementMock(), SubElementMock.class)
                ;
    }
}
