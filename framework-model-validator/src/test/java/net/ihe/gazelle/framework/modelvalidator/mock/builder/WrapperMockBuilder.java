package net.ihe.gazelle.framework.modelvalidator.mock.builder;

import net.ihe.gazelle.framework.modelvalidator.domain.AbstractBuilder;
import net.ihe.gazelle.framework.modelvalidator.domain.BuilderVisitor;
import net.ihe.gazelle.framework.modelvalidator.domain.ValidatorBuilderFactory;
import net.ihe.gazelle.framework.modelvalidator.mock.builder.visitors.WrapperMockBuilderVisitor;
import net.ihe.gazelle.framework.modelvalidator.mock.structure.ParentMock;
import net.ihe.gazelle.framework.modelvalidator.mock.structure.SubElementMock;
import net.ihe.gazelle.framework.modelvalidator.mock.structure.WrapperMock;

import java.util.ArrayList;
import java.util.List;

public class WrapperMockBuilder extends AbstractBuilder<WrapperMock> {


    private final List<ParentMock> parentMocks = new ArrayList<>();
    private SubElementMock subElementMock;



    public WrapperMockBuilder(ValidatorBuilderFactory validatorBuilderFactory) {
        super(validatorBuilderFactory);
    }


    public WrapperMockBuilder addParentMock(ParentMock parentMock) {
        parentMocks.add(parentMock);
        return this;
    }


    public WrapperMockBuilder setSubElementMock(SubElementMock subElementMock) {
        this.subElementMock = subElementMock;
        return this;
    }

    @Override
    protected BuilderVisitor getBuilderVisitor() {
        return WrapperMockBuilderVisitor.INSTANCE;
    }

    @Override
    protected Class<? extends WrapperMock> getParameterClass() {
        return WrapperMock.class;
    }

    @Override
    protected void make() {
        super.getObjectToBuild()
                .setParentMocks(parentMocks)
                .setSubElementMock(subElementMock)
        ;
    }




}
