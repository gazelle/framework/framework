package net.ihe.gazelle.framework.modelvalidator.mock.polymorphism.validator;

import net.ihe.gazelle.framework.modelvalidator.domain.AbstractValidatorBuilderSupplier;
import net.ihe.gazelle.framework.modelvalidator.domain.ValidatorBuilder;
import net.ihe.gazelle.framework.modelvalidator.domain.ValidatorBuilderFactory;
import net.ihe.gazelle.framework.modelvalidator.mock.polymorphism.structure.NestedAbstractParent;

public class NestedAbstractParentValidatorBuilderSupplier extends AbstractValidatorBuilderSupplier<NestedAbstractParent> {
    public NestedAbstractParentValidatorBuilderSupplier(ValidatorBuilderFactory validatorBuilderFactory) {
        super(validatorBuilderFactory);
    }

    @Override
    protected Class<? extends NestedAbstractParent> getType() {
        return NestedAbstractParent.class;
    }

    @Override
    protected ValidatorBuilder<NestedAbstractParent> supplyRulesAndMembers(ValidatorBuilder<NestedAbstractParent> validatorBuilder) {
        return validatorBuilder
                .addRule("nestedParentAttrValid", NestedAbstractParent::isNestedParentAttrValid, "nestedParentAttr must be valid");
    }
}
