package net.ihe.gazelle.framework.modelvalidator.mock.polymorphism.validator;

import net.ihe.gazelle.framework.modelvalidator.domain.ValidatorBuilder;
import net.ihe.gazelle.framework.modelvalidator.domain.ValidatorBuilderFactory;
import net.ihe.gazelle.framework.modelvalidator.mock.polymorphism.structure.AbstractParent;
import net.ihe.gazelle.framework.modelvalidator.mock.polymorphism.structure.FirstChildOfAbstract;

public class FirstChildOfAbstractValidatorBuilderSupplier extends AbstractParentValidatorBuilderSupplier {

    public FirstChildOfAbstractValidatorBuilderSupplier(ValidatorBuilderFactory validatorBuilderFactory) {
        super(validatorBuilderFactory);
    }

    @Override
    protected Class<? extends FirstChildOfAbstract> getType() {
        return FirstChildOfAbstract.class;
    }

    @Override
    protected ValidatorBuilder<AbstractParent> supplyRulesAndMembers(ValidatorBuilder<AbstractParent> validatorBuilder) {
        return super
                .supplyRulesAndMembers(validatorBuilder)
                .addRule("firstChildAttrValid",c -> ((FirstChildOfAbstract) c).isFirstChildAttrValid(), "firstChildAttr must be valid");
    }
}
