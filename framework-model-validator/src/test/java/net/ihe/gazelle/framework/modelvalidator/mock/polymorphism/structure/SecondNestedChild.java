package net.ihe.gazelle.framework.modelvalidator.mock.polymorphism.structure;

public class SecondNestedChild extends NestedAbstractParent {

    public String secondNestedChildAttr;

    public String getSecondNestedChildAttr() {
        return secondNestedChildAttr;
    }

    public SecondNestedChild setSecondNestedChildAttr(String secondNestedChildAttr) {
        this.secondNestedChildAttr = secondNestedChildAttr;
        return this;
    }

    public boolean isSecondNestedChildAttrValid() {
        return secondNestedChildAttr != null;
    }

}
