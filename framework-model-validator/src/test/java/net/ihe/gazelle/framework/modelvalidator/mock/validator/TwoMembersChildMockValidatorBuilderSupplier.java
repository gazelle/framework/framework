package net.ihe.gazelle.framework.modelvalidator.mock.validator;

import net.ihe.gazelle.framework.modelvalidator.domain.AbstractValidatorBuilderSupplier;
import net.ihe.gazelle.framework.modelvalidator.domain.ValidatorBuilder;
import net.ihe.gazelle.framework.modelvalidator.domain.ValidatorBuilderFactory;
import net.ihe.gazelle.framework.modelvalidator.mock.structure.SubElementMock;
import net.ihe.gazelle.framework.modelvalidator.mock.structure.TwoMembersChildMock;

public class TwoMembersChildMockValidatorBuilderSupplier extends AbstractValidatorBuilderSupplier<TwoMembersChildMock> {


    public TwoMembersChildMockValidatorBuilderSupplier(ValidatorBuilderFactory validatorBuilderFactory) {
        super(validatorBuilderFactory);
    }

    @Override
    protected Class<? extends TwoMembersChildMock> getType() {
        return TwoMembersChildMock.class;
    }

    @Override
    protected ValidatorBuilder<TwoMembersChildMock> supplyRulesAndMembers(ValidatorBuilder<TwoMembersChildMock> validatorBuilder) {
        return validatorBuilder.addRule("firstSubElementMockValid", TwoMembersChildMock::isFirstSubElementMockValid, "First sub element mock must be valid")
                .addRule("secondSubElementMockValid", TwoMembersChildMock::isSecondSubElementMockValid, "Second sub element mock must be valid")
                .addRule("firstSubElementMockListValid", TwoMembersChildMock::isFirstSubElementMockListValid, "First sub element mock list must be valid")
                .addRule("secondSubElementMockListValid", TwoMembersChildMock::isSecondSubElementMockListValid, "Second sub element mock list must be valid")
                .addMember("firstSubElementMock", TwoMembersChildMock::getFirstSubElementMock, SubElementMock.class)
                .addMember("secondSubElementMock", TwoMembersChildMock::getSecondSubElementMock, SubElementMock.class)
                .addMember("firstSubElementMockList", TwoMembersChildMock::getFirstSubElementMockList, SubElementMock.class)
                .addMember("secondSubElementMockList", TwoMembersChildMock::getSecondSubElementMockList, SubElementMock.class);

    }
}
