package net.ihe.gazelle.framework.modelvalidator.mock.structure;

public class ThirdChildMock extends SecondChildMock{

    private String thirdChildAttribute;


    public ThirdChildMock setThirdChildAttribute(String thirdChildAttribute) {
        this.thirdChildAttribute = thirdChildAttribute;
        return this;
    }

    @Override
    public ThirdChildMock setSecondChildAttribute(String secondChildAttribute) {
        super.setSecondChildAttribute(secondChildAttribute);
        return this;
    }

    @Override
    public ThirdChildMock setParentAttribute(String parentAttribute) {
        super.setParentAttribute(parentAttribute);
        return this;
    }

    @Override
    public String getParentAttribute() {
        return super.getParentAttribute();
    }

    public String getThirdChildAttribute() {
        return thirdChildAttribute;
    }

    public boolean isThirdChildAttributeValid() {
        return thirdChildAttribute != null;
    }


}
