package net.ihe.gazelle.framework.modelvalidator.mock.validator;

import net.ihe.gazelle.framework.modelvalidator.domain.AbstractValidatorBuilderSupplier;
import net.ihe.gazelle.framework.modelvalidator.domain.ValidatorBuilder;
import net.ihe.gazelle.framework.modelvalidator.domain.ValidatorBuilderFactory;
import net.ihe.gazelle.framework.modelvalidator.mock.structure.NestedMockOne;
import net.ihe.gazelle.framework.modelvalidator.mock.structure.NestedMockTwo;

public class NestedMockTwoValidatorBuilderSupplier extends AbstractValidatorBuilderSupplier<NestedMockTwo> {

    public NestedMockTwoValidatorBuilderSupplier(ValidatorBuilderFactory validatorBuilderFactory) {
        super(validatorBuilderFactory);
    }

    @Override
    protected Class<? extends NestedMockTwo> getType() {
        return NestedMockTwo.class;
    }

    @Override
    protected ValidatorBuilder<NestedMockTwo> supplyRulesAndMembers(ValidatorBuilder<NestedMockTwo> validatorBuilder) {
        return validatorBuilder
                .addRule("twoAttributeValid", NestedMockTwo::isTwoAttrValid, "Two attribute must be valid")
                .addMember("nestedMockOneFromTwo", NestedMockTwo::getNestedMockOneFromTwo, NestedMockOne.class)
                .addMember("nestedMockTwoFromTwo", NestedMockTwo::getNestedMockTwoFromTwo, NestedMockTwo.class)
                ;
    }
}
