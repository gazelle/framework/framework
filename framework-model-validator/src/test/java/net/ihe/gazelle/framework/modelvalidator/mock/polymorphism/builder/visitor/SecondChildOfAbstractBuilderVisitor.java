package net.ihe.gazelle.framework.modelvalidator.mock.polymorphism.builder.visitor;

import net.ihe.gazelle.framework.modelvalidator.domain.BuilderVisitor;
import net.ihe.gazelle.framework.modelvalidator.domain.RootValidatorBuilder;
import net.ihe.gazelle.framework.modelvalidator.domain.ValidatorBuilderFactory;
import net.ihe.gazelle.framework.modelvalidator.mock.polymorphism.structure.SecondChildOfAbstract;
import net.ihe.gazelle.framework.modelvalidator.mock.polymorphism.validator.SecondChildOfAbstractValidatorBuilderSupplier;

public class SecondChildOfAbstractBuilderVisitor implements BuilderVisitor {

    public static SecondChildOfAbstractBuilderVisitor INSTANCE = new SecondChildOfAbstractBuilderVisitor();


    @Override
    public void visit(RootValidatorBuilder<?> rootValidatorBuilder, ValidatorBuilderFactory validatorBuilderFactory) {

        rootValidatorBuilder.addValidatorBuilder(SecondChildOfAbstract.class, new SecondChildOfAbstractValidatorBuilderSupplier(validatorBuilderFactory));
    }
}
