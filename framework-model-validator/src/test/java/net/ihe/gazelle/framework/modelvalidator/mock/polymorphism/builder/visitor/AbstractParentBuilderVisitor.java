package net.ihe.gazelle.framework.modelvalidator.mock.polymorphism.builder.visitor;

import net.ihe.gazelle.framework.modelvalidator.domain.BuilderVisitor;
import net.ihe.gazelle.framework.modelvalidator.domain.RootValidatorBuilder;
import net.ihe.gazelle.framework.modelvalidator.domain.ValidatorBuilderFactory;
import net.ihe.gazelle.framework.modelvalidator.mock.polymorphism.structure.AbstractParent;
import net.ihe.gazelle.framework.modelvalidator.mock.polymorphism.validator.AbstractParentValidatorBuilderSupplier;

public class AbstractParentBuilderVisitor implements BuilderVisitor {

    public static AbstractParentBuilderVisitor INSTANCE = new AbstractParentBuilderVisitor();

    @Override
    public void visit(RootValidatorBuilder<?> rootValidatorBuilder, ValidatorBuilderFactory validatorBuilderFactory) {
        rootValidatorBuilder.addValidatorBuilder(AbstractParent.class, new AbstractParentValidatorBuilderSupplier(validatorBuilderFactory));
        NestedAbstractParentBuilderVisitor.INSTANCE.visit(rootValidatorBuilder, validatorBuilderFactory);
        SecondChildOfAbstractBuilderVisitor.INSTANCE.visit(rootValidatorBuilder, validatorBuilderFactory);
    }
}
