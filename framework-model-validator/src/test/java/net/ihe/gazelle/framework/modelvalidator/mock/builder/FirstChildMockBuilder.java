package net.ihe.gazelle.framework.modelvalidator.mock.builder;

import net.ihe.gazelle.framework.modelvalidator.domain.BuilderVisitor;
import net.ihe.gazelle.framework.modelvalidator.domain.ValidatorBuilderFactory;
import net.ihe.gazelle.framework.modelvalidator.mock.builder.visitors.FirstChildBuilderVisitor;
import net.ihe.gazelle.framework.modelvalidator.mock.structure.FirstChildMock;
import net.ihe.gazelle.framework.modelvalidator.mock.structure.ParentMock;
import net.ihe.gazelle.framework.modelvalidator.mock.structure.SubElementMock;

public class FirstChildMockBuilder extends ParentMockBuilder {

    private String firstChildAttribute;

    private SubElementMock subElementMock;

    private SubElementMock optionalSubElementMock;

    public FirstChildMockBuilder(ValidatorBuilderFactory validatorBuilderFactory) {
        super(validatorBuilderFactory);

    }

    @Override
    protected BuilderVisitor getBuilderVisitor() {
        return FirstChildBuilderVisitor.INSTANCE;
    }

    @Override
    protected Class<? extends ParentMock> getParameterClass() {
        return FirstChildMock.class;
    }

    public FirstChildMockBuilder setFirstChildAttribute(String firstChildAttribute) {
        this.firstChildAttribute = firstChildAttribute;
        return this;
    }

    public FirstChildMockBuilder setSubElementMockBuilder(SubElementMock subElementMockObjectBuilder) {
        this.subElementMock = subElementMockObjectBuilder;
        return this;
    }

    @Override
    public FirstChildMockBuilder setParentAttribute(String parentAttribute) {
        super.setParentAttribute(parentAttribute);
        return this;
    }

    public FirstChildMockBuilder setOptionalSubElementMockBuilder(SubElementMock optionalSubElementMock) {
        this.optionalSubElementMock = optionalSubElementMock;
        return this;
    }



    @Override
    protected void make() {
        super.make();
        super.<FirstChildMock>getObjectToBuild()
                .setFirstChildAttribute(firstChildAttribute)
                .setSubElementMock(subElementMock)
                .setOptionalSubElementMock(optionalSubElementMock)
        ;
    }


}