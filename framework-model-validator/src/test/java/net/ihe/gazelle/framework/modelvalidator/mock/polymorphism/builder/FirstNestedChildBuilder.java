package net.ihe.gazelle.framework.modelvalidator.mock.polymorphism.builder;

import net.ihe.gazelle.framework.modelvalidator.domain.BuilderVisitor;
import net.ihe.gazelle.framework.modelvalidator.domain.ValidatorBuilderFactory;
import net.ihe.gazelle.framework.modelvalidator.mock.polymorphism.builder.visitor.FirstNestedChildBuilderVisitor;
import net.ihe.gazelle.framework.modelvalidator.mock.polymorphism.structure.FirstNestedChild;
import net.ihe.gazelle.framework.modelvalidator.mock.polymorphism.structure.NestedAbstractParent;

public class FirstNestedChildBuilder extends NestedAbstractParentBuilder {

    private  String firstNestedChildAttr;

    public FirstNestedChildBuilder setFirstNestedChildAttr(String firstNestedChildAttr) {
        this.firstNestedChildAttr = firstNestedChildAttr;
        return this;
    }


    @Override
    public FirstNestedChildBuilder setNestedParentAttr(String nestedParentAttr) {
        super.setNestedParentAttr(nestedParentAttr);
        return this;
    }

    public FirstNestedChildBuilder(ValidatorBuilderFactory validatorBuilderFactory) {
        super(validatorBuilderFactory);
    }

    @Override
    protected Class<? extends NestedAbstractParent> getParameterClass() {
        return FirstNestedChild.class;
    }

    @Override
    protected BuilderVisitor getBuilderVisitor() {
        return FirstNestedChildBuilderVisitor.INSTANCE;
    }

    @Override
    protected void make() {
        super.make();
        super.<FirstNestedChild>getObjectToBuild()
                .setFirstNestedChildAttr(firstNestedChildAttr);

    }

}
