package net.ihe.gazelle.framework.modelvalidator.mock.builder.visitors;

import net.ihe.gazelle.framework.modelvalidator.domain.BuilderVisitor;
import net.ihe.gazelle.framework.modelvalidator.domain.RootValidatorBuilder;
import net.ihe.gazelle.framework.modelvalidator.domain.ValidatorBuilderFactory;
import net.ihe.gazelle.framework.modelvalidator.mock.structure.NestedMockTwo;
import net.ihe.gazelle.framework.modelvalidator.mock.validator.NestedMockTwoValidatorBuilderSupplier;

public class NestedMockTwoBuilderVisitor implements BuilderVisitor {

    public static NestedMockTwoBuilderVisitor INSTANCE = new NestedMockTwoBuilderVisitor();

    @Override
    public void visit(RootValidatorBuilder<?> rootValidatorBuilder, ValidatorBuilderFactory validatorBuilderFactory) {
        rootValidatorBuilder.addValidatorBuilder(NestedMockTwo.class, new NestedMockTwoValidatorBuilderSupplier(validatorBuilderFactory));

        // This will produce a StackOverflowError
//        NestedMockOneBuilderVisitor.INSTANCE.visit(rootValidatorBuilder, validatorBuilderFactory);

    }
}
