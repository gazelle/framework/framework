package net.ihe.gazelle.framework.modelvalidator.mock.structure;

import java.util.List;

public class TwoMembersChildMock {

    private SubElementMock firstSubElementMock;

    private SubElementMock secondSubElementMock;

    private List<SubElementMock> firstSubElementMockList;

    private List<SubElementMock> secondSubElementMockList;

    public SubElementMock getFirstSubElementMock() {
        return firstSubElementMock;
    }

    public TwoMembersChildMock setFirstSubElementMock(SubElementMock firstSubElementMock) {
        this.firstSubElementMock = firstSubElementMock;
        return this;
    }

    public SubElementMock getSecondSubElementMock() {
        return secondSubElementMock;
    }

    public TwoMembersChildMock setSecondSubElementMock(SubElementMock secondSubElementMock) {
        this.secondSubElementMock = secondSubElementMock;
        return this;
    }

    public List<SubElementMock> getFirstSubElementMockList() {
        return firstSubElementMockList;
    }

    public TwoMembersChildMock setFirstSubElementMockList(List<SubElementMock> firstSubElementMockList) {
        this.firstSubElementMockList = firstSubElementMockList;
        return this;
    }

    public List<SubElementMock> getSecondSubElementMockList() {
        return secondSubElementMockList;
    }

    public TwoMembersChildMock setSecondSubElementMockList(List<SubElementMock> secondSubElementMockList) {
        this.secondSubElementMockList = secondSubElementMockList;
        return this;
    }

    public boolean isFirstSubElementMockValid() {
        return firstSubElementMock != null;
    }

    public boolean isSecondSubElementMockValid() {
        return secondSubElementMock != null;
    }

    public boolean isFirstSubElementMockListValid() {
        return firstSubElementMockList != null;
    }

    public boolean isSecondSubElementMockListValid() {
        return secondSubElementMockList != null;
    }
}
