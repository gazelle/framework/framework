package net.ihe.gazelle.framework.modelvalidator.mock.structurevalidator;

import net.ihe.gazelle.framework.modelvalidator.domain.RootValidatorBuilder;
import net.ihe.gazelle.framework.modelvalidator.domain.StructureValidator;
import net.ihe.gazelle.framework.modelvalidator.domain.Validator;
import net.ihe.gazelle.framework.modelvalidator.domain.ValidatorBuilderFactory;
import net.ihe.gazelle.framework.modelvalidator.domain.result.ObjectResult;
import net.ihe.gazelle.framework.modelvalidator.mock.builder.visitors.FirstChildBuilderVisitor;
import net.ihe.gazelle.framework.modelvalidator.mock.structure.FirstChildMock;

public class FirstChildMockValidator implements StructureValidator<FirstChildMock> {

    private final Validator<FirstChildMock> validator;

    public FirstChildMockValidator(ValidatorBuilderFactory validatorBuilderFactory) {
        RootValidatorBuilder<FirstChildMock> rootValidatorBuilder = new RootValidatorBuilder<>(FirstChildMock.class);
        FirstChildBuilderVisitor.INSTANCE.visit(rootValidatorBuilder, validatorBuilderFactory);
        validator = rootValidatorBuilder.build();
    }

    @Override
    public ObjectResult validate(FirstChildMock object) {
        return validator.validate(object);
    }

    @Override
    public void assertValid(FirstChildMock object) {
        validator.assertValid(object);
    }

    @Override
    public boolean isValid(FirstChildMock object) {
        return validator.isValid(object);
    }

}
