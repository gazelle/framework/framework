package net.ihe.gazelle.framework.modelvalidator.mock.builder;

import net.ihe.gazelle.framework.modelvalidator.domain.AbstractBuilder;
import net.ihe.gazelle.framework.modelvalidator.domain.BuilderVisitor;
import net.ihe.gazelle.framework.modelvalidator.domain.ValidatorBuilderFactory;
import net.ihe.gazelle.framework.modelvalidator.mock.builder.visitors.WrongConstructorMockBuilderVisitor;
import net.ihe.gazelle.framework.modelvalidator.mock.structure.WrongConstructorMock;

public class WrongConstructorMockBuilder extends AbstractBuilder<WrongConstructorMock> {

    public WrongConstructorMockBuilder(ValidatorBuilderFactory validatorBuilderFactory) {
        super(validatorBuilderFactory);
    }

    @Override
    protected BuilderVisitor getBuilderVisitor() {
        return WrongConstructorMockBuilderVisitor.INSTANCE;
    }

    @Override
    protected Class<? extends WrongConstructorMock> getParameterClass() {
        return WrongConstructorMock.class;
    }

    @Override
    protected void make() {
        // Nothing to do
    }
}
