package net.ihe.gazelle.framework.modelvalidator.mock.builder;

import net.ihe.gazelle.framework.modelvalidator.domain.BuilderVisitor;
import net.ihe.gazelle.framework.modelvalidator.domain.ValidatorBuilderFactory;
import net.ihe.gazelle.framework.modelvalidator.mock.builder.visitors.SecondChildMockBuilderVisitor;
import net.ihe.gazelle.framework.modelvalidator.mock.structure.ParentMock;
import net.ihe.gazelle.framework.modelvalidator.mock.structure.SecondChildMock;

public class SecondChildMockBuilder extends ParentMockBuilder {

    private String secondChildAttribute;

    public SecondChildMockBuilder(ValidatorBuilderFactory validatorBuilderFactory) {
        super(validatorBuilderFactory);
    }


    @Override
    protected BuilderVisitor getBuilderVisitor() {
        return SecondChildMockBuilderVisitor.INSTANCE;
    }

    @Override
    protected Class<? extends ParentMock> getParameterClass() {
        return SecondChildMock.class;
    }

    public SecondChildMockBuilder setSecondChildAttribute(String secondChildAttribute) {
        this.secondChildAttribute = secondChildAttribute;
        return this;
    }

    @Override
    public SecondChildMockBuilder setParentAttribute(String parentAttribute) {
        super.setParentAttribute(parentAttribute);
        return this;
    }

    @Override
    protected void make() {
        super.make();
        super.<SecondChildMock>getObjectToBuild()
                .setSecondChildAttribute(secondChildAttribute);
    }
}