package net.ihe.gazelle.framework.modelvalidator.mock.polymorphism.builder.visitor;

import net.ihe.gazelle.framework.modelvalidator.domain.BuilderVisitor;
import net.ihe.gazelle.framework.modelvalidator.domain.RootValidatorBuilder;
import net.ihe.gazelle.framework.modelvalidator.domain.ValidatorBuilderFactory;
import net.ihe.gazelle.framework.modelvalidator.mock.polymorphism.structure.SecondNestedChild;
import net.ihe.gazelle.framework.modelvalidator.mock.polymorphism.validator.SecondNestedChildValidatorBuilderSupplier;

public class SecondNestedChildBuilderVisitor implements BuilderVisitor {

    public static SecondNestedChildBuilderVisitor INSTANCE = new SecondNestedChildBuilderVisitor();
    @Override
    public void visit(RootValidatorBuilder<?> rootValidatorBuilder, ValidatorBuilderFactory validatorBuilderFactory) {
        rootValidatorBuilder.addValidatorBuilder(SecondNestedChild.class, new SecondNestedChildValidatorBuilderSupplier(validatorBuilderFactory));
    }
}
