package net.ihe.gazelle.framework.modelvalidator.mock.structure;

public class SecondChildMock extends ParentMock {

    private String secondChildAttribute;


    @Override
    public SecondChildMock setParentAttribute(String parentAttribute) {
        super.setParentAttribute(parentAttribute);
        return this;
    }

    public SecondChildMock setSecondChildAttribute(String secondChildAttribute) {
        this.secondChildAttribute = secondChildAttribute;
        return this;
    }

    public String getSecondChildAttribute() {
        return secondChildAttribute;
    }

    public boolean isSecondChildAttributeValid() {
        return secondChildAttribute != null;
    }

}
