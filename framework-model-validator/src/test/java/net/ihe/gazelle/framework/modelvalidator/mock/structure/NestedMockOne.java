package net.ihe.gazelle.framework.modelvalidator.mock.structure;

public class NestedMockOne {

    private String oneAttr;
    private NestedMockOne nestedMockOneFromOne;

    private NestedMockTwo nestedMockTwoFromOne;

    public NestedMockOne getNestedMockOneFromOne() {
        return nestedMockOneFromOne;
    }


    public NestedMockTwo getNestedMockTwoFromOne() {
        return nestedMockTwoFromOne;
    }


    public NestedMockOne setNestedMockOneFromOne(NestedMockOne nestedMockOneFromOne) {
        this.nestedMockOneFromOne = nestedMockOneFromOne;
        return this;
    }

    public NestedMockOne setNestedMockTwoFromOne(NestedMockTwo nestedMockTwoFromOne) {
        this.nestedMockTwoFromOne = nestedMockTwoFromOne;
        return this;
    }

    public String getOneAttr() {
        return oneAttr;
    }

    public NestedMockOne setOneAttr(String oneAttr) {
        this.oneAttr = oneAttr;
        return this;
    }



    public boolean isOneAttrValid() {
        return oneAttr != null;
    }


}
