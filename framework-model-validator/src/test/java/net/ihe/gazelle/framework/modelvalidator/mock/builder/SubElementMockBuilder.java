package net.ihe.gazelle.framework.modelvalidator.mock.builder;

import net.ihe.gazelle.framework.modelvalidator.domain.AbstractBuilder;
import net.ihe.gazelle.framework.modelvalidator.domain.BuilderVisitor;
import net.ihe.gazelle.framework.modelvalidator.domain.ValidatorBuilderFactory;
import net.ihe.gazelle.framework.modelvalidator.mock.builder.visitors.SubElementBuilderVisitor;
import net.ihe.gazelle.framework.modelvalidator.mock.structure.SubElementMock;

public class SubElementMockBuilder extends AbstractBuilder<SubElementMock> {

    protected String subElementAttribute;

    public SubElementMockBuilder(ValidatorBuilderFactory validatorBuilderFactory) {
        super(validatorBuilderFactory);
    }

    @Override
    protected BuilderVisitor getBuilderVisitor() {
        return SubElementBuilderVisitor.INSTANCE;
    }

    @Override
    protected Class<? extends SubElementMock> getParameterClass() {
        return SubElementMock.class;
    }

    public SubElementMockBuilder setSubElementAttribute(String subElementAttribute) {
        this.subElementAttribute = subElementAttribute;
        return this;
    }



    @Override
    protected void make() {
        getObjectToBuild()
                .setSubElementAttribute(subElementAttribute);
    }

}