package net.ihe.gazelle.framework.modelvalidator.mock.extension;

import net.ihe.gazelle.framework.modelvalidator.domain.BuilderVisitor;
import net.ihe.gazelle.framework.modelvalidator.domain.ValidatorBuilderFactory;

public class WrapperMockBuilder extends net.ihe.gazelle.framework.modelvalidator.mock.builder.WrapperMockBuilder {

    public WrapperMockBuilder(ValidatorBuilderFactory validatorBuilderFactory) {
        super(validatorBuilderFactory);
    }



    @Override
    protected BuilderVisitor getBuilderVisitor() {
        return WrapperMockBuilderVisitor.INSTANCE;
    }
}
