package net.ihe.gazelle.framework.modelvalidator.mock.builder;

import net.ihe.gazelle.framework.modelvalidator.domain.BuilderVisitor;
import net.ihe.gazelle.framework.modelvalidator.domain.ValidatorBuilderFactory;
import net.ihe.gazelle.framework.modelvalidator.mock.builder.visitors.ChildOfAbstractMockBuilderVisitor;
import net.ihe.gazelle.framework.modelvalidator.mock.structure.AbstractParentMock;
import net.ihe.gazelle.framework.modelvalidator.mock.structure.ChildOfAbstractMock;

public class ChildOfAbstractMockBuilder extends AbstractParentMockBuilder {

    private String childOfAbstractParentAttribute;

    public ChildOfAbstractMockBuilder(ValidatorBuilderFactory validatorBuilderFactory) {
        super(validatorBuilderFactory);
    }

    @Override
    public ChildOfAbstractMockBuilder setAbstractParentAttribute(String abstractParentAttribute) {
        super.setAbstractParentAttribute(abstractParentAttribute);
        return this;
    }

    public ChildOfAbstractMockBuilder setChildOfAbstractParentAttribute(String childOfAbstractParentAttribute) {
        this.childOfAbstractParentAttribute = childOfAbstractParentAttribute;
        return this;
    }

    @Override
    protected BuilderVisitor getBuilderVisitor() {
        return ChildOfAbstractMockBuilderVisitor.INSTANCE;
    }

    @Override
    protected void make() {
        super.make();
        super.<ChildOfAbstractMock>getObjectToBuild()
                .setChildOfAbstractAttribute(childOfAbstractParentAttribute);
    }

    @Override
    protected Class<? extends AbstractParentMock> getParameterClass() {
        return ChildOfAbstractMock.class;
    }
}
