package net.ihe.gazelle.framework.modelvalidator.mock.builder.visitors;

import net.ihe.gazelle.framework.modelvalidator.domain.BuilderVisitor;
import net.ihe.gazelle.framework.modelvalidator.domain.RootValidatorBuilder;
import net.ihe.gazelle.framework.modelvalidator.domain.ValidatorBuilderFactory;
import net.ihe.gazelle.framework.modelvalidator.mock.structure.ChildOfAbstractMock;
import net.ihe.gazelle.framework.modelvalidator.mock.validator.ChildOfAbstractMockValidatorBuilderSupplier;

public class ChildOfAbstractMockBuilderVisitor extends AbstractParentMockBuilderVisitor {

    public static ChildOfAbstractMockBuilderVisitor INSTANCE = new ChildOfAbstractMockBuilderVisitor();

    @Override
    public void visit(RootValidatorBuilder<?> rootValidatorBuilder, ValidatorBuilderFactory validatorBuilderFactory) {
        super.visit(rootValidatorBuilder, validatorBuilderFactory);
        rootValidatorBuilder.addValidatorBuilder(ChildOfAbstractMock.class, new ChildOfAbstractMockValidatorBuilderSupplier(validatorBuilderFactory));
    }
}
