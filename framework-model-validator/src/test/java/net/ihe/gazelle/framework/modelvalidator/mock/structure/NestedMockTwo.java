package net.ihe.gazelle.framework.modelvalidator.mock.structure;

public class NestedMockTwo {

    private String twoAttr;

    private NestedMockOne nestedMockOneFromTwo;

    private NestedMockTwo nestedMockTwoFromTwo;

    public NestedMockOne getNestedMockOneFromTwo() {
        return nestedMockOneFromTwo;
    }



    public NestedMockTwo getNestedMockTwoFromTwo() {
        return nestedMockTwoFromTwo;
    }


    public NestedMockTwo setNestedMockOneFromTwo(NestedMockOne nestedMockOneFromTwo) {
        this.nestedMockOneFromTwo = nestedMockOneFromTwo;
        return this;
    }

    public NestedMockTwo setNestedMockTwoFromTwo(NestedMockTwo nestedMockTwoFromTwo) {
        this.nestedMockTwoFromTwo = nestedMockTwoFromTwo;
        return this;
    }

    public String getTwoAttr() {
        return twoAttr;
    }

    public NestedMockTwo setTwoAttr(String twoAttr) {
        this.twoAttr = twoAttr;
        return this;
    }


    public boolean isTwoAttrValid() {
        return twoAttr != null;
    }
}
