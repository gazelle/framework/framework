package net.ihe.gazelle.framework.modelvalidator.mock.builder.visitors;

import net.ihe.gazelle.framework.modelvalidator.domain.BuilderVisitor;
import net.ihe.gazelle.framework.modelvalidator.domain.RootValidatorBuilder;
import net.ihe.gazelle.framework.modelvalidator.domain.ValidatorBuilderFactory;
import net.ihe.gazelle.framework.modelvalidator.mock.structure.GenericMock;
import net.ihe.gazelle.framework.modelvalidator.mock.validator.GenericMockValidatorBuilderSupplier;

public class GenericMockBuilderVisitor implements BuilderVisitor {

    public static GenericMockBuilderVisitor INSTANCE = new GenericMockBuilderVisitor();

    @Override
    public void visit(RootValidatorBuilder<?> rootValidatorBuilder, ValidatorBuilderFactory validatorBuilderFactory) {
        rootValidatorBuilder.addValidatorBuilder(GenericMock.class, new GenericMockValidatorBuilderSupplier(validatorBuilderFactory));
    }
}
