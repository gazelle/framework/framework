package net.ihe.gazelle.framework.modelvalidator.mock.builder;

import net.ihe.gazelle.framework.modelvalidator.domain.AbstractBuilder;
import net.ihe.gazelle.framework.modelvalidator.domain.BuilderVisitor;
import net.ihe.gazelle.framework.modelvalidator.domain.ValidatorBuilderFactory;
import net.ihe.gazelle.framework.modelvalidator.mock.builder.visitors.AbstractParentMockBuilderVisitor;
import net.ihe.gazelle.framework.modelvalidator.mock.structure.AbstractParentMock;

public abstract class AbstractParentMockBuilder extends AbstractBuilder<AbstractParentMock> {

    private String abstractParentAttribute;

    public AbstractParentMockBuilder(ValidatorBuilderFactory validatorBuilderFactory) {
        super(validatorBuilderFactory);
    }

    @Override
    protected BuilderVisitor getBuilderVisitor() {
        return AbstractParentMockBuilderVisitor.INSTANCE;
    }

    public AbstractParentMockBuilder setAbstractParentAttribute(String abstractParentAttribute) {
        this.abstractParentAttribute = abstractParentAttribute;
        return this;
    }

    @Override
    protected void make() {
        super.getObjectToBuild()
                .setAbstractParentAttribute(abstractParentAttribute);
    }
}
