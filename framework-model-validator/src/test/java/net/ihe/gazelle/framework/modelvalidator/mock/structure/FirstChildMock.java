package net.ihe.gazelle.framework.modelvalidator.mock.structure;

public class FirstChildMock extends ParentMock {

    private String firstChildAttribute;

    private SubElementMock subElementMock;

    private SubElementMock optionalSubElementMock;



    public FirstChildMock setFirstChildAttribute(String firstChildAttribute) {
        this.firstChildAttribute = firstChildAttribute;
        return this;
    }

    @Override
    public FirstChildMock setParentAttribute(String parentAttribute) {
        super.setParentAttribute(parentAttribute);
        return this;
    }

    @Override
    public String getParentAttribute() {
        return super.getParentAttribute();
    }

    public String getFirstChildAttribute() {
        return firstChildAttribute;
    }

    public SubElementMock getSubElementMock() {
        return subElementMock;
    }

    public FirstChildMock setSubElementMock(SubElementMock subElementMock) {
        this.subElementMock = subElementMock;
        return this;
    }

    public SubElementMock getOptionalSubElementMock() {
        return optionalSubElementMock;
    }

    public FirstChildMock setOptionalSubElementMock(SubElementMock optionalSubElementMock) {
        this.optionalSubElementMock = optionalSubElementMock;
        return this;
    }


    public boolean isFirstChildAttributeValid() {
        return firstChildAttribute != null;
    }

    public boolean isSubElementAttributeValid() {
        return subElementMock != null ;
    }


}
