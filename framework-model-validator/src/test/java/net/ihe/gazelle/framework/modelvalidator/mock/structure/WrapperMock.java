package net.ihe.gazelle.framework.modelvalidator.mock.structure;

import java.util.ArrayList;
import java.util.List;

public class WrapperMock {

    private final List<ParentMock> parentMocks = new ArrayList<>();

    private SubElementMock subElementMock;


    public List<ParentMock> getParentMocks() {
        return parentMocks;
    }

    public WrapperMock setParentMocks(List<ParentMock> parentMocks) {
        this.parentMocks.addAll(parentMocks);
        return this;
    }

    public WrapperMock addParentMock(ParentMock parentMock) {
        parentMocks.add(parentMock);
        return this;
    }

    public SubElementMock getSubElementMock() {
        return subElementMock;
    }

    public WrapperMock setSubElementMock(SubElementMock subElementMock) {
        this.subElementMock = subElementMock;
        return this;
    }

    public boolean isParentMocksValid() {
        return !parentMocks.isEmpty();
    }

    public boolean isSubElementAttributeValid() {
        return subElementMock != null;
    }

}
