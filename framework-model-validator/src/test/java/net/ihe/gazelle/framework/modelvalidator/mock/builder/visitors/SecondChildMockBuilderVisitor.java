package net.ihe.gazelle.framework.modelvalidator.mock.builder.visitors;

import net.ihe.gazelle.framework.modelvalidator.domain.BuilderVisitor;
import net.ihe.gazelle.framework.modelvalidator.domain.RootValidatorBuilder;
import net.ihe.gazelle.framework.modelvalidator.domain.ValidatorBuilderFactory;
import net.ihe.gazelle.framework.modelvalidator.mock.structure.SecondChildMock;
import net.ihe.gazelle.framework.modelvalidator.mock.validator.SecondChildMockValidatorBuilderSupplier;

public class SecondChildMockBuilderVisitor extends ParentMockBuilderVisitor {

    public static SecondChildMockBuilderVisitor INSTANCE = new SecondChildMockBuilderVisitor();

    @Override
    public void visit(RootValidatorBuilder<?> rootValidatorBuilder, ValidatorBuilderFactory validatorBuilderFactory) {
        super.visit(rootValidatorBuilder, validatorBuilderFactory);
        rootValidatorBuilder.addValidatorBuilder(SecondChildMock.class, new SecondChildMockValidatorBuilderSupplier(validatorBuilderFactory));
    }
}
