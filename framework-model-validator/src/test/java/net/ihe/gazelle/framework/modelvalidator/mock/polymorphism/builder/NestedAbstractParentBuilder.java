package net.ihe.gazelle.framework.modelvalidator.mock.polymorphism.builder;

import net.ihe.gazelle.framework.modelvalidator.domain.AbstractBuilder;
import net.ihe.gazelle.framework.modelvalidator.domain.BuilderVisitor;
import net.ihe.gazelle.framework.modelvalidator.domain.ValidatorBuilderFactory;
import net.ihe.gazelle.framework.modelvalidator.mock.polymorphism.builder.visitor.NestedAbstractParentBuilderVisitor;
import net.ihe.gazelle.framework.modelvalidator.mock.polymorphism.structure.NestedAbstractParent;

public abstract class NestedAbstractParentBuilder extends AbstractBuilder<NestedAbstractParent> {


    private  String nestedParentAttr;

    public NestedAbstractParentBuilder setNestedParentAttr(String nestedParentAttr) {
        this.nestedParentAttr = nestedParentAttr;
        return this;
    }

    public NestedAbstractParentBuilder(ValidatorBuilderFactory validatorBuilderFactory) {
        super(validatorBuilderFactory);
    }

    @Override
    protected BuilderVisitor getBuilderVisitor() {
        return NestedAbstractParentBuilderVisitor.INSTANCE;
    }

    @Override
    protected void make() {
        super.getObjectToBuild()
                .setNestedParentAttr(nestedParentAttr);

    }
}
