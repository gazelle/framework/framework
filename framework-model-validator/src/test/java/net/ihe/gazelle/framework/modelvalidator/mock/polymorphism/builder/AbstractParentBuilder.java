package net.ihe.gazelle.framework.modelvalidator.mock.polymorphism.builder;

import net.ihe.gazelle.framework.modelvalidator.domain.AbstractBuilder;
import net.ihe.gazelle.framework.modelvalidator.domain.BuilderVisitor;
import net.ihe.gazelle.framework.modelvalidator.domain.ValidatorBuilderFactory;
import net.ihe.gazelle.framework.modelvalidator.mock.polymorphism.builder.visitor.AbstractParentBuilderVisitor;
import net.ihe.gazelle.framework.modelvalidator.mock.polymorphism.structure.AbstractParent;
import net.ihe.gazelle.framework.modelvalidator.mock.polymorphism.structure.NestedAbstractParent;
import net.ihe.gazelle.framework.modelvalidator.mock.polymorphism.structure.SecondChildOfAbstract;

import java.util.List;

public abstract class AbstractParentBuilder extends AbstractBuilder<AbstractParent> {

    private String abstractParentAttr;
    private List<NestedAbstractParent> nestedList;
    private SecondChildOfAbstract secondChild;

    public AbstractParentBuilder(ValidatorBuilderFactory validatorBuilderFactory) {
        super(validatorBuilderFactory);
    }

    public AbstractParentBuilder setAbstractParentAttr(String parentAttr) {
        this.abstractParentAttr = parentAttr;
        return this;
    }

    public AbstractParentBuilder setNestedList(List<NestedAbstractParent> nestedList) {
        this.nestedList = nestedList;
        return this;
    }

    public AbstractParentBuilder addNestedList(NestedAbstractParent nested) {
        this.nestedList.add(nested);
        return this;
    }

    public AbstractParentBuilder setSecondChild(SecondChildOfAbstract secondChild) {
        this.secondChild = secondChild;
        return this;
    }

    @Override
    protected BuilderVisitor getBuilderVisitor() {
        return AbstractParentBuilderVisitor.INSTANCE;
    }

    @Override
    protected void make() {
        super.getObjectToBuild()
                .setAbstractParentAttr(abstractParentAttr)
                .setSecondChild(secondChild)
                .setNestedList(nestedList);

    }

    @Override
    protected Class<? extends AbstractParent> getParameterClass() {
        return AbstractParent.class;
    }
}
