package net.ihe.gazelle.framework.modelvalidator.mock.polymorphism.structure;

public class SecondChildOfAbstract extends AbstractParent {

    private String secondChildAttr;

    public String getSecondChildAttr() {
        return secondChildAttr;
    }

    public SecondChildOfAbstract setSecondChildAttr(String secondChildAttr) {
        this.secondChildAttr = secondChildAttr;
        return this;
    }

    public boolean isSecondChildAttrValid() {
        return secondChildAttr != null;
    }

}
