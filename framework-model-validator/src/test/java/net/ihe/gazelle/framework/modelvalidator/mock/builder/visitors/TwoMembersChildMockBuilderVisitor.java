package net.ihe.gazelle.framework.modelvalidator.mock.builder.visitors;

import net.ihe.gazelle.framework.modelvalidator.domain.BuilderVisitor;
import net.ihe.gazelle.framework.modelvalidator.domain.RootValidatorBuilder;
import net.ihe.gazelle.framework.modelvalidator.domain.ValidatorBuilderFactory;
import net.ihe.gazelle.framework.modelvalidator.mock.structure.TwoMembersChildMock;
import net.ihe.gazelle.framework.modelvalidator.mock.validator.TwoMembersChildMockValidatorBuilderSupplier;

public class TwoMembersChildMockBuilderVisitor implements BuilderVisitor {

    public static TwoMembersChildMockBuilderVisitor INSTANCE = new TwoMembersChildMockBuilderVisitor();

    @Override
    public void visit(RootValidatorBuilder<?> rootValidatorBuilder, ValidatorBuilderFactory validatorBuilderFactory) {
        rootValidatorBuilder.addValidatorBuilder(TwoMembersChildMock.class, new TwoMembersChildMockValidatorBuilderSupplier(validatorBuilderFactory));
        SubElementBuilderVisitor.INSTANCE.visit(rootValidatorBuilder, validatorBuilderFactory);
    }
}
