package net.ihe.gazelle.framework.modelvalidator.mock.polymorphism.validator;

import net.ihe.gazelle.framework.modelvalidator.domain.AbstractValidatorBuilderSupplier;
import net.ihe.gazelle.framework.modelvalidator.domain.ValidatorBuilder;
import net.ihe.gazelle.framework.modelvalidator.domain.ValidatorBuilderFactory;
import net.ihe.gazelle.framework.modelvalidator.mock.polymorphism.structure.AbstractParent;
import net.ihe.gazelle.framework.modelvalidator.mock.polymorphism.structure.NestedAbstractParent;
import net.ihe.gazelle.framework.modelvalidator.mock.polymorphism.structure.SecondChildOfAbstract;

public class AbstractParentValidatorBuilderSupplier extends AbstractValidatorBuilderSupplier<AbstractParent> {


    public AbstractParentValidatorBuilderSupplier(ValidatorBuilderFactory validatorBuilderFactory) {
        super(validatorBuilderFactory);
    }

    @Override
    protected Class<? extends AbstractParent> getType() {
        return AbstractParent.class;
    }

    @Override
    protected ValidatorBuilder<AbstractParent> supplyRulesAndMembers(ValidatorBuilder<AbstractParent> validatorBuilder) {
        return validatorBuilder
                .addRule("abstractParentAttrValid", AbstractParent::isParentAttrValid, "abstractParentAttr must be valid")
                .addRule("nestedListValid", AbstractParent::isNestedListValid, "nestedList must be valid")
                .addRule("secondChildValid", AbstractParent::isSecondChildValid, "secondChild must be valid")
                .addMember("nestedList", AbstractParent::getNestedList, NestedAbstractParent.class)
                .addMember("secondChild", AbstractParent::getSecondChild, SecondChildOfAbstract.class);
    }
}
