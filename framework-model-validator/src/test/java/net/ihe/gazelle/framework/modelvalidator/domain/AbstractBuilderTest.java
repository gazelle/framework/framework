package net.ihe.gazelle.framework.modelvalidator.domain;

import net.ihe.gazelle.framework.modelvalidator.domain.*;
import net.ihe.gazelle.framework.modelvalidator.interlay.adapter.BValidatorBuilderFactory;
import net.ihe.gazelle.framework.modelvalidator.mock.builder.*;
import net.ihe.gazelle.framework.modelvalidator.mock.extension.FourthChildMock;
import net.ihe.gazelle.framework.modelvalidator.mock.structure.*;
import net.ihe.gazelle.framework.modelvalidator.mock.extension.FourthChildMockValidatorBuilderSupplier;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class AbstractBuilderTest {

    private static ValidatorBuilderFactory validatorBuilderFactory;

    @BeforeAll
    public static void init() {
        validatorBuilderFactory = new BValidatorBuilderFactory();
    }


    @Test
    public void testBuilderWithNullType(){
        Throwable throwable = assertThrows(IllegalArgumentException.class, () ->
                new AbstractBuilder<Object>(validatorBuilderFactory) {
                    @Override
                    protected BuilderVisitor getBuilderVisitor() {
                        return null;
                    }

                    @Override
                    protected Class<Object> getParameterClass() {
                        return null;
                    }

                    @Override
                    protected void make() {

                    }
                }
        );
        assertEquals("type must not be null", throwable.getMessage());
    }

    @Test
    public void testBuilderWithNullBuilderVisitor(){
        Throwable throwable = assertThrows(IllegalArgumentException.class, () ->
                new AbstractBuilder<Object>(validatorBuilderFactory) {
                    @Override
                    protected BuilderVisitor getBuilderVisitor() {
                        return null;
                    }

                    @Override
                    protected Class<Object> getParameterClass() {
                        return Object.class;
                    }

                    @Override
                    protected void make() {

                    }
                }
        );
        assertEquals("builderVisitor must not be null", throwable.getMessage());
    }

    @Test
    public void testBuildParent() {
        ParentMock parentMock = new ParentMockBuilder(validatorBuilderFactory)
                .setParentAttribute("parentAttribute")
                .build();

        assertEquals("parentAttribute", parentMock.getParentAttribute());
    }

    @Test
    public void testBuildInvalidParent() {
        Throwable throwable = assertThrows(ValidationException.class, () ->
            new ParentMockBuilder(validatorBuilderFactory)
                    .setParentAttribute(null)
                    .build()
        );
        assertTrue(throwable.getMessage().contains("ParentMock [parentAttributeValid] Parent attribute must be valid => invalid"));
    }


    @Test
    public void testBuildFirstChild() {
        FirstChildMock firstChildMock = new FirstChildMockBuilder(validatorBuilderFactory)
                .setParentAttribute("parentAttribute")
                .setFirstChildAttribute("firstChildAttribute")
                .setSubElementMockBuilder(new SubElementMock()
                        .setSubElementAttribute("subElementAttribute"))
                .build();

        assertEquals("parentAttribute", firstChildMock.getParentAttribute());
        assertEquals("firstChildAttribute", firstChildMock.getFirstChildAttribute());
        assertEquals("subElementAttribute", firstChildMock.getSubElementMock().getSubElementAttribute());

    }


    @Test
    public void testBuildInvalidFirstChild() {
        Throwable throwable = assertThrows(ValidationException.class, () ->
            new FirstChildMockBuilder(validatorBuilderFactory)
                    .setParentAttribute("parentAttribute")
                    .setFirstChildAttribute("firstChildAttribute")
                    .setSubElementMockBuilder(new SubElementMock()
                            .setSubElementAttribute(null))
                    .build()
        );
        assertTrue(throwable.getMessage().contains("FirstChildMock.subElementMock [subElementAttributeValid] SubElement attribute must be valid => invalid"));
    }

    @Test
    public void testBuildSecondChild() {
        SecondChildMock secondChildMock = new SecondChildMockBuilder(validatorBuilderFactory)
                .setParentAttribute("parentAttribute")
                .setSecondChildAttribute("secondChildAttribute")
                .build();

        assertEquals("parentAttribute", secondChildMock.getParentAttribute());
        assertEquals("secondChildAttribute", secondChildMock.getSecondChildAttribute());
    }

    @Test
    public void testBuildInvalidSecondChild() {
        Throwable throwable = assertThrows(ValidationException.class, () ->
            new SecondChildMockBuilder(validatorBuilderFactory)
                    .setParentAttribute("parentAttribute")
                    .setSecondChildAttribute(null)
                    .build()
        );
        assertTrue(throwable.getMessage().contains("SecondChildMock [secondChildAttributeValid] Second child attribute must be valid => invalid"));
    }

    @Test
    public void testBuildThirdChild() {
        ThirdChildMock thirdChildMock =  new ThirdChildMockBuilder(validatorBuilderFactory)
                .setParentAttribute("parentAttribute")
                .setSecondChildAttribute("secondChildAttribute")
                .setThirdChildAttribute("thirdChildAttribute")
                .build();

        assertEquals("parentAttribute", thirdChildMock.getParentAttribute());
        assertEquals("thirdChildAttribute", thirdChildMock.getThirdChildAttribute());
    }


    @Test
    public void testBuildInvalidThirdChild() {
        Throwable throwable = assertThrows(ValidationException.class, () ->
              new ThirdChildMockBuilder(validatorBuilderFactory)
                    .setParentAttribute("parentAttribute")
                    .setSecondChildAttribute(null)
                    .setThirdChildAttribute("thirdChildAttribute")
                    .build()
        );
        assertTrue(throwable.getMessage().contains("ThirdChildMock [secondChildAttributeValid] Second child attribute must be valid => invalid"));
    }


    @Test
    public void testBuildTwoMembersChild(){
        TwoMembersChildMock twoMembersChildMock = new TwoMembersChildMockBuilder(validatorBuilderFactory)
                .setFirstSubElementMock(new SubElementMock()
                        .setSubElementAttribute("firstSubElementAttribute"))
                .setSecondSubElementMock(new SubElementMock()
                        .setSubElementAttribute("secondSubElementAttribute"))
                .addFirstSubElementMockBuilder(new SubElementMock()
                        .setSubElementAttribute("firstSubElementAttributeInFirstList"))
                .addSecondSubElementMockBuilder(new SubElementMock()
                        .setSubElementAttribute("secondSubElementAttributeInSecondList"))
                .build();
        assertEquals("firstSubElementAttribute", twoMembersChildMock.getFirstSubElementMock().getSubElementAttribute());
        assertEquals("secondSubElementAttribute", twoMembersChildMock.getSecondSubElementMock().getSubElementAttribute());
        assertEquals(1, twoMembersChildMock.getFirstSubElementMockList().size());
        assertEquals("firstSubElementAttributeInFirstList", twoMembersChildMock.getFirstSubElementMockList().get(0).getSubElementAttribute());
        assertEquals(1, twoMembersChildMock.getSecondSubElementMockList().size());
        assertEquals("secondSubElementAttributeInSecondList", twoMembersChildMock.getSecondSubElementMockList().get(0).getSubElementAttribute());
    }



    @Test
    public void testBuildFirstChildWithNullOptionalSubElement(){
        FirstChildMock firstChildMock = new FirstChildMockBuilder(validatorBuilderFactory)
                .setParentAttribute("parentAttribute")
                .setFirstChildAttribute("firstChildAttribute")
                .setSubElementMockBuilder(new SubElementMock()
                        .setSubElementAttribute("subElementAttribute"))
                .setOptionalSubElementMockBuilder(null)
                .build();
        assertEquals("parentAttribute", firstChildMock.getParentAttribute());
        assertEquals("firstChildAttribute", firstChildMock.getFirstChildAttribute());
        assertEquals("subElementAttribute", firstChildMock.getSubElementMock().getSubElementAttribute());
        assertNull(firstChildMock.getOptionalSubElementMock()); //optional sub element is null
    }

    @Test
    public void testBuildWrongConstructorMock(){
        Throwable throwable = assertThrows(ObjectBuildException.class, () ->
                new WrongConstructorMockBuilder(validatorBuilderFactory)
                        .appendVisitor(new BuilderVisitor() {
                            @Override
                            public void visit(RootValidatorBuilder<?> rootValidatorBuilder, ValidatorBuilderFactory validatorBuilderFactory) {
                                rootValidatorBuilder.addValidatorBuilder(WrongConstructorMock.class, new AbstractValidatorBuilderSupplier<>(validatorBuilderFactory) {
                                    @Override
                                    protected Class<? extends WrongConstructorMock> getType() {
                                        return WrongConstructorMock.class;
                                    }

                                    @Override
                                    protected ValidatorBuilder<WrongConstructorMock> supplyRulesAndMembers(ValidatorBuilder<WrongConstructorMock> validatorBuilder) {
                                        return validatorBuilder.addRule("true", a -> true,"always true");
                                    }
                                });
                            }
                        })
                        .build()
        );
        assertInstanceOf(NoSuchMethodException.class, throwable.getCause());
    }


    @Test
    public void testValidateFirstChildMock(){
        Validator<FirstChildMock> validator = new RootValidatorBuilder<>(FirstChildMock.class)
                .addValidatorBuilder(
                        FirstChildMock.class,
                        validatorBuilderFactory
                        .getBuilder(FirstChildMock.class)
                        .addRule("firstChildAttributeValid", FirstChildMock::isFirstChildAttributeValid, "First child attribute must be valid")
                )
                .build();
        assertTrue(validator.isValid(new FirstChildMock().setFirstChildAttribute("firstChildAttribute")));

    }

    @Test
    public void testValidateInvalidFirstChildMock(){
        Validator<FirstChildMock> validator = new RootValidatorBuilder<>(FirstChildMock.class)
                .addValidatorBuilder(
                        FirstChildMock.class,
                        validatorBuilderFactory
                                .getBuilder(FirstChildMock.class)
                                .addRule("firstChildAttributeValid", FirstChildMock::isFirstChildAttributeValid, "First child attribute must be valid")
                )
                .build();
        assertFalse(validator.isValid(new FirstChildMock().setFirstChildAttribute(null)));
        assertThrows(ValidationException.class, () -> validator.assertValid(new FirstChildMock().setFirstChildAttribute(null)));

    }

    @Test
    public void testValidateArrayOfParentMock(){
        ParentMock[] parentMocks = new ParentMock[]{
                new ParentMockBuilder(validatorBuilderFactory)
                        .setParentAttribute("parentAttribute")
                        .build(),
                new ParentMockBuilder(validatorBuilderFactory)
                        .setParentAttribute("parentAttribute")
                        .build()
        };
        Validator<ParentMock> validator = new RootValidatorBuilder<>(ParentMock.class)
                .addValidatorBuilder(
                    ParentMock.class,
                    validatorBuilderFactory
                        .getBuilder(ParentMock.class)
                        .addRule("parentAttributeValid", ParentMock::isParentAttributeValid, "Parent attribute must be valid")
                )
                .build();
        assertTrue(validator.isValid(parentMocks));
        assertDoesNotThrow(() -> validator.assertValid(parentMocks));
    }

    @Test
    public void testBuildChildOfAbstractMock(){
        ChildOfAbstractMock childOfAbstractMock = new ChildOfAbstractMockBuilder(validatorBuilderFactory)
                .setAbstractParentAttribute("parentAttribute")
                .setChildOfAbstractParentAttribute("childOfAbstractAttribute")
                .build();
        assertEquals("parentAttribute", childOfAbstractMock.getAbstractParentAttribute());
        assertEquals("childOfAbstractAttribute", childOfAbstractMock.getChildOfAbstractAttribute());
    }

    @Test
    public void testBuildChildOfAbstractMockWithNullAbstractParentAttribute(){
        Throwable throwable = assertThrows(ValidationException.class, () ->
                new ChildOfAbstractMockBuilder(validatorBuilderFactory)
                        .setAbstractParentAttribute(null)
                        .setChildOfAbstractParentAttribute(null)
                        .build()
        );
        assertTrue(throwable.getMessage().contains("ChildOfAbstractMock [parentAttributeValid] Parent attribute must be valid => invalid"));
        assertTrue(throwable.getMessage().contains("ChildOfAbstractMock [childAttributeValid] Child attribute must be valid => invalid"));

    }

    @Test
    public void testPolymorphismUseCase(){
        Throwable throwable = assertThrows(ValidationException.class, () -> new WrapperMockBuilder(validatorBuilderFactory)
                .setSubElementMock(new SubElementMock()
                        .setSubElementAttribute("subElementAttribute"))
                .addParentMock(new ParentMock()
                        .setParentAttribute("parentAttribute"))
                .addParentMock(new FirstChildMock()
                        .setParentAttribute("parentAttribute1")
                        .setSubElementMock(new SubElementMock()
                                .setSubElementAttribute("subElementAttribute1"))
                        .setFirstChildAttribute("firstChildAttribute1"))
                .addParentMock(new SecondChildMock()
                        .setParentAttribute("parentAttribute2")
                        .setSecondChildAttribute("secondChildAttribute2"))
                .addParentMock(new ThirdChildMock()
                        .setParentAttribute("parentAttribute3")
                        .setSecondChildAttribute("secondChildAttribute3")
                        .setThirdChildAttribute(null))
                .build()
        );

        assertTrue(throwable.getMessage().contains("WrapperMock.parentMocks[3] [thirdChildAttributeValid] Third child attribute must be valid => invalid"));
    }

    @Test
    public void testBuilderExtension(){
        Throwable throwable = assertThrows(ValidationException.class, new WrapperMockBuilder(validatorBuilderFactory)
                .setSubElementMock(new SubElementMock()
                        .setSubElementAttribute("subElementAttribute"))
                .addParentMock(new ParentMock()
                        .setParentAttribute("parentAttribute"))
                .addParentMock(new FirstChildMock()
                        .setParentAttribute("parentAttribute1")
                        .setSubElementMock(new SubElementMock()
                                .setSubElementAttribute("subElementAttribute1"))
                        .setFirstChildAttribute("firstChildAttribute1"))
                .addParentMock(new SecondChildMock()
                        .setParentAttribute("parentAttribute2")
                        .setSecondChildAttribute("secondChildAttribute2"))
                .addParentMock(new ThirdChildMock()
                        .setParentAttribute("parentAttribute3")
                        .setSecondChildAttribute("secondChildAttribute3")
                        .setThirdChildAttribute("thirdChildAttribute3"))
                .addParentMock(new FourthChildMock()
                        .setParentAttribute(null)
                        .setFourthChildAttribute(null)
                )
                .appendVisitor(new BuilderVisitor() {
                    @Override
                    public void visit(RootValidatorBuilder<?> rootValidatorBuilder, ValidatorBuilderFactory validatorBuilderFactory) {
                        rootValidatorBuilder.addValidatorBuilder(
                                FourthChildMock.class, new FourthChildMockValidatorBuilderSupplier(validatorBuilderFactory)
                        );
                    }
                })::build
        );
        assertTrue(throwable.getMessage().contains("WrapperMock.parentMocks[4] [parentAttributeValid] Parent attribute must be valid => invalid"));
        assertTrue(throwable.getMessage().contains("WrapperMock.parentMocks[4] [fourthChildAttributeValid] Fourth child attribute must be valid => invalid"));

    }

    @Test
    public void testBuilderExtensionExternal(){
        Throwable throwable = assertThrows(ValidationException.class, () -> new net.ihe.gazelle.framework.modelvalidator.mock.extension.WrapperMockBuilder(validatorBuilderFactory)
                .setSubElementMock(new SubElementMock()
                        .setSubElementAttribute("subElementAttribute"))
                .addParentMock(new ParentMock()
                        .setParentAttribute("parentAttribute"))
                .addParentMock(new FirstChildMock()
                        .setParentAttribute("parentAttribute1")
                        .setSubElementMock(new SubElementMock()
                                .setSubElementAttribute("subElementAttribute1"))
                        .setFirstChildAttribute("firstChildAttribute1"))
                .addParentMock(new SecondChildMock()
                        .setParentAttribute("parentAttribute2")
                        .setSecondChildAttribute("secondChildAttribute2"))
                .addParentMock(new ThirdChildMock()
                        .setParentAttribute("parentAttribute3")
                        .setSecondChildAttribute("secondChildAttribute3")
                        .setThirdChildAttribute("thirdChildAttribute3"))
                .addParentMock(new FourthChildMock()
                        .setParentAttribute(null)
                        .setFourthChildAttribute(null)
                )
                .build()

        );
        assertTrue(throwable.getMessage().contains("WrapperMock.parentMocks[4] [parentAttributeValid] Parent attribute must be valid => invalid"));
        assertTrue(throwable.getMessage().contains("WrapperMock.parentMocks[4] [fourthChildAttributeValid] Fourth child attribute must be valid => invalid"));

    }

    @Test
    public void testBuildNestedObject(){
        //Create 4 level nested object
        Throwable throwable = assertThrows(ValidationException.class, () -> new NestedMockOneBuilder(validatorBuilderFactory)
                .setOneAttr("1")
                .setNestedMockOneFromOne(
                        new NestedMockOne()
                                .setNestedMockOneFromOne(
                                        new NestedMockOne()
                                                .setOneAttr("1.1.1")
                                )
                                .setNestedMockTwoFromOne(
                                        new NestedMockTwo()
                                                .setTwoAttr("1.1.2")
                                )
                                .setOneAttr("1.1")
                )
                .setNestedMockTwoFromOne(
                        new NestedMockTwo()
                                .setNestedMockOneFromTwo(
                                        new NestedMockOne()
                                                .setOneAttr("1.2.1")
                                )
                                .setNestedMockTwoFromTwo(
                                        new NestedMockTwo()
                                                .setTwoAttr("1.2.2")
                                                .setNestedMockOneFromTwo(
                                                        new NestedMockOne()
                                                )
                                )
                                .setTwoAttr("1.2")
                )
                .build()
        );
        assertTrue(throwable.getMessage().contains("NestedMockOne.nestedMockTwoFromOne.nestedMockTwoFromTwo.nestedMockOneFromTwo [oneAttributeValid] One attribute must be valid => invalid"));
    }

    @Test
    public void testValidateWithUnregisteredClass(){
        RootValidatorBuilder<WrapperMock> builder = new RootValidatorBuilder<>(WrapperMock.class)
                .addValidatorBuilder(WrapperMock.class,
                        validatorBuilderFactory.getBuilder(WrapperMock.class)
                                .addMember("parentAttribute", WrapperMock::getParentMocks, ParentMock.class)
                );
        assertThrows(IllegalStateException.class, () ->
                builder.build().isValid(new WrapperMock().addParentMock(new ParentMock().setParentAttribute("parentAttribute"))));
    }

    @Test
    public void testValidateWithInvalidRootType(){
        RootValidatorBuilder<WrapperMock> builder = new RootValidatorBuilder<>(WrapperMock.class)
                .addValidatorBuilder(ParentMock.class,
                        validatorBuilderFactory.getBuilder(ParentMock.class)
                                .addMember("parentAttribute", ParentMock::getParentAttribute, ParentMock.class)
                );
        assertThrows(IllegalStateException.class, () ->
                builder.build().isValid(new WrapperMock().addParentMock(new ParentMock().setParentAttribute("parentAttribute"))));
    }

    @Test
    public void testGenericUseCase(){
        GenericMock<String> genericMockString = new GenericMockBuilder(validatorBuilderFactory)
                .setGenericAttribute("genericAttribute")
                .build();
        GenericMock<Integer> genericMockInteger = new GenericMockBuilder(validatorBuilderFactory)
                .setGenericAttribute(1)
                .build();
        assertEquals("genericAttribute", genericMockString.getAttr());
        assertEquals(1, genericMockInteger.getAttr());
    }

    @Test
    public void testBuildField(){
        assertNull(AbstractBuilder.buildField(null));
        ParentMockBuilder parentMockBuilder = new ParentMockBuilder(validatorBuilderFactory)
                .setParentAttribute("parentAttribute");
        ParentMock expected = parentMockBuilder.build();
        ParentMock actual = AbstractBuilder.buildField(parentMockBuilder);
        assertEquals(expected.isParentAttributeValid(), actual.isParentAttributeValid());
        assertEquals(expected.getParentAttribute(), actual.getParentAttribute());
    }


    //Test exceptions for coverage

    @Test
    public void testObjectBuildExceptionWithCause(){
        assertThrows(ObjectBuildException.class, () -> {
            throw new ObjectBuildException(new Exception("cause"));
        });
    }

    @Test
    public void testObjectBuildExceptionWithCauseAndMessage(){
        assertThrows(ObjectBuildException.class, () -> {
            throw new ObjectBuildException("message", new Exception("cause"));
        });
    }

    @Test
    public void testObjectBuildExceptionWithMessage(){
        assertThrows(ObjectBuildException.class, () -> {
            throw new ObjectBuildException("message");
        });
    }

    @Test
    public void testValidationExceptionWithCause(){
        assertThrows(ValidationException.class, () -> {
            throw new ValidationException(new Exception("cause"));
        });
    }

    @Test
    public void testValidationExceptionWithCauseAndMessage(){
        assertThrows(ValidationException.class, () -> {
            throw new ValidationException("message", new Exception("cause"));
        });
    }

    @Test
    public void testValidationExceptionWithMessage(){
        assertThrows(ValidationException.class, () -> {
            throw new ValidationException("message");
        });
    }


}
