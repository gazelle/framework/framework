package net.ihe.gazelle.framework.modelvalidator.mock.validator;

import net.ihe.gazelle.framework.modelvalidator.domain.ValidatorBuilder;
import net.ihe.gazelle.framework.modelvalidator.domain.ValidatorBuilderFactory;
import net.ihe.gazelle.framework.modelvalidator.mock.structure.ParentMock;
import net.ihe.gazelle.framework.modelvalidator.mock.structure.ThirdChildMock;

public class ThirdChildMockValidatorBuilderSupplier extends SecondChildMockValidatorBuilderSupplier {

    public ThirdChildMockValidatorBuilderSupplier(ValidatorBuilderFactory validatorBuilderFactory) {
        super(validatorBuilderFactory);
    }

    @Override
    protected Class<? extends ParentMock> getType() {
        return ThirdChildMock.class;
    }

    @Override
    protected ValidatorBuilder<ParentMock> supplyRulesAndMembers(ValidatorBuilder<ParentMock> validatorBuilder) {
        return super.supplyRulesAndMembers(validatorBuilder)
                .addRule("thirdChildAttributeValid", tcm -> ((ThirdChildMock) tcm).isThirdChildAttributeValid(), "Third child attribute must be valid");
    }

}
