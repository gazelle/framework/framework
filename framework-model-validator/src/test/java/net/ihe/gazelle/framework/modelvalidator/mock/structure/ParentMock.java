package net.ihe.gazelle.framework.modelvalidator.mock.structure;

public class ParentMock {

    private String parentAttribute;

    public ParentMock setParentAttribute(String parentAttribute) {
        this.parentAttribute = parentAttribute;
        return this;
    }

    public String getParentAttribute() {
        return parentAttribute;
    }

    public boolean isParentAttributeValid() {
        return parentAttribute != null;
    }


}
