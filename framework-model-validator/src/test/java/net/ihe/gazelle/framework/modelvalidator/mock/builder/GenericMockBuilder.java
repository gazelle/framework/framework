package net.ihe.gazelle.framework.modelvalidator.mock.builder;

import net.ihe.gazelle.framework.modelvalidator.domain.AbstractBuilder;
import net.ihe.gazelle.framework.modelvalidator.domain.BuilderVisitor;
import net.ihe.gazelle.framework.modelvalidator.domain.ValidatorBuilderFactory;
import net.ihe.gazelle.framework.modelvalidator.mock.builder.visitors.GenericMockBuilderVisitor;
import net.ihe.gazelle.framework.modelvalidator.mock.structure.GenericMock;

public class GenericMockBuilder extends AbstractBuilder<GenericMock> {

    private Object genericAttribute;

    public GenericMockBuilder(ValidatorBuilderFactory validatorBuilderFactory) {
        super(validatorBuilderFactory);
    }

    @Override
    protected Class<? extends GenericMock> getParameterClass() {
        return  GenericMock.class;
    }

    @Override
    protected BuilderVisitor getBuilderVisitor() {
        return GenericMockBuilderVisitor.INSTANCE;
    }

    public GenericMockBuilder setGenericAttribute(Object genericAttribute) {
        this.genericAttribute = genericAttribute;
        return this;
    }

    @Override
    protected void make() {
        super.getObjectToBuild()
                .setAttr(genericAttribute);
    }
}
