package net.ihe.gazelle.framework.modelvalidator.mock.extension;

import net.ihe.gazelle.framework.modelvalidator.domain.RootValidatorBuilder;
import net.ihe.gazelle.framework.modelvalidator.domain.ValidatorBuilderFactory;

public class WrapperMockBuilderVisitor extends net.ihe.gazelle.framework.modelvalidator.mock.builder.visitors.WrapperMockBuilderVisitor{

    public static final WrapperMockBuilderVisitor INSTANCE = new WrapperMockBuilderVisitor();

    @Override
    public void visit(RootValidatorBuilder<?> rootValidatorBuilder, ValidatorBuilderFactory validatorBuilderFactory) {
        super.visit(rootValidatorBuilder, validatorBuilderFactory);
        FourthChildMockBuilderVisitor.INSTANCE.visit(rootValidatorBuilder, validatorBuilderFactory);
    }
}
