package net.ihe.gazelle.framework.modelvalidator.mock.polymorphism;

import net.ihe.gazelle.framework.modelvalidator.domain.ValidationException;
import net.ihe.gazelle.framework.modelvalidator.domain.ValidatorBuilderFactory;
import net.ihe.gazelle.framework.modelvalidator.interlay.adapter.BValidatorBuilderFactory;
import net.ihe.gazelle.framework.modelvalidator.mock.polymorphism.builder.FirstChildOfAbstractBuilder;
import net.ihe.gazelle.framework.modelvalidator.mock.polymorphism.builder.FirstNestedChildBuilder;
import net.ihe.gazelle.framework.modelvalidator.mock.polymorphism.structure.*;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class AbstractParentObjectBuilder {

    private static ValidatorBuilderFactory validatorBuilderFactory;

    @BeforeAll
    public static void init() {
        validatorBuilderFactory = new BValidatorBuilderFactory();
    }

    @Test
    public void testBuildValidFirstNestedChild() {
            FirstNestedChild firstNestedChild = new FirstNestedChildBuilder(validatorBuilderFactory)
                    .setNestedParentAttr("nestedParentAttr")
                    .setFirstNestedChildAttr("firstNestedChildAttr")
                    .build();
            assertEquals("nestedParentAttr", firstNestedChild.getNestedParentAttr());
            assertEquals("firstNestedChildAttr", firstNestedChild.getFirstNestedChildAttr());
    }

    @Test
    public void testBuildFirstNestedChildWithInvalidNestedParentAttr() {
        Throwable throwable = assertThrows(ValidationException.class, () ->
                new FirstNestedChildBuilder(validatorBuilderFactory)
                .setNestedParentAttr(null)
                .setFirstNestedChildAttr("FirstNestedChild [nestedParentAttrValid] nestedParentAttr must be valid => invalid")
                .build());
        assertTrue(throwable.getMessage().contains("nestedParentAttr must be valid => invalid"));
    }

    @Test
    public void testBuildFirstNestedChildWithInvalidFirstNestedChildAttr() {
        Throwable throwable = assertThrows(ValidationException.class, () ->
                new FirstNestedChildBuilder(validatorBuilderFactory)
                .setNestedParentAttr("nestedParentAttr")
                .setFirstNestedChildAttr(null)
                .build());
        assertTrue(throwable.getMessage().contains("FirstNestedChild [firstNestedChildAttrValid] firstNestedChildAttr must be valid => invalid"));
    }

    @Test
    public void testBuildValidFirstChildOfAbstractWithAllFields() {
        AbstractParent firstChildOfAbstract = new FirstChildOfAbstractBuilder(validatorBuilderFactory)
                .setFirstChildOfAbstractAttr("firstChildOfAbstractAttr")
                .setAbstractParentAttr("abstractParentAttr")
                .setSecondChild(new SecondChildOfAbstract()
                        .setSecondChildAttr("secondChildAttr"))
                .setNestedList(List.of(
                        new FirstNestedChild()
                            .setNestedParentAttr("nestedParentAttr")
                            .setFirstNestedChildAttr("firstNestedChildAttr"),
                        new SecondNestedChild()
                                .setSecondNestedChildAttr("secondNestedChildAttr")
                                .setNestedParentAttr("nestedParentAttr")))
                .build();
        assertEquals("abstractParentAttr", firstChildOfAbstract.getAbstractParentAttr());
        assertEquals("firstChildOfAbstractAttr", ((FirstChildOfAbstract) firstChildOfAbstract).getFirstChildAttr());
        assertEquals("secondChildAttr", firstChildOfAbstract.getSecondChild().getSecondChildAttr());
        assertEquals("nestedParentAttr", firstChildOfAbstract.getNestedList().get(0).getNestedParentAttr());
        assertEquals("firstNestedChildAttr", ((FirstNestedChild) firstChildOfAbstract.getNestedList().get(0)).getFirstNestedChildAttr());
        assertEquals("nestedParentAttr", firstChildOfAbstract.getNestedList().get(1).getNestedParentAttr());
        assertEquals("secondNestedChildAttr", ((SecondNestedChild) firstChildOfAbstract.getNestedList().get(1)).getSecondNestedChildAttr());
    }

    @Test
    public void testBuildFirstChildOfAbstractWithInvalidAbstractParentAttr() {
        Throwable throwable = assertThrows(ValidationException.class, () ->
                new FirstChildOfAbstractBuilder(validatorBuilderFactory)
                .setFirstChildOfAbstractAttr("firstChildOfAbstractAttr")
                .setAbstractParentAttr(null)
                .setSecondChild(new SecondChildOfAbstract()
                        .setSecondChildAttr("secondChildAttr"))
                .setNestedList(List.of(new FirstNestedChild()
                        .setNestedParentAttr("nestedParentAttr")
                        .setFirstNestedChildAttr("firstNestedChildAttr")))
                .build());
        assertTrue(throwable.getMessage().contains("FirstChildOfAbstract [abstractParentAttrValid] abstractParentAttr must be valid => invalid"));
    }

    @Test
    public void testBuildFirstChildOfAbstractWithInvalidFirstChildOfAbstractAttr() {
        Throwable throwable = assertThrows(ValidationException.class, () ->
                new FirstChildOfAbstractBuilder(validatorBuilderFactory)
                .setFirstChildOfAbstractAttr(null)
                .setAbstractParentAttr("abstractParentAttr")
                .setSecondChild(new SecondChildOfAbstract()
                        .setSecondChildAttr("secondChildAttr"))
                .setNestedList(List.of(new FirstNestedChild()
                        .setNestedParentAttr("nestedParentAttr")
                        .setFirstNestedChildAttr("firstNestedChildAttr")))
                .build());
        assertTrue(throwable.getMessage().contains("FirstChildOfAbstract [firstChildAttrValid] firstChildAttr must be valid => invalid"));
    }

    @Test
    public void testBuildFirstChildOfAbstractWithInvalidAttOfSecondChild() {
        Throwable throwable = assertThrows(ValidationException.class, () ->
                new FirstChildOfAbstractBuilder(validatorBuilderFactory)
                .setFirstChildOfAbstractAttr("firstChildOfAbstractAttr")
                .setAbstractParentAttr("abstractParentAttr")
                .setSecondChild(new SecondChildOfAbstract()
                        .setSecondChildAttr(null))
                .setNestedList(List.of(new FirstNestedChild()
                        .setNestedParentAttr("nestedParentAttr")
                        .setFirstNestedChildAttr("firstNestedChildAttr")))
                .build());
        assertTrue(throwable.getMessage().contains("FirstChildOfAbstract.secondChild [secondChildAttrValid] secondChildAttr must be valid => invalid"));
    }

    @Test
    public void testBuildFirstChildOfAbstractWithInvalidAttOfNestedList() {
        Throwable throwable = assertThrows(ValidationException.class, () ->
                new FirstChildOfAbstractBuilder(validatorBuilderFactory)
                .setFirstChildOfAbstractAttr("firstChildOfAbstractAttr")
                .setAbstractParentAttr("abstractParentAttr")
                .setSecondChild(new SecondChildOfAbstract()
                        .setSecondChildAttr("secondChildAttr"))
                .setNestedList(List.of(new FirstNestedChild()
                        .setNestedParentAttr(null)
                        .setFirstNestedChildAttr("firstNestedChildAttr")))
                .build());
        assertTrue(throwable.getMessage().contains("FirstChildOfAbstract.nestedList[0] [nestedParentAttrValid] nestedParentAttr must be valid => invalid"));
    }

    @Test
    public void testBuildFirstChildWithAllInvalid(){
        Throwable throwable = assertThrows(ValidationException.class, () ->
                new FirstChildOfAbstractBuilder(validatorBuilderFactory)
                .setFirstChildOfAbstractAttr(null)
                .setAbstractParentAttr(null)
                .setSecondChild(new SecondChildOfAbstract()
                        .setSecondChildAttr(null))
                .setNestedList(List.of(new FirstNestedChild()
                        .setNestedParentAttr(null)
                        .setFirstNestedChildAttr("firstNestedChildAttr")))
                .build());
        throwable.printStackTrace();
        assertTrue(throwable.getMessage().contains("FirstChildOfAbstract [abstractParentAttrValid] abstractParentAttr must be valid => invalid"));
        assertTrue(throwable.getMessage().contains("FirstChildOfAbstract [firstChildAttrValid] firstChildAttr must be valid => invalid"));
        assertTrue(throwable.getMessage().contains("FirstChildOfAbstract.secondChild [secondChildAttrValid] secondChildAttr must be valid => invalid"));
        assertTrue(throwable.getMessage().contains("FirstChildOfAbstract.nestedList[0] [nestedParentAttrValid] nestedParentAttr must be valid => invalid"));
    }

}
