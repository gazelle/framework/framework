package net.ihe.gazelle.framework.modelvalidator.mock.polymorphism.validator;

import net.ihe.gazelle.framework.modelvalidator.domain.ValidatorBuilder;
import net.ihe.gazelle.framework.modelvalidator.domain.ValidatorBuilderFactory;
import net.ihe.gazelle.framework.modelvalidator.mock.polymorphism.structure.NestedAbstractParent;
import net.ihe.gazelle.framework.modelvalidator.mock.polymorphism.structure.SecondNestedChild;

public class SecondNestedChildValidatorBuilderSupplier extends NestedAbstractParentValidatorBuilderSupplier {


    public SecondNestedChildValidatorBuilderSupplier(ValidatorBuilderFactory validatorBuilderFactory) {
        super(validatorBuilderFactory);
    }

    @Override
    protected Class<? extends SecondNestedChild> getType() {
        return SecondNestedChild.class;
    }

    @Override
    protected ValidatorBuilder<NestedAbstractParent> supplyRulesAndMembers(ValidatorBuilder<NestedAbstractParent> validatorBuilder) {
        return super
                .supplyRulesAndMembers(validatorBuilder)
                .addRule("secondNestedChildAttrValid", c -> ((SecondNestedChild) c).isSecondNestedChildAttrValid(), "secondNestedChildAttr must be valid");
    }
}
