package net.ihe.gazelle.framework.modelvalidator.mock.validator;

import net.ihe.gazelle.framework.modelvalidator.domain.AbstractValidatorBuilderSupplier;
import net.ihe.gazelle.framework.modelvalidator.domain.ValidatorBuilder;
import net.ihe.gazelle.framework.modelvalidator.domain.ValidatorBuilderFactory;
import net.ihe.gazelle.framework.modelvalidator.mock.structure.SubElementMock;

public class SubElementMockValidatorBuilderSupplier extends AbstractValidatorBuilderSupplier<SubElementMock> {

    public SubElementMockValidatorBuilderSupplier(ValidatorBuilderFactory validatorBuilderFactory) {
        super(validatorBuilderFactory);

    }

    @Override
    protected Class<? extends SubElementMock> getType() {
        return SubElementMock.class;
    }

    @Override
    protected  ValidatorBuilder<SubElementMock> supplyRulesAndMembers(ValidatorBuilder<SubElementMock> validatorBuilder) {
        return validatorBuilder
                .addRule("subElementAttributeValid", SubElementMock::isSubElementAttributeValid, "SubElement attribute must be valid");
    }
}
