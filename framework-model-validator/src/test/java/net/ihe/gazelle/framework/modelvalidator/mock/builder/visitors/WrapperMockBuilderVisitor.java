package net.ihe.gazelle.framework.modelvalidator.mock.builder.visitors;

import net.ihe.gazelle.framework.modelvalidator.domain.BuilderVisitor;
import net.ihe.gazelle.framework.modelvalidator.domain.RootValidatorBuilder;
import net.ihe.gazelle.framework.modelvalidator.domain.ValidatorBuilderFactory;
import net.ihe.gazelle.framework.modelvalidator.mock.structure.WrapperMock;
import net.ihe.gazelle.framework.modelvalidator.mock.validator.WrapperMockValidatorBuilderSupplier;

public class WrapperMockBuilderVisitor implements BuilderVisitor {

    public static WrapperMockBuilderVisitor INSTANCE = new WrapperMockBuilderVisitor();

    @Override
    public void visit(RootValidatorBuilder<?> rootValidatorBuilder, ValidatorBuilderFactory validatorBuilderFactory) {
        rootValidatorBuilder.addValidatorBuilder(WrapperMock.class, new WrapperMockValidatorBuilderSupplier(validatorBuilderFactory));
        ParentMockBuilderVisitor.INSTANCE.visit(rootValidatorBuilder, validatorBuilderFactory);
        SubElementBuilderVisitor.INSTANCE.visit(rootValidatorBuilder, validatorBuilderFactory);
        FirstChildBuilderVisitor.INSTANCE.visit(rootValidatorBuilder, validatorBuilderFactory);
        SecondChildMockBuilderVisitor.INSTANCE.visit(rootValidatorBuilder, validatorBuilderFactory);
        ThirdChildMockBuilderVisitor.INSTANCE.visit(rootValidatorBuilder, validatorBuilderFactory);
    }

}
