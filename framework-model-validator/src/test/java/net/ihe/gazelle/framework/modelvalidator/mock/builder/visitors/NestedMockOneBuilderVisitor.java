package net.ihe.gazelle.framework.modelvalidator.mock.builder.visitors;

import net.ihe.gazelle.framework.modelvalidator.domain.BuilderVisitor;
import net.ihe.gazelle.framework.modelvalidator.domain.RootValidatorBuilder;
import net.ihe.gazelle.framework.modelvalidator.domain.ValidatorBuilderFactory;
import net.ihe.gazelle.framework.modelvalidator.mock.structure.NestedMockOne;
import net.ihe.gazelle.framework.modelvalidator.mock.validator.NestedMockOneValidatorBuilderSupplier;

public class NestedMockOneBuilderVisitor implements BuilderVisitor {

    public static NestedMockOneBuilderVisitor INSTANCE = new NestedMockOneBuilderVisitor();

    @Override
    public void visit(RootValidatorBuilder<?> rootValidatorBuilder, ValidatorBuilderFactory validatorBuilderFactory) {
        rootValidatorBuilder.addValidatorBuilder(NestedMockOne.class, new NestedMockOneValidatorBuilderSupplier(validatorBuilderFactory));
        NestedMockTwoBuilderVisitor.INSTANCE.visit(rootValidatorBuilder, validatorBuilderFactory);

    }
}
