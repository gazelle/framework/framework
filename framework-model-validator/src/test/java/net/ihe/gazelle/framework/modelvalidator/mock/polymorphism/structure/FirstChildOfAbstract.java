package net.ihe.gazelle.framework.modelvalidator.mock.polymorphism.structure;

public class FirstChildOfAbstract extends AbstractParent {

    private String firstChildAttr;

    public String getFirstChildAttr() {
        return firstChildAttr;
    }

    public FirstChildOfAbstract setFirstChildAttr(String firstChildAttr) {
        this.firstChildAttr = firstChildAttr;
        return this;
    }



    public boolean isFirstChildAttrValid() {
        return firstChildAttr != null;
    }
}
