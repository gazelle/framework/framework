package net.ihe.gazelle.framework.modelvalidator.mock.builder.visitors;

import net.ihe.gazelle.framework.modelvalidator.domain.BuilderVisitor;
import net.ihe.gazelle.framework.modelvalidator.domain.RootValidatorBuilder;
import net.ihe.gazelle.framework.modelvalidator.domain.ValidatorBuilderFactory;
import net.ihe.gazelle.framework.modelvalidator.mock.structure.AbstractParentMock;
import net.ihe.gazelle.framework.modelvalidator.mock.validator.AbstractParentMockValidatorBuilderSupplier;

public class AbstractParentMockBuilderVisitor implements BuilderVisitor {

    public static AbstractParentMockBuilderVisitor INSTANCE = new AbstractParentMockBuilderVisitor();

    @Override
    public void visit(RootValidatorBuilder<?> rootValidatorBuilder, ValidatorBuilderFactory validatorBuilderFactory) {
        rootValidatorBuilder.addValidatorBuilder(AbstractParentMock.class, new AbstractParentMockValidatorBuilderSupplier(validatorBuilderFactory));
    }
}
