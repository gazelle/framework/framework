package net.ihe.gazelle.framework.modelvalidator.mock.builder;

import net.ihe.gazelle.framework.modelvalidator.domain.AbstractBuilder;
import net.ihe.gazelle.framework.modelvalidator.domain.BuilderVisitor;
import net.ihe.gazelle.framework.modelvalidator.domain.ValidatorBuilderFactory;
import net.ihe.gazelle.framework.modelvalidator.mock.builder.visitors.NestedMockOneBuilderVisitor;
import net.ihe.gazelle.framework.modelvalidator.mock.structure.NestedMockOne;
import net.ihe.gazelle.framework.modelvalidator.mock.structure.NestedMockTwo;

public class NestedMockOneBuilder extends AbstractBuilder<NestedMockOne> {

    private NestedMockOne nestedMockOneFromOne;

    private NestedMockTwo nestedMockTwoFromOne;

    private String oneAttr;

    public NestedMockOneBuilder(ValidatorBuilderFactory validatorBuilderFactory) {
        super(validatorBuilderFactory);
    }

    public NestedMockOneBuilder setNestedMockOneFromOne(NestedMockOne nestedMockOneFromOne) {
        this.nestedMockOneFromOne = nestedMockOneFromOne;
        return this;
    }

    public NestedMockOneBuilder setNestedMockTwoFromOne(NestedMockTwo nestedMockTwoFromOne) {
        this.nestedMockTwoFromOne = nestedMockTwoFromOne;
        return this;
    }

    public NestedMockOneBuilder setOneAttr(String oneAttr) {
        this.oneAttr = oneAttr;
        return this;
    }

    @Override
    protected BuilderVisitor getBuilderVisitor() {
        return NestedMockOneBuilderVisitor.INSTANCE;
    }

    @Override
    protected Class<? extends NestedMockOne> getParameterClass() {
        return NestedMockOne.class;
    }

    @Override
    protected void make() {
        super.getObjectToBuild()
                .setNestedMockOneFromOne(nestedMockOneFromOne)
                .setNestedMockTwoFromOne(nestedMockTwoFromOne)
                .setOneAttr(oneAttr)
                ;
    }
}
