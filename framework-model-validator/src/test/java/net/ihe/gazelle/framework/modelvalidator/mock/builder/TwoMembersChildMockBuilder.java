package net.ihe.gazelle.framework.modelvalidator.mock.builder;

import net.ihe.gazelle.framework.modelvalidator.domain.AbstractBuilder;
import net.ihe.gazelle.framework.modelvalidator.domain.BuilderVisitor;
import net.ihe.gazelle.framework.modelvalidator.domain.ValidatorBuilderFactory;
import net.ihe.gazelle.framework.modelvalidator.mock.builder.visitors.TwoMembersChildMockBuilderVisitor;
import net.ihe.gazelle.framework.modelvalidator.mock.structure.SubElementMock;
import net.ihe.gazelle.framework.modelvalidator.mock.structure.TwoMembersChildMock;

import java.util.ArrayList;
import java.util.List;

public class TwoMembersChildMockBuilder extends AbstractBuilder<TwoMembersChildMock> {

    private SubElementMock firstSubElementMock;

    private SubElementMock secondSubElementMock;

    private final List<SubElementMock> firstSubElementMockList = new ArrayList<>();

    private final List<SubElementMock> secondSubElementMockList = new ArrayList<>();

    public TwoMembersChildMockBuilder(ValidatorBuilderFactory validatorBuilderFactory) {
        super(validatorBuilderFactory);
    }

    public TwoMembersChildMockBuilder setFirstSubElementMock(SubElementMock firstSubElementMock) {
        this.firstSubElementMock = firstSubElementMock;
        return this;
    }

    public TwoMembersChildMockBuilder setSecondSubElementMock(SubElementMock secondSubElementMock) {
        this.secondSubElementMock = secondSubElementMock;
        return this;
    }

    public TwoMembersChildMockBuilder addFirstSubElementMockBuilder(SubElementMock firstSubElementMockBuilder) {
        this.firstSubElementMockList.add(firstSubElementMockBuilder);
        return this;
    }

    public TwoMembersChildMockBuilder addSecondSubElementMockBuilder(SubElementMock secondSubElementMockBuilder) {
        this.secondSubElementMockList.add(secondSubElementMockBuilder);
        return this;
    }

    @Override
    protected BuilderVisitor getBuilderVisitor() {
        return TwoMembersChildMockBuilderVisitor.INSTANCE;
    }

    @Override
    protected Class<? extends TwoMembersChildMock> getParameterClass() {
        return TwoMembersChildMock.class;
    }

    @Override
    protected void make() {
        super.getObjectToBuild()
                .setFirstSubElementMock(firstSubElementMock)
                .setSecondSubElementMock(secondSubElementMock)
                .setFirstSubElementMockList(firstSubElementMockList)
                .setSecondSubElementMockList(secondSubElementMockList)
                ;
    }
}
