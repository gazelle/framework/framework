package net.ihe.gazelle.framework.modelvalidator.application;

public interface StructureValidatorFactoryProvider {

    <T> StructureValidatorFactory<T> getStructureValidatorFactory(Class<T> type);

    <T> StructureValidatorFactory<T> getStructureValidatorFactory(String name);
}
