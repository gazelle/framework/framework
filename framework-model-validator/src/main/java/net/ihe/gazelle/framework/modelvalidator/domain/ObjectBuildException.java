package net.ihe.gazelle.framework.modelvalidator.domain;

import java.io.Serial;

public class ObjectBuildException extends RuntimeException {

    @Serial
    private static final long serialVersionUID = 512324554403959672L;

    public ObjectBuildException(String message) {
        super(message);
    }

    public ObjectBuildException(String message, Throwable cause) {
        super(message, cause);
    }

    public ObjectBuildException(Throwable cause) {
        super(cause);
    }
}
