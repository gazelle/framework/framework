package net.ihe.gazelle.framework.modelvalidator.domain;

import net.ihe.gazelle.framework.modelvalidator.domain.result.ObjectResult;

public interface Validator <T>{

    void assertValid(T object);

    void assertValid(T[] object);

    boolean isValid(T object);

    boolean isValid(T[] object);

    ObjectResult validate(T object);
}
