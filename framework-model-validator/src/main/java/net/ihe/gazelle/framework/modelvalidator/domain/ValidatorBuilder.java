package net.ihe.gazelle.framework.modelvalidator.domain;

import java.util.function.Function;
import java.util.function.Predicate;

public interface ValidatorBuilder<T>{

    ValidatorBuilder<T> addRule(String id, Predicate<T> rule, String description);

    <R> ValidatorBuilder<T> addMember(String name, Function<T, ?> getter, Class<? extends R> memberClass);



}
