package net.ihe.gazelle.framework.modelvalidator.domain;

import java.util.function.Supplier;

public abstract class AbstractValidatorBuilderSupplier<T> implements Supplier<ValidatorBuilder<T>> {

    private final ValidatorBuilderFactory validatorBuilderFactory;


    protected abstract Class<? extends T> getType();


    protected AbstractValidatorBuilderSupplier(ValidatorBuilderFactory validatorBuilderFactory) {
        this.validatorBuilderFactory = validatorBuilderFactory;

    }

    protected abstract ValidatorBuilder<T> supplyRulesAndMembers(ValidatorBuilder<T> validatorBuilder);

    @Override
    public final ValidatorBuilder<T> get() {
        return supplyRulesAndMembers(validatorBuilderFactory.getBuilder(getType()));
    }


}
