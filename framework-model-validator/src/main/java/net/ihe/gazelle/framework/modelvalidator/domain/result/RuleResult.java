package net.ihe.gazelle.framework.modelvalidator.domain.result;

public class RuleResult {

    private final String id ;
    private final String description;
    private final boolean valid;


    public RuleResult(String id, String description, boolean valid) {
        this.id = id;
        this.description = description;
        this.valid = valid;
    }

    public String getId() {
        return id;
    }

    public String getDescription() {
        return description;
    }

    public boolean isValid() {
        return valid;
    }


    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        if (id != null && !id.isEmpty()) {
            sb.append("[").append(id).append("] ");
        }
        return sb.append(description).append(" => ").append(asResultString(valid)).toString();
    }

    private String asResultString(boolean result) {
        return result ? "valid" : "invalid";
    }

}
