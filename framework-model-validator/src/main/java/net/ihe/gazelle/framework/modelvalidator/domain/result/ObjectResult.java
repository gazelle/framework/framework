package net.ihe.gazelle.framework.modelvalidator.domain.result;


import java.util.ArrayList;
import java.util.List;

public class ObjectResult {

    private final String businessObjectName;
    private final List<RuleResult> ruleResults = new ArrayList<>();
    private final List<ObjectResult> memberResults = new ArrayList<>();


    public ObjectResult(String businessObjectName) {
        this.businessObjectName = businessObjectName;
    }

    /**
     * Return the overall business rules and members result.
     *
     * @return true if all rules and members are valid, false otherwise.
     */
    public boolean isValid() {
        for (RuleResult ruleResult : ruleResults) {
            if (!ruleResult.isValid()) {
                return false;
            }
        }
        for (ObjectResult memberResult : memberResults) {
            if (!memberResult.isValid()) {
                return false;
            }
        }
        return true;
    }


    public ObjectResult addMemberResult(ObjectResult memberResult) {
        this.memberResults.add(memberResult);
        return this;
    }

    public ObjectResult addRuleResult(RuleResult ruleResult) {
        this.ruleResults.add(ruleResult);
        return this;
    }


    public List<RuleResult> getRuleResults() {
        return ruleResults;
    }

    @Override
    public String toString() {
        return toString("");
    }

    private String toString(final String prefix) {
        StringBuilder sb = new StringBuilder();
        for (RuleResult ruleResult : ruleResults) {
            sb.append(prefix).append(businessObjectName).append(" ").append(ruleResult.toString()).append(System.lineSeparator());
        }
        if (!memberResults.isEmpty()) {
            String subPrefix = prefix + businessObjectName + ".";
            for (ObjectResult objectResult : memberResults) {
                sb.append(objectResult.toString(subPrefix));
            }
        }
        return sb.toString();
    }

}
