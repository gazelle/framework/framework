package net.ihe.gazelle.framework.modelvalidator.domain;

import java.util.Map;

public abstract class AbstractValidatorBuilder <T> implements ValidatorBuilder <T>{



    protected abstract Validator<T> build(Map<Class<?>, AbstractValidatorBuilder<?>> validatorBuilders);

}
