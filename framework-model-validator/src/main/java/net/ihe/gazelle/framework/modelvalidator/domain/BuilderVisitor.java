package net.ihe.gazelle.framework.modelvalidator.domain;


/**
 * The API to visit the {@link AbstractValidatorBuilderSupplier} and accumulate in
 * {@link RootValidatorBuilder} the {@link ValidatorBuilder} for each type.
 *
 * @author Achraf Achkari
 */
public interface BuilderVisitor {


    void visit(RootValidatorBuilder<?> rootValidatorBuilder, ValidatorBuilderFactory validatorBuilderFactory);



}
