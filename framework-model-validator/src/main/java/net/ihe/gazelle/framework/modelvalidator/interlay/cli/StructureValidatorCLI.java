package net.ihe.gazelle.framework.modelvalidator.interlay.cli;

import com.fasterxml.jackson.databind.ObjectMapper;
import net.ihe.gazelle.framework.modelvalidator.application.StructureValidatorFactory;
import net.ihe.gazelle.framework.modelvalidator.application.StructureValidatorFactoryProvider;
import net.ihe.gazelle.framework.modelvalidator.domain.StructureValidator;
import net.ihe.gazelle.framework.modelvalidator.domain.result.ObjectResult;
import net.ihe.gazelle.framework.modelvalidator.interlay.factory.StructureValidatorFactoryProviderSPI;
import org.kohsuke.args4j.CmdLineParser;
import org.kohsuke.args4j.Option;

import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * CLI tool for validating structures.
 * The structure and its factory should be present in the classpath.
 * The validation report structure is read from stdin or from a file.
 *
 * @author Achraf Achkari
 */

public class StructureValidatorCLI {

    protected static final String DOCUMENTATION = "\tStructure Validator\n"+
            "\t------------------------------------\n"+
            "\tThis tool validates any structure that present in the classpath against the predefined schema (see README.md).\n"+
            "\tThe validation report structure is read from stdin or from a file.\n"+
            "\tThe tool could be executed using maven with the following command:\n"+
            "\t\tmvn exec:java -Dexec.mainClass=\"net.ihe.gazelle.framework.modelvalidator.interlay.cli.StructureValidatorCLI\" [-Dexec.args=\"-f <path_to_validation_report>]\"" +
            "[-t canonical_name_of_the_structure] [-n name_of_the_structure]\"\n"
            ;


    public static final String ANSI_RESET = "\u001B[0m";
    public static final String ANSI_RED = "\u001B[31m";
    public static final String ANSI_GREEN = "\u001B[32m";

    private final StructureValidatorFactoryProvider structureValidatorFactoryProvider;

    @Option(name="--file",usage="Absolute path to validation report to validate", aliases = "-f")
    private String validationReportFilePath;

    @Option(name="--type",usage="The full canonical name of the structure to validate", aliases = "-t", required = false)
    private String structureType;

    @Option(name="--name",usage="The name of the structure to validate if the class name was not provided", aliases = "-n", required = false)
    private String structureName;

    @Option(name="--help", usage="print this message", aliases = "-h")
    private boolean help;


    protected String getDocumentation() {
        return DOCUMENTATION;
    }


    public StructureValidatorCLI() {
        this.structureValidatorFactoryProvider = new StructureValidatorFactoryProviderSPI();
    }

    public static void main(String[] args)  {
        new StructureValidatorCLI().execute(args);
    }

    void execute(String[] args) {
        CmdLineParser parser = new CmdLineParser(this);
        try{
            parser.parseArgument(args);

            if(help){
                System.out.println(getDocumentation());
                parser.printUsage(System.out);
                return;
            }


            StructureValidator<Object> structureValidator;
            Class<Object> structureClass;

            if(structureType != null){
                structureClass = (Class<Object>) Class.forName(structureType);
                structureValidator = structureValidatorFactoryProvider
                        .getStructureValidatorFactory(structureClass)
                        .createStructureValidator();
            }
            else if (structureName != null){
                StructureValidatorFactory<Object> factory = structureValidatorFactoryProvider
                        .getStructureValidatorFactory(structureName);
                structureValidator = factory.createStructureValidator();
                structureClass = factory.getType();

            }
            else{
                System.out.println(ANSI_RED+"No structure type provided"+ANSI_RESET);
                parser.printUsage(System.out);
                return;
            }


            Object structure;
            ObjectMapper mapper = new ObjectMapper();

            if(validationReportFilePath == null){
                if(System.in.available() == 0){
                    System.out.println(ANSI_RED+"No structure provided in STDIN"+ANSI_RESET);
                    parser.printUsage(System.out);
                    return;
                }
                // read from stdin
                structure = mapper.readValue(System.in, structureClass);
            }
            else{
                // read from file
                String reportContent = new String(Files.readAllBytes(Paths.get(validationReportFilePath)));
                structure = mapper.readValue(reportContent, structureClass);
            }


            ObjectResult objectResult = structureValidator.validate(structure);

            boolean isValid = objectResult.isValid();

            System.out.println("Validation result: " + (isValid? ANSI_GREEN + "VALID" + ANSI_RESET : ANSI_RED + "INVALID" + ANSI_RESET));

            System.out.println(objectResult);
        }
        catch (Exception e){
            System.out.println(getDocumentation());
            parser.printUsage(System.out);
            throw new RuntimeException(e);
        }

    }


}
