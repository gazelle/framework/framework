package net.ihe.gazelle.framework.modelvalidator.domain;

import java.util.HashMap;
import java.util.Map;

public class RootValidatorBuilder<T> {

    private final Class<T> rootType;

    private final Map<Class<?>,AbstractValidatorBuilder<?>> validatorBuilders = new HashMap<>();

    public RootValidatorBuilder(Class<T> rootType) {
        this.rootType = rootType;
    }

    public <R> RootValidatorBuilder<T> addValidatorBuilder(Class<R> type, ValidatorBuilder<R> validatorBuilder) {
        validatorBuilders.put(type,(AbstractValidatorBuilder<R>)validatorBuilder);
        return this;
    }

    public <R> RootValidatorBuilder<T> addValidatorBuilder(Class<? extends R> type, AbstractValidatorBuilderSupplier<? extends R> validatorBuilderSupplier) {
        validatorBuilders.put(type,(AbstractValidatorBuilder<?>)validatorBuilderSupplier.get());
        return this;
    }



    public Validator<T> build() {
        if (!validatorBuilders.containsKey(rootType))
            throw new IllegalStateException("Root type " + rootType + " is not registered in validator builders");
        return (Validator<T>) validatorBuilders.get(rootType).build(validatorBuilders);
    }

}
