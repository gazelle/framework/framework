package net.ihe.gazelle.framework.modelvalidator.domain;


import net.ihe.gazelle.framework.modelvalidator.domain.result.ObjectResult;


/**
 * The API to assemble a validator for a given type.
 * @param <T> the type to validate
 *
 * @author Achraf Achkari
 */
public interface StructureValidator<T> {


    /**
     * Validate the given object.
     * @param object the object to validate
     * @return {@link ObjectResult} containing the result of the validation
     */
    ObjectResult validate(T object);


    /**
     * Assert that the given object is valid.
     * @param object the object to validate
     * @throws ValidationException if the object is not valid
     */
    void assertValid(T object);

    /**
     * Check if the given object is valid.
     * @param object the object to validate
     * @return true if the object is valid, false otherwise
     */
    boolean isValid(T object);
}
