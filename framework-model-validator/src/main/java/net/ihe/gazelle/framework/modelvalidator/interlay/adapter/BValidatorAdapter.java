package net.ihe.gazelle.framework.modelvalidator.interlay.adapter;

import io.github.ceoche.bvalid.BValidator;
import net.ihe.gazelle.framework.modelvalidator.domain.ValidationException;
import net.ihe.gazelle.framework.modelvalidator.domain.Validator;
import net.ihe.gazelle.framework.modelvalidator.domain.result.ObjectResult;
import net.ihe.gazelle.framework.modelvalidator.domain.result.RuleResult;

public class BValidatorAdapter <T> implements Validator<T> {

    private final BValidator<T> bValidator;

    public BValidatorAdapter(BValidator<T> bValidator) {
        this.bValidator = bValidator;
    }

    @Override
    public void assertValid(T object) {
        try{
            io.github.ceoche.bvalid.ObjectResult result = bValidator.validate(object);
            if(!result.isValid()){
                throw new IllegalArgumentException("Invalid Object: \n"+result);
            }
        }
        catch (Exception e){
            throw new ValidationException(e);
        }
    }

    @Override
    public void assertValid(T[] object) {
        for (T t : object) {
            this.assertValid(t);
        }
    }

    @Override
    public boolean isValid(T object) {
        return bValidator.validate(object).isValid();
    }

    @Override
    public boolean isValid(T[] object) {
        return bValidator.validate(object).stream().allMatch(io.github.ceoche.bvalid.ObjectResult::isValid);
    }

    @Override
    public ObjectResult validate(T object) {
        io.github.ceoche.bvalid.ObjectResult result = bValidator.validate(object);
        return mapResult(result);
    }

    private ObjectResult mapResult(io.github.ceoche.bvalid.ObjectResult result){
        ObjectResult objectResult = new ObjectResult(result.getBusinessObjectName());
        result.getRuleResults().forEach(ruleResult -> objectResult.addRuleResult(new RuleResult(ruleResult.getId(),
                ruleResult.getDescription(), ruleResult.isValid())));
        result.getMemberResults().forEach(memberResult -> objectResult.addMemberResult(mapResult(memberResult)));
        return objectResult;
    }
}
