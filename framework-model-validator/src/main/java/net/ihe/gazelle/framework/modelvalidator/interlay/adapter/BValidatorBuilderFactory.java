package net.ihe.gazelle.framework.modelvalidator.interlay.adapter;

import io.github.ceoche.bvalid.BValidatorManualBuilder;
import net.ihe.gazelle.framework.modelvalidator.domain.ValidatorBuilder;
import net.ihe.gazelle.framework.modelvalidator.domain.ValidatorBuilderFactory;

public class BValidatorBuilderFactory implements ValidatorBuilderFactory {

    @Override
    public <T,R extends T> ValidatorBuilder<R> getBuilder(Class<? extends T> clazz) {
        return new BValidatorBuilderAdapter<>((new BValidatorManualBuilder<>((Class<R>) clazz)).setBusinessObjectName(clazz.getSimpleName()));
    }

}
