package net.ihe.gazelle.framework.modelvalidator.domain;

import java.lang.reflect.InvocationTargetException;

/**
 * This class is used with a template design pattern to build a validator for a given class.
 * Once extended, the class must implement the behavior of {@link #make()} to build the object
 * Using {@link #getObjectToBuild()}.
 * @param <T> the class to build
 *
 * @author Achraf Achkari
 */
@SuppressWarnings("unchecked")
public abstract class AbstractBuilder<T>   {

    protected final RootValidatorBuilder<?> rootValidatorBuilder;


    private final ValidatorBuilderFactory validatorBuilderFactory;

    protected T objectToBuild;


    public AbstractBuilder(ValidatorBuilderFactory validatorBuilderFactory) {
        this.rootValidatorBuilder = new RootValidatorBuilder<>(getParameterClass());
        this.validatorBuilderFactory = validatorBuilderFactory;
        if (getParameterClass() == null) {
            throw new IllegalArgumentException("type must not be null");
        }
        if(getBuilderVisitor() == null){
            throw new IllegalArgumentException("builderVisitor must not be null");
        }
    }

    protected abstract BuilderVisitor getBuilderVisitor();

    protected abstract Class<? extends T> getParameterClass();

    protected abstract void make();


    protected final <R extends T> R buildObject() throws ObjectBuildException{
        // Dynamic instantiation of the object to build
        try {
            objectToBuild = getParameterClass().getDeclaredConstructor().newInstance();
        } catch (InstantiationException | IllegalAccessException | NoSuchMethodException | InvocationTargetException e) {
            throw new ObjectBuildException("Error while building object of type " + getParameterClass().getName(), e);
        }
        make();
        return (R) objectToBuild;
    }



    public final <R extends T> AbstractBuilder<R> appendVisitor(BuilderVisitor builderVisitor){
        builderVisitor.visit(rootValidatorBuilder, validatorBuilderFactory);
        return (AbstractBuilder<R>) this;
    }

    public final <R extends T> R build(){
        R output = buildObject();
        getBuilderVisitor().visit(rootValidatorBuilder, validatorBuilderFactory);
        ((Validator<R>)(rootValidatorBuilder.build())).assertValid(output);
        return output;
    }



    protected final <R extends T> R getObjectToBuild(){
        return (R) objectToBuild;
    }

    protected static <R> R buildField(AbstractBuilder<R> builder){
        if (builder != null) {
            return builder.buildObject();
        }
        return null;
    }




}
