package net.ihe.gazelle.framework.modelvalidator.domain;

public interface ValidatorBuilderFactory {

     <T, R extends T> ValidatorBuilder<R> getBuilder(Class<? extends T> clazz);

}
