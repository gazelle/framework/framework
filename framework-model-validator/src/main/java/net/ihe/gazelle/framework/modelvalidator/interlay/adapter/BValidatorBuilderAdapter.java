package net.ihe.gazelle.framework.modelvalidator.interlay.adapter;

import io.github.ceoche.bvalid.BValidatorBuilder;
import io.github.ceoche.bvalid.BValidatorManualBuilder;
import net.ihe.gazelle.framework.modelvalidator.domain.AbstractValidatorBuilder;
import net.ihe.gazelle.framework.modelvalidator.domain.Validator;
import net.ihe.gazelle.framework.modelvalidator.domain.ValidatorBuilder;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.function.Predicate;

public class BValidatorBuilderAdapter<T> extends AbstractValidatorBuilder<T> {

    private final BValidatorManualBuilder<T> bValidatorBuilder;

    private final List<MemberWrapper<T,?>> memberWrappers;


    public BValidatorBuilderAdapter(BValidatorManualBuilder<T> validatorBuilder) {
        this.bValidatorBuilder = validatorBuilder;
        memberWrappers = new ArrayList<>();
    }


    @Override
    public ValidatorBuilder<T> addRule(String id, Predicate<T> rule, String description) {
        bValidatorBuilder.addRule(id, rule, description);
        return this;
    }

    @Override
    public <R> ValidatorBuilder<T> addMember(String name, Function<T, ?> getter, Class<? extends R> memberClass) {
        // TODO: 10/03/2023 Perform a type checking between the memberClass and the getter return type
        memberWrappers.add(new MemberWrapper<>(name, getter, memberClass));
        return this;
    }



    @Override
    protected Validator<T> build(Map<Class<?>,AbstractValidatorBuilder<?>> validatorBuilders) {
        for(AbstractValidatorBuilder<?> validatorBuilder : validatorBuilders.values()){
            ((BValidatorBuilderAdapter<?>) validatorBuilder).addMembersToBValidatorBuilder(validatorBuilders);
        }
        return new BValidatorAdapter<>(bValidatorBuilder.build());
    }


    private <R> BValidatorBuilder<?>[] getValidatorBuilders(Class<? extends R> memberClass, Map<Class<?>,AbstractValidatorBuilder<?>> validatorBuilders) {
        return validatorBuilders.entrySet().stream()
                .filter(entry -> memberClass.isAssignableFrom(entry.getKey())) // Giving all possible implementations, bvalid behind will choose the CLOSEST one
                .map(entry -> ((BValidatorBuilderAdapter<R>) entry.getValue()).bValidatorBuilder)
                .toArray(BValidatorBuilder[]::new);
    }

    private void addMembersToBValidatorBuilder(Map<Class<?>,AbstractValidatorBuilder<?>> validatorBuilders) {
        for (MemberWrapper<T,?> memberWrapper : memberWrappers) {
            BValidatorBuilder<?>[] bValidatorBuilders = getValidatorBuilders(memberWrapper.memberClass(), validatorBuilders);
            this.bValidatorBuilder.addMember(memberWrapper.name(), memberWrapper.getGetter(), bValidatorBuilders);
        }
    }

    private record MemberWrapper<T, R>(String name, Function<T, ?> getter, Class<? extends R> memberClass) {

        public <P> Function<T, P> getGetter() {
                return (Function<T, P>) getter;
            }


        }
}
