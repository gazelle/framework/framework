package net.ihe.gazelle.framework.modelvalidator.application;

import net.ihe.gazelle.framework.modelvalidator.domain.StructureValidator;

/**
 * Factory to create a {@link StructureValidator} for a given type.
 * @param <T> the type of the structure to validate
 */
public interface StructureValidatorFactory<T> {


    /**
     * Create a {@link StructureValidator} for the given type.
     * @return the {@link StructureValidator}
     */
    StructureValidator<T> createStructureValidator();


    /**
     * Get the type of the structure to validate.
     * @return the type of the structure to validate
     */
    Class<T> getType();

    /**
     * Get a simplified name of the structure to validate.
     * @return the name of the structure to validate
     */
    String getName();
}
