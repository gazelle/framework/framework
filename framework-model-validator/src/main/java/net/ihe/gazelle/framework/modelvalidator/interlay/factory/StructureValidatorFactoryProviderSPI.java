package net.ihe.gazelle.framework.modelvalidator.interlay.factory;

import net.ihe.gazelle.framework.modelvalidator.application.StructureValidatorFactory;
import net.ihe.gazelle.framework.modelvalidator.application.StructureValidatorFactoryProvider;

import java.util.ServiceLoader;

public class StructureValidatorFactoryProviderSPI implements StructureValidatorFactoryProvider {

    @Override
    public <T> StructureValidatorFactory<T> getStructureValidatorFactory(Class<T> type) {
        ServiceLoader<StructureValidatorFactory> loader = ServiceLoader.load(StructureValidatorFactory.class);
        for (StructureValidatorFactory structureValidatorFactory : loader) {
            if (structureValidatorFactory.getType().equals(type)) {
                return structureValidatorFactory;
            }
        }
        throw new IllegalArgumentException("No StructureValidatorFactory found for type " + type);
    }

    @Override
    public <T> StructureValidatorFactory<T> getStructureValidatorFactory(String name) {
        ServiceLoader<StructureValidatorFactory> loader = ServiceLoader.load(StructureValidatorFactory.class);
        for (StructureValidatorFactory structureValidatorFactory : loader) {
            if (structureValidatorFactory.getName().equals(name)) {
                return structureValidatorFactory;
            }
        }
        throw new IllegalArgumentException("No StructureValidatorFactory found with name " + name);
    }
}
