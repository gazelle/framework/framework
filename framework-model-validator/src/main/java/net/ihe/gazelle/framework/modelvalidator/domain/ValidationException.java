package net.ihe.gazelle.framework.modelvalidator.domain;

public class ValidationException extends RuntimeException {

    private static final long serialVersionUID = 4980378041217263821L;


    public ValidationException(String message) {
        super(message);
    }

    public ValidationException(String message, Throwable cause) {
        super(message, cause);
    }

    public ValidationException(Throwable cause) {
        super(cause);
    }
}
