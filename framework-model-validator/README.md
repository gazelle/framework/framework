# Framework Model Validator

## 1. Introduction

The framework model validator is a library that provides you a fluent API to build & validate your domain objects.

It is based on the [BValid Project](https://github.com/ceoche/bvalid/) engine.

This library is basically an adapter between the BValid engine and the Gazelle framework.

## 2. Usage

To create a structure builder, there is 4 steps to follow:
- Create the structure
- Create the validator supplier for your structure
- Create the visitor to visit your structure and aggregate rules
- Create the builder to build your structure

The order of the steps is important.

### Explanation
Before going into the details of each step, let's explain the purpose all this work.

The purpose of the framework model validator is to provide a way to validate your domain objects.
The engine used behind the scene is the [BValid Project](https://github.com/ceoche/bvalid/). The purpose
of this module is to define an adapter between the BValid engine and the Gazelle framework.

Not only this, but the framework provides also the structure builder to create your structure. As building 
a wrong structure is not a good idea, the framework validate automatically the structure at build time.
So the validation suppliers provides the rules to validate your structure, and then used by the structure builder at
build time.

The challenge was to make the framework extensible and generic, so the rules & builders could be reused, composed and
extended. To achieve this without any coupling to an injection context framework,
we used the visitor pattern with and a flattenization technique.

The rules are natively constructor with BValid in a tree structure, hence, this cause extensibility issues as
a parent object cannot validate a new added children in another module. To solve this, we used 
the RootValidatorBuilder to flattenize the rules in a Map of rules. This way, the rules are not anymore in a tree
structure, but in a Map where the key is the exact type of the structure and the value is its rules (wrapped in a validator builder).

This gives us the possibility of adding new rules to an existing structure without any coupling to the structure itself, 
at moment at runtime.


### 2.1. Create the structure

There is nothing special to do here, just create your structure.

Add some rules to your structure.

> We suggest you to put the rules with the structure, but you can also put them in a separate class.


```java
package org.example.domain;

public class Person {

    private String name;
    
    private int age;

    public String getName() {
        return name;
    }

    public Person setName(String name) {
        this.name = name;
        return this;
    }
    
    public int getAge() {
        return age;
    }
    
    public Person setAge(int age) {
        this.age = age;
        return this;
    }
    
    // Add some rules here
    
    public boolean isNameValid() {
        return name != null && !name.isEmpty();
    }
    
    public boolean isAgeValid() {
        return age > 0;
    }
    
    // ...
}
```

### 2.2. Create the validator supplier for your structure

The validator supplier is a class that implements the interface `net.ihe.gazelle.framework.modelvalidator.domain.AbstractValidatorBuilderSupplier<T>`.
Its main purpose is to assemble the rules of your structure.

You have to override 2 methods:

```java
 Class<? extends AbstractParentMock> getType();
// The getType method returns the exact type (or subtype) of the structure you want to validate.
// It is used to deal with polymorphism.
```
```java
 ValidatorBuilder<AbstractParentMock> supplyRulesAndMembers(ValidatorBuilder<AbstractParentMock> validatorBuilder);
// The supplyRulesAndMembers method is used to assemble the rules of your structure.
```


And you have to provide a constructor that takes a `ValidatorBuilderFactory` as parameter.

```java
AbstractValidatorBuilderSupplier(ValidatorBuilderFactory validatorBuilderFactory);
// The validatorBuilderFactory is used to create the validator builder.
```

The full implementation of the validator supplier is:

```java

package org.example.domain;

import net.ihe.gazelle.framework.modelvalidator.domain.AbstractValidatorBuilderSupplier;
import net.ihe.gazelle.framework.modelvalidator.domain.ValidatorBuilder;
import net.ihe.gazelle.framework.modelvalidator.domain.ValidatorBuilderFactory;

import org.example.domain.Person;

public class PersonValidatorBuilderSupplier extends AbstractValidatorBuilderSupplier<Person> {


    public PersonValidatorBuilderSupplier(ValidatorBuilderFactory validatorBuilderFactory) {
        super(validatorBuilderFactory);
    }

    @Override
    protected Class<? extends Person> getType() {
        return Person.class;
    }

    @Override
    protected ValidatorBuilder<Person> supplyRulesAndMembers(ValidatorBuilder<Person> validatorBuilder) {
        return validatorBuilder
                .addRule("nameValid", Person::isNameValid, "Name attribute must be valid")
                .addRule("ageValid", Person::isAgeValid, "Age attribute must be valid");
                ;
    }
}
        

```

### 2.3. Create the visitor to visit your structure and aggregate rules

The visitor is a class that extends the abstract class `net.ihe.gazelle.framework.modelvalidator.domain.BuilderVisitor`.
Its main purpose is to visit the composite attribute of your structure and aggregate rules.

The visitors concept is used to support the extensibility of builders and validators, 
by flattening the rules of the composite attributes to a single level (root level).

You have to override 1 method:

```java

void visit(RootValidatorBuilder<?> rootValidatorBuilder, ValidatorBuilderFactory validatorBuilderFactory);

// The visit method is used to visit the composite attributes of your structure and aggregate rules.
// The rootValidatorBuilder is the root builder of your structure. (see next sections)
// The validatorBuilderFactory is used to create the validator builder.
```

> The visitor is a singleton.

The full implementation of the visitor is:

```java

package org.example.application;

import net.ihe.gazelle.framework.modelvalidator.domain.BuilderVisitor;
import net.ihe.gazelle.framework.modelvalidator.domain.ValidatorBuilder;
import net.ihe.gazelle.framework.modelvalidator.domain.ValidatorBuilderFactory;

import org.example.domain.Person;
import org.example.domain.PersonValidatorBuilderSupplier;

public class PersonVisitor implements BuilderVisitor {

    public static PersonVisitor INSTANCE = new PersonVisitor();

    @Override
    public void visit(RootValidatorBuilder<?> rootValidatorBuilder, ValidatorBuilderFactory validatorBuilderFactory) {
        rootValidatorBuilder.addValidatorBuilder(Person.class, new PersonValidatorBuilderSupplier(validatorBuilderFactory));
    }
}
        

```

### 2.4. Create the builder to build your structure

The builder is a class that extends the abstract class `net.ihe.gazelle.framework.modelvalidator.domain.AbstractObjectBuilder<T>`.

Its main purpose is to build your structure, assemble attributes and validate the object.

You have to override 2 methods:

```java
BuilderVisitor getBuilderVisitor();
// The getBuilderVisitor method returns the visitor of your structure.
// It's often a call to a singleton. (eg: MyStructureVisitor.INSTANCE)
```

```java
void make();
// The make method is used to assemble attributes.
```

And 1 constructor:

```java
AbstractObjectBuilder(ValidatorBuilderFactory validatorBuilderFactory)
// The validatorBuilderFactory is used to create the validator builder.
```

>The builder should redeclare all the attributes of the structure (see the implementation).


The builder apply the template design pattern.

The full implementation of the builder is:

```java

package org.example.application;

import net.ihe.gazelle.framework.modelvalidator.domain.AbstractBuilder;
import net.ihe.gazelle.framework.modelvalidator.domain.BuilderVisitor;
import net.ihe.gazelle.framework.modelvalidator.domain.ValidatorBuilderFactory;

import org.example.domain.Person;
import org.example.domain.PersonVisitor;

public class PersonBuilder extends AbstractBuilder<Person> {

    private String name;

    private int age;

    public PersonBuilder(ValidatorBuilderFactory validatorBuilderFactory) {
        super(validatorBuilderFactory);
    }

    @Override
    public BuilderVisitor getBuilderVisitor() {
        return PersonVisitor.INSTANCE;
    }

    @Override
    public void make() {
        // Add some attributes here
    }

    public PersonBuilder setName(String name) {
        this.name = name;
        return this;
    }

    public PersonBuilder setAge(int age) {
        this.age = age;
        return this;
    }
}


```

### 2.5. Use the builder

After doing all the previous steps (in the same order), you can use the builder to build & validate your structure.

```java

package org.example.domain;

import net.ihe.gazelle.framework.modelvalidator.domain.ValidatorBuilderFactory;
import net.ihe.gazelle.framework.modelvalidator.interlay.adapter.BValidatorBuilderFactory;

import org.example.domain.Person;
import org.example.domain.PersonBuilder;

public class Main {

    public static void main(String[] args) {
        ValidatorBuilderFactory validatorBuilderFactory = new BValidatorBuilderFactory();
        // The BValidatorBuilderFactory is a class that implements the interface ValidatorBuilderFactory.

        Person Person = new PersonBuilder(validatorBuilderFactory)
                .setName("John")
                .setAge(42)
                .build();
        // The object is built and validated.
    }
}

```

When there is no error, the object is built and validated.

When there is an error, a `ValidationException` is thrown. If we provide an invalid age,
the error message is:

```
Invalid Object: 
Person [nameValid] Name attribute must be valid => invalid

```

## 3. Extensions and polymorphism

A complete example of an extension usecase is provided in unit tests: `net.ihe.gazelle.framework.modelvalidator.mock.extension`.

Same as for polymorphism: `net.ihe.gazelle.framework.modelvalidator.mock.polymorphism`.


## 4. Assembled Validator

An optional step is to create an assembled validator using the API `net.ihe.gazelle.framework.modelvalidator.domain.StructureValidator`.
This is just a refactoring step to avoid code duplication and usability.

It's also used for CLI validations.

You can create your own assembled validator by implementing the interface `net.ihe.gazelle.framework.modelvalidator.domain.StructureValidator`.

The full implementation of the assembled validator is:

```java

package org.example.application;

import net.ihe.gazelle.framework.modelvalidator.domain.StructureValidator;
import org.example.domain.Person;

public class PersonValidator extends StructureValidator<Person> {

    private final Validator<Person> validator;

    public PersonValidator(ValidatorBuilderFactory validatorBuilderFactory) {
        RootValidatorBuilder<Person> validatorBuilder = new RootValidatorBuilder<>(Person.class);
        PersonVisitor.INSTANCE.visit(validatorBuilder, validatorBuilderFactory);
        this.validator = validatorBuilder.build();
    }

    @Override
    public ObjectResult validate(Person object) {
        return validator.validate(object);
    }

    @Override
    public void assertValid(Person object) {
        validator.assertValid(object);
    }

    @Override
    public boolean isValid(Person object) {
        return validator.isValid(object);
    }
}

```

## 5. CLI

A CLI is provided to validate a JSON file against provided rules of a structure.

The structure & the StructureValidator must be available in the classpath.

The CLI is available in the jar file `net.ihe.gazelle.framework.modelvalidator.interlay.cli.StructureValidatorCLI`.

The CLI must take at least one argument, the full canonical name of the StructureValidator or its simple name.
The Json path is considered as an optional argument and could be provided from STDIN.

The CLI prints the result of the validation in the STDOUT.

It's recommended to use maven exec plugin to run the CLI.

This is the full usage:

```bash
mvn exec:java \
      -Dexec.mainClass="net.ihe.gazelle.framework.modelvalidator.interlay.cli.StructureValidatorCLI" \
      -Dexec.args="[-f <path_to_validation_report>] -t canonical_name_of_the_structure "
```

or

```bash
mvn exec:java \ 
      -Dexec.mainClass="net.ihe.gazelle.framework.modelvalidator.interlay.cli.StructureValidatorCLI" \
      -Dexec.args="[-f <path_to_validation_report>] -n name_of_the_structure"
```

You can run -h or --help to get the usage.

To simplify the classpath inclusion, you can create an extension of the CLI class as an empty class.

```java

package org.example.interlay;

import net.ihe.gazelle.framework.modelvalidator.interlay.cli.StructureValidatorCLI;

public class MyProjectStructureValidatorCLI extends StructureValidatorCLI {
}

```

Then you can run the CLI with:

```bash
mvn exec:java \
      -Dexec.mainClass="org.example.interlay.MyProjectStructureValidatorCLI" \
      -Dexec.args="[-f <path_to_validation_report>] -t canonical_name_of_the_structure "
```

example with the Person structure:

```bash
mvn exec:java \
      -Dexec.mainClass="org.example.interlay.MyProjectStructureValidatorCLI" \
      -Dexec.args="-f src/main/resources/person.json -t org.example.application.PersonValidator"
```