# Framework Model Marshaller

This module contains the serializers and deserializers for structures.

The module provides an API StructureMapper as an entry point to marshall/unmarshall structures.
In addition, it provides an ObjectMapperBuilder to build the instance of the engine marshaller, used
by the implementation of the StructureMapper.

## Usage

Having a predefined implementation of ObjectMapperBuilder for Json use case `JsonObjectMapperBuilder` and 
the main API implementation for json `JsonStructureMapper`:

```java

import net.ihe.gazelle.modelmarshaller.application.StructureMapper;
import net.ihe.gazelle.modelmarshaller.interlay.factory.JsonObjectMapperBuilder;
import net.ihe.gazelle.modelmarshaller.interlay.mapper.JsonStructureMapper;

public class Example{
    public static void main(String[] args) {
        StructureMapper structureMapper = new JsonStructureMapper(new JsonObjectMapperBuilder().build());
        
        //marshall
        Structure structure = new Structure();
        String json = structureMapper.marshall(structure);
        
        //unmarshall
        String json = "{\"name\":\"structure\"}";
        Structure structure = structureMapper.unmarshall(json, Structure.class);
        
    }

}

```

The `new JsonObjectMapperBuilder().build()` provides a configured instance of Json ObjectMapper with the
following options disabled: 
- `AUTO_DETECT_CREATORS`
- `AUTO_DETECT_FIELDS`
- `AUTO_DETECT_GETTERS`
- `AUTO_DETECT_IS_GETTERS`

## Customization

You can create your own ObjectMapperBuilder to customize the ObjectMapper instance used by the StructureMapper.

```java

import com.fasterxml.jackson.databind.ObjectMapper;
import net.ihe.gazelle.modelmarshaller.application.ObjectMapperBuilder;

public class CustomObjectMapperBuilder implements ObjectMapperBuilder<ObjectMapper> {
    @Override
    public ObjectMapper build() {
        ObjectMapper<ObjectMapper> objectMapper = new ObjectMapper();
        //customize the ObjectMapper instance
        return objectMapper;
    }
}

```

## Other formats

The module only provides a Json implementation of the StructureMapper, but you can create your own implementations
for other formats like XML, YAML, etc.



