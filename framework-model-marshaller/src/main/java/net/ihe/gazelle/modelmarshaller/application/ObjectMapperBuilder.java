package net.ihe.gazelle.modelmarshaller.application;


public interface ObjectMapperBuilder<T> {

    T build();
}
