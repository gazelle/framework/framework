package net.ihe.gazelle.modelmarshaller.interlay.mapper;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import net.ihe.gazelle.modelmarshaller.application.StructureMapper;

public class JsonStructureMapper implements StructureMapper {

    private final ObjectMapper objectMapper;

    public JsonStructureMapper(ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
    }

    @Override
    public <T> String marshall(T object) throws MarshallingException {
        try {
            return objectMapper.writeValueAsString(object);
        } catch (JsonProcessingException e) {
            throw new MarshallingException("Error while marshalling " +object.getClass().getSimpleName(), e);
        }
    }

    @Override
    public <T> T unmarshall(String object, Class<T> clazz) throws UnmarshallingException {
        try {
            return objectMapper.readValue(object, clazz);
        } catch (JsonProcessingException e) {
            throw new UnmarshallingException("Error while unmarshalling JSON to "+ clazz.getSimpleName(), e);
        }
    }

}
