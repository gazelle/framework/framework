package net.ihe.gazelle.modelmarshaller.interlay.dto;

public interface DTOFactoryProvider<T> {

    <R> DTOFactory<T, R> createFactory(Class<R> type);
}
