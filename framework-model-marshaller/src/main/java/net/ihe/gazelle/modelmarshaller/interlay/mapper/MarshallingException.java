package net.ihe.gazelle.modelmarshaller.interlay.mapper;

public class MarshallingException extends RuntimeException {

    public MarshallingException(String message, Throwable cause) {
        super(message, cause);
    }
}
