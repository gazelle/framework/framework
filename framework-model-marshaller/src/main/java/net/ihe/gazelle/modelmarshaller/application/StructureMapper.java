package net.ihe.gazelle.modelmarshaller.application;

import net.ihe.gazelle.modelmarshaller.interlay.mapper.MarshallingException;
import net.ihe.gazelle.modelmarshaller.interlay.mapper.UnmarshallingException;

public interface StructureMapper {

    <T> String marshall(T object) throws MarshallingException;

    <T> T unmarshall(String object, Class<T> clazz) throws UnmarshallingException;
}
