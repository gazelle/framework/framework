package net.ihe.gazelle.modelmarshaller.interlay.dto;

public interface DTOFactory<T, R> {

    T createDTO(R object);

    Class<R> getType();
}
