package net.ihe.gazelle.modelmarshaller.interlay.mapper;

import java.util.ServiceLoader;

public class StructureJsonFactoryProviderSPI<T> implements StructureJsonFactoryProvider<T>{

    @SuppressWarnings({"unchecked", "rawtypes"})
    @Override
    public StructureJsonFactory<T> getFactory(String type) {
        ServiceLoader<StructureJsonFactory> factories = ServiceLoader.load(StructureJsonFactory.class);
        return factories
                .stream()
                .map(ServiceLoader.Provider::get)
                .filter(factory -> factory.getType().equals(type))
                .findFirst()
                .orElseThrow(()-> new IllegalArgumentException("No Json Factory implementation found for type " + type));
    }
}
