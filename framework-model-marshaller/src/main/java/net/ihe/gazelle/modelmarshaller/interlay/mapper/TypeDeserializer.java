package net.ihe.gazelle.modelmarshaller.interlay.mapper;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import com.fasterxml.jackson.databind.node.ObjectNode;

import java.io.IOException;
import java.io.Serial;

/**
 * This class is used to deserialize a json object into a java object.
 * It supports polymorphism by using a type attribute in the json object.
 * This mechanism does not break extensibility of structures.
 * @param <T> The type of the java object to deserialize
 *
 * @author Achraf Achkari
 */
public abstract class TypeDeserializer<T> extends StdDeserializer<T> {

    @Serial
    private static final long serialVersionUID = 3063052451904073919L;
    public static final String TYPE_FIELD = "type";

    protected TypeDeserializer(Class<T> clazz) {
        super(clazz);
    }

    @Override
    public T deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
        ObjectNode node = jsonParser.getCodec().readTree(jsonParser);

        if(!node.has(TYPE_FIELD)) {
            throw new UnmarshallingException("No type attribute found in json");
        }
        if(node.get(TYPE_FIELD).isNull() || !node.get(TYPE_FIELD).isTextual()) {
            throw new UnmarshallingException("Type attribute must be a string");
        }

        String type = node.get(TYPE_FIELD).asText();

        return getStructureFactoryProvider().getFactory(type).createStructure(node);
    }


    // fixme: 07/09/2023 This is used to have explicit type in the @JsonDeserialize annotation

    protected abstract <R extends T> StructureJsonFactoryProvider<R> getStructureFactoryProvider();


}
