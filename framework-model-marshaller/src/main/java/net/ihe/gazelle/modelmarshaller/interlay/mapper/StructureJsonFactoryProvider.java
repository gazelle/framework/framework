package net.ihe.gazelle.modelmarshaller.interlay.mapper;


/**
 * The API to assemble a factory for a given type.
 * @param <T> the type of the structure to create its factory
 */
public interface StructureJsonFactoryProvider<T> {

    StructureJsonFactory<T> getFactory(String type);
}
