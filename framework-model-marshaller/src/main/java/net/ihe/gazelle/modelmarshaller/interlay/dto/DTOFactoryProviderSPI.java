package net.ihe.gazelle.modelmarshaller.interlay.dto;

import java.util.ServiceLoader;

public class DTOFactoryProviderSPI<T> implements DTOFactoryProvider<T>{


    @SuppressWarnings({"unchecked", "rawtypes"})
    @Override
    public <R> DTOFactory<T, R> createFactory(Class<R> type) {
        ServiceLoader<DTOFactory> factories = ServiceLoader.load(DTOFactory.class);
        return factories
                .stream()
                .map(ServiceLoader.Provider::get)
                .filter(factory -> factory.getType().equals(type))
                .findFirst()
                .orElseThrow(()-> new IllegalArgumentException("No DTO Factory implementation found for type " + type));
    }
}
