package net.ihe.gazelle.modelmarshaller.interlay.mapper;

import com.fasterxml.jackson.databind.node.ObjectNode;

public interface StructureJsonFactory <T> {

    T createStructure(ObjectNode objectNode);

    String getType();
}
