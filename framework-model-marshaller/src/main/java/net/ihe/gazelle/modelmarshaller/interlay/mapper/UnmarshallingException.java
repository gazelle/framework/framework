package net.ihe.gazelle.modelmarshaller.interlay.mapper;

import java.io.Serial;

public class UnmarshallingException extends RuntimeException {

    @Serial
    private static final long serialVersionUID = -2647321947221257707L;

    public UnmarshallingException(String message, Throwable cause) {
        super(message, cause);
    }

    public UnmarshallingException(String message) {
        super(message);
    }

}

