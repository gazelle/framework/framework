package net.ihe.gazelle.modelmarshaller.interlay.factory;

import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.json.JsonMapper;
import net.ihe.gazelle.modelmarshaller.application.ObjectMapperBuilder;

public class JsonObjectMapperBuilder implements ObjectMapperBuilder<ObjectMapper> {

    private final JsonMapper.Builder objectMapperBuilder;

    public JsonObjectMapperBuilder() {
        this.objectMapperBuilder = JsonMapper
                .builder()
                .disable(
                        MapperFeature.AUTO_DETECT_CREATORS,
                        MapperFeature.AUTO_DETECT_FIELDS,
                        MapperFeature.AUTO_DETECT_GETTERS,
                        MapperFeature.AUTO_DETECT_IS_GETTERS);
    }

    @Override
    public ObjectMapper build() {
        return objectMapperBuilder.build();
    }
}
