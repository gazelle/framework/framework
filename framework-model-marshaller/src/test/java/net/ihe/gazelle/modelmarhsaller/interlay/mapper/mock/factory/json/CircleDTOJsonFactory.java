package net.ihe.gazelle.modelmarhsaller.interlay.mapper.mock.factory.json;

import com.fasterxml.jackson.databind.node.ObjectNode;
import net.ihe.gazelle.modelmarhsaller.interlay.mapper.mock.dto.CircleDTO;
import net.ihe.gazelle.modelmarshaller.interlay.mapper.StructureJsonFactory;

public class CircleDTOJsonFactory implements StructureJsonFactory<CircleDTO> {


    @Override
    public CircleDTO createStructure(ObjectNode objectNode) {
        return new CircleDTO()
                .setName(objectNode.get("name").asText())
                .setRadius(objectNode.get("radius").asInt())
                ;
    }

    @Override
    public String getType() {
        return "circle";
    }
}
