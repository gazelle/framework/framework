package net.ihe.gazelle.modelmarhsaller.interlay.mapper.mock.factory.json;

import com.fasterxml.jackson.databind.node.ObjectNode;
import net.ihe.gazelle.modelmarhsaller.interlay.mapper.mock.structure.Person;
import net.ihe.gazelle.modelmarshaller.interlay.mapper.StructureJsonFactory;

public class PersonJsonFactory implements StructureJsonFactory<Person> {

    @Override
    public Person createStructure(ObjectNode objectNode) {
        return new Person()
                .setName(objectNode.get("name").asText())
                .setAge(objectNode.get("age").asInt())
                ;
    }

    @Override
    public String getType() {
        return "person";
    }

}
