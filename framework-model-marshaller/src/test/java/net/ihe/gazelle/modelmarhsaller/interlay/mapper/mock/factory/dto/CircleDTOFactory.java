package net.ihe.gazelle.modelmarhsaller.interlay.mapper.mock.factory.dto;

import net.ihe.gazelle.modelmarhsaller.interlay.mapper.mock.dto.CircleDTO;
import net.ihe.gazelle.modelmarhsaller.interlay.mapper.mock.structure.Circle;
import net.ihe.gazelle.modelmarshaller.interlay.dto.DTOFactory;

public class CircleDTOFactory implements DTOFactory<CircleDTO, Circle> {

    @Override
    public CircleDTO createDTO(Circle object) {
        return new CircleDTO()
                .setName(object.getName())
                .setRadius(object.getRadius())
                ;
    }

    @Override
    public Class<Circle> getType() {
        return Circle.class;
    }
}
