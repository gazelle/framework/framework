package net.ihe.gazelle.modelmarhsaller.interlay.mapper.mock;

import net.ihe.gazelle.modelmarhsaller.interlay.mapper.mock.structure.Shape;
import net.ihe.gazelle.modelmarshaller.interlay.mapper.StructureJsonFactoryProvider;
import net.ihe.gazelle.modelmarshaller.interlay.mapper.StructureJsonFactoryProviderSPI;
import net.ihe.gazelle.modelmarshaller.interlay.mapper.TypeDeserializer;

import java.io.Serial;

public class ShapeDeserializer extends TypeDeserializer<Shape> {


    @Serial
    private static final long serialVersionUID = -240657766240000141L;
    private final StructureJsonFactoryProvider<Shape> structureFactoryProvider = new StructureJsonFactoryProviderSPI<>();

    public ShapeDeserializer() {
        super(Shape.class);
    }

    @Override
    protected StructureJsonFactoryProvider<? extends Shape> getStructureFactoryProvider() {
        return structureFactoryProvider;
    }
}
