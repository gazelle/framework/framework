package net.ihe.gazelle.modelmarhsaller.interlay.mapper.mock.factory.dto;

import net.ihe.gazelle.modelmarhsaller.interlay.mapper.mock.structure.Shape;
import net.ihe.gazelle.modelmarhsaller.interlay.mapper.mock.dto.ShapeDTO;
import net.ihe.gazelle.modelmarshaller.interlay.dto.DTOFactory;

public class ShapeDTOFactory implements DTOFactory<ShapeDTO, Shape> {

    @Override
    public ShapeDTO createDTO(Shape object) {
        return new ShapeDTO()
                .setName(object.getName());
    }

    @Override
    public Class<Shape> getType() {
        return Shape.class;
    }
}

