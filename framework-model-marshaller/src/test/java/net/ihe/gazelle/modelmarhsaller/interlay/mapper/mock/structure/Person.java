package net.ihe.gazelle.modelmarhsaller.interlay.mapper.mock.structure;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Person {

    private String name;

    private int age;

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    public Person setName(String name) {
        this.name = name;
        return this;
    }

    @JsonProperty("age")
    public int getAge() {
        return age;
    }

    public Person setAge(int age) {
        this.age = age;
        return this;
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';
    }
}
