package net.ihe.gazelle.modelmarhsaller.interlay.mapper.mock.structure;

public class Shape {


    private String name;

    public String getName() {
        return name;
    }

    public Shape setName(String name) {
        this.name = name;
        return this;
    }


}
