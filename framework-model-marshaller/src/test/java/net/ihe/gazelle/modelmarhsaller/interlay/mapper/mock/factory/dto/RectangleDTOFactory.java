package net.ihe.gazelle.modelmarhsaller.interlay.mapper.mock.factory.dto;

import net.ihe.gazelle.modelmarhsaller.interlay.mapper.mock.dto.RectangleDTO;
import net.ihe.gazelle.modelmarhsaller.interlay.mapper.mock.structure.Rectangle;
import net.ihe.gazelle.modelmarshaller.interlay.dto.DTOFactory;

public class RectangleDTOFactory implements DTOFactory<RectangleDTO, Rectangle> {

    @Override
    public RectangleDTO createDTO(Rectangle object) {
        return new RectangleDTO()
                .setName(object.getName())
                .setHeight(object.getHeight())
                .setWidth(object.getWidth())
                ;
    }

    @Override
    public Class<Rectangle> getType() {
        return Rectangle.class;
    }
}
