package net.ihe.gazelle.modelmarhsaller.interlay.mapper.mock.dto;

public class RectangleDTO extends ShapeDTO {

    private int width;

    private int height;

    public RectangleDTO() {
        super.setType("rectangle");
    }

    public int getWidth() {
        return width;
    }

    public RectangleDTO setWidth(int width) {
        this.width = width;
        return this;
    }

    public int getHeight() {
        return height;
    }

    public RectangleDTO setHeight(int height) {
        this.height = height;
        return this;
    }

    @Override
    public RectangleDTO setName(String name) {
        super.setName(name);
        return this;
    }
}
