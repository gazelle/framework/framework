package net.ihe.gazelle.modelmarhsaller.interlay.mapper.mock.dto;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import net.ihe.gazelle.modelmarhsaller.interlay.mapper.mock.ShapeDeserializer;

@JsonDeserialize(using = ShapeDeserializer.class)

public class ShapeDTO {

    private String type;

    private String name;

    public String getName() {
        return name;
    }

    public ShapeDTO setName(String name) {
        this.name = name;
        return this;
    }

    public String getType() {
        return type;
    }

    public ShapeDTO setType(String type) {
        this.type = type;
        return this;
    }
}
