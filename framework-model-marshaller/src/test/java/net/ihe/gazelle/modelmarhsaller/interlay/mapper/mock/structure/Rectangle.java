package net.ihe.gazelle.modelmarhsaller.interlay.mapper.mock.structure;

public class Rectangle extends Shape  {

    private int width;

    private int height;


    public int getWidth() {
        return width;
    }

    public Rectangle setWidth(int width) {
        this.width = width;
        return this;
    }

    public int getHeight() {
        return height;
    }

    public Rectangle setHeight(int height) {
        this.height = height;
        return this;
    }

    @Override
    public Rectangle setName(String name) {
        super.setName(name);
        return this;
    }

    @Override
    public String toString() {
        return "Rectangle{" +
                "width=" + width +
                ", height=" + height +
                '}';
    }
}
