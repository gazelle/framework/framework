package net.ihe.gazelle.modelmarhsaller.interlay.mapper;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import net.ihe.gazelle.modelmarhsaller.interlay.mapper.mock.structure.Person;
import net.ihe.gazelle.modelmarshaller.interlay.mapper.StructureJsonFactory;
import net.ihe.gazelle.modelmarshaller.interlay.mapper.StructureJsonFactoryProvider;
import net.ihe.gazelle.modelmarshaller.interlay.mapper.StructureJsonFactoryProviderSPI;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class StructureJsonFactoryProviderTest {

    @Test
    public void testGetFactory() throws JsonProcessingException {
        StructureJsonFactoryProvider<Person> structureJsonFactoryProvider = new StructureJsonFactoryProviderSPI<>();
        StructureJsonFactory<Person> factory = structureJsonFactoryProvider.getFactory("person");
        assertNotNull(factory);
        ObjectNode objectNode = (ObjectNode) (new ObjectMapper()).readTree("{\"name\":\"John\",\"age\":20}");
        Person person = factory.createStructure(objectNode);
        assertNotNull(person);
        assertEquals("John", person.getName());
        assertEquals(20, person.getAge());

    }
}
