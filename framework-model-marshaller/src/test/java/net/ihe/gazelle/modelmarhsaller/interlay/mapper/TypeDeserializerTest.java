package net.ihe.gazelle.modelmarhsaller.interlay.mapper;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import net.ihe.gazelle.modelmarhsaller.interlay.mapper.mock.*;
import net.ihe.gazelle.modelmarhsaller.interlay.mapper.mock.dto.CircleDTO;
import net.ihe.gazelle.modelmarhsaller.interlay.mapper.mock.dto.RectangleDTO;
import net.ihe.gazelle.modelmarhsaller.interlay.mapper.mock.dto.ShapeDTO;
import net.ihe.gazelle.modelmarshaller.interlay.mapper.UnmarshallingException;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class TypeDeserializerTest {

    private final ObjectMapper objectMapper = new ObjectMapper();

    @Test
    public void testDeserializeCircle() throws JsonProcessingException {
        String json = "{\"type\":\"circle\",\"name\":\"Acircle\",\"radius\":10}";
        ShapeDTO shape = objectMapper.readValue(json, ShapeDTO.class);
        assertInstanceOf(CircleDTO.class, shape);
        CircleDTO circle = (CircleDTO) shape;
        assertEquals("Acircle", circle.getName());
        assertEquals(10, circle.getRadius());

    }

    @Test
    public void testDeserializeRectangle() throws JsonProcessingException {
        String json = "{\"type\":\"rectangle\",\"name\":\"Arectangle\",\"width\":10,\"height\":20}";
        ShapeDTO shape = objectMapper.readValue(json, ShapeDTO.class);
        assertInstanceOf(RectangleDTO.class, shape);
        RectangleDTO rectangle = (RectangleDTO) shape;
        assertEquals("Arectangle", rectangle.getName());
        assertEquals(10, rectangle.getWidth());
        assertEquals(20, rectangle.getHeight());
    }

    @Test
    public void testDeserializeWithNoType(){
        String json = "{\"name\":\"Arectangle\",\"width\":10,\"height\":20}";
        Throwable throwable = assertThrows(UnmarshallingException.class, () -> objectMapper.readValue(json, ShapeDTO.class));
        assertEquals("No type attribute found in json", throwable.getMessage());
    }

    @Test
    public void testDeserializeWithUnknownType(){
        String json = "{\"type\":\"unknown\",\"name\":\"Arectangle\",\"width\":10,\"height\":20}";
        Throwable throwable = assertThrows(IllegalArgumentException.class, () -> objectMapper.readValue(json, ShapeDTO.class));
        assertEquals("No Json Factory implementation found for type unknown", throwable.getMessage());
    }

    @Test
    public void testDeserializeWithInvalidTypeType(){
        String json = "{\"type\":10,\"name\":\"Arectangle\",\"width\":10,\"height\":20}";
        Throwable throwable = assertThrows(UnmarshallingException.class, () -> objectMapper.readValue(json, ShapeDTO.class));
        assertEquals("Type attribute must be a string", throwable.getMessage());
    }

}
