package net.ihe.gazelle.modelmarhsaller.interlay.mapper;

import net.ihe.gazelle.modelmarhsaller.interlay.mapper.mock.dto.CircleDTO;
import net.ihe.gazelle.modelmarhsaller.interlay.mapper.mock.dto.FigureDTO;
import net.ihe.gazelle.modelmarhsaller.interlay.mapper.mock.dto.RectangleDTO;
import net.ihe.gazelle.modelmarhsaller.interlay.mapper.mock.dto.ShapeDTO;
import net.ihe.gazelle.modelmarhsaller.interlay.mapper.mock.structure.Circle;
import net.ihe.gazelle.modelmarhsaller.interlay.mapper.mock.structure.Figure;
import net.ihe.gazelle.modelmarhsaller.interlay.mapper.mock.structure.Rectangle;
import net.ihe.gazelle.modelmarhsaller.interlay.mapper.mock.structure.Shape;
import net.ihe.gazelle.modelmarshaller.interlay.dto.DTOFactoryProviderSPI;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertInstanceOf;

public class DTOFactoryProviderTest {

    @Test
    public void testCreateDTO(){
        DTOFactoryProviderSPI<ShapeDTO> shapeDTOFactoryProviderSPI = new DTOFactoryProviderSPI<>();
        ShapeDTO shapeDTO = shapeDTOFactoryProviderSPI.createFactory(Circle.class).createDTO(new Circle().setName("Acircle").setRadius(10));
        assertInstanceOf(CircleDTO.class, shapeDTO);
        CircleDTO circleDTO = (CircleDTO) shapeDTO;
        assertEquals("Acircle", circleDTO.getName());
        assertEquals(10, circleDTO.getRadius());
    }

    @Test
    public void testRealUseCaseWithPolymorphism(){
        Figure figure = new Figure()
                .addShape(new Circle().setName("Acircle").setRadius(10))
                .addShape(new Rectangle().setWidth(10).setHeight(20).setName("Arectangle"))
                .addShape(new Shape().setName("Ashape"));
        FigureDTO figureDTO = new FigureDTO(figure);
        assertEquals(3, figureDTO.getShapes().size());
        assertInstanceOf(CircleDTO.class, figureDTO.getShapes().get(0));
        CircleDTO circleDTO = (CircleDTO) figureDTO.getShapes().get(0);
        assertEquals("Acircle", circleDTO.getName());
        assertEquals(10, circleDTO.getRadius());

        assertInstanceOf(RectangleDTO.class, figureDTO.getShapes().get(1));
        RectangleDTO rectangleDTO = (RectangleDTO) figureDTO.getShapes().get(1);
        assertEquals("Arectangle", rectangleDTO.getName());
        assertEquals(10, rectangleDTO.getWidth());
        assertEquals(20, rectangleDTO.getHeight());

        assertInstanceOf(ShapeDTO.class, figureDTO.getShapes().get(2));
        ShapeDTO shapeDTO = figureDTO.getShapes().get(2);
        assertEquals("Ashape", shapeDTO.getName());


    }
}
