package net.ihe.gazelle.modelmarhsaller.interlay.mapper.mock.dto;

public class CircleDTO extends ShapeDTO{

    private int radius;

    public CircleDTO() {
        super.setType("circle");
    }

    public int getRadius() {
        return radius;
    }

    public CircleDTO setRadius(int radius) {
        this.radius = radius;
        return this;
    }

    @Override
    public CircleDTO setName(String name) {
        super.setName(name);
        return this;
    }
}
