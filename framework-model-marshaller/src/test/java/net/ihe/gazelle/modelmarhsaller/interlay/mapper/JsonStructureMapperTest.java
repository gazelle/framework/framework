package net.ihe.gazelle.modelmarhsaller.interlay.mapper;

import net.ihe.gazelle.modelmarhsaller.interlay.mapper.mock.structure.Person;
import net.ihe.gazelle.modelmarshaller.application.StructureMapper;
import net.ihe.gazelle.modelmarshaller.interlay.factory.JsonObjectMapperBuilder;
import net.ihe.gazelle.modelmarshaller.interlay.mapper.JsonStructureMapper;
import net.ihe.gazelle.modelmarshaller.interlay.mapper.MarshallingException;
import net.ihe.gazelle.modelmarshaller.interlay.mapper.UnmarshallingException;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class JsonStructureMapperTest {

    private final StructureMapper structureMapper = new JsonStructureMapper(new JsonObjectMapperBuilder().build());

    @Test
    public void testMarshall() {
        Person person = new Person().setName("John").setAge(20);
        System.out.println(person);
        assertEquals("{\"name\":\"John\",\"age\":20}", structureMapper.marshall(person));
    }

    @Test
    public void testUnmarshall() {
        String json = "{\"name\":\"John\",\"age\":20}";
        Person person = structureMapper.unmarshall(json, Person.class);
        assertEquals("John", person.getName());
        assertEquals(20, person.getAge());
    }

    @Test
    public void testUnmarshallInvalidJson(){
        String json = "{\"name\":\"John\",\"age\":20";
        Throwable throwable = assertThrows(UnmarshallingException.class, () -> structureMapper.unmarshall(json, Person.class));
        assertEquals("Error while unmarshalling JSON to "+ Person.class.getSimpleName(), throwable.getMessage());
    }

    @Test
    public void testMarshallInvalidObject(){
        InvalidObject invalidObject = new InvalidObject();
        Throwable throwable = assertThrows(MarshallingException.class, () -> structureMapper.marshall(invalidObject));
        assertEquals("Error while marshalling " +invalidObject.getClass().getSimpleName(), throwable.getMessage());
    }

    private static class InvalidObject  {

        private String field;

    }
}
