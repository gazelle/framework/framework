package net.ihe.gazelle.modelmarhsaller.interlay.mapper.mock.factory.json;

import com.fasterxml.jackson.databind.node.ObjectNode;
import net.ihe.gazelle.modelmarhsaller.interlay.mapper.mock.dto.RectangleDTO;
import net.ihe.gazelle.modelmarshaller.interlay.mapper.StructureJsonFactory;

public class RectangleDTOJsonFactory implements StructureJsonFactory<RectangleDTO> {

    @Override
    public RectangleDTO createStructure(ObjectNode objectNode) {
        return new RectangleDTO()
                .setWidth(objectNode.get("width").asInt())
                .setHeight(objectNode.get("height").asInt())
                .setName(objectNode.get("name").asText())
                ;
    }

    @Override
    public String getType() {
        return "rectangle";
    }


}
