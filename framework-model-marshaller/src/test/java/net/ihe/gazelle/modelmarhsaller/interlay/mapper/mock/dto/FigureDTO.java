package net.ihe.gazelle.modelmarhsaller.interlay.mapper.mock.dto;

import net.ihe.gazelle.modelmarhsaller.interlay.mapper.mock.structure.Figure;
import net.ihe.gazelle.modelmarhsaller.interlay.mapper.mock.structure.Shape;
import net.ihe.gazelle.modelmarshaller.interlay.dto.DTOFactory;
import net.ihe.gazelle.modelmarshaller.interlay.dto.DTOFactoryProviderSPI;

import java.util.List;

public class FigureDTO {

    private List<ShapeDTO> shapesDTOs;

    private final DTOFactoryProviderSPI<ShapeDTO> shapeDTOFactoryProviderSPI = new DTOFactoryProviderSPI<>();

    public FigureDTO(Figure figure) {
        this.shapesDTOs = figure.getShapes().stream()
                .map(shape -> ((DTOFactory<ShapeDTO, Shape>)shapeDTOFactoryProviderSPI.createFactory(shape.getClass())).createDTO(shape))
                .collect(java.util.stream.Collectors.toList());
    }

    public List<ShapeDTO> getShapes() {
        return shapesDTOs;
    }

    public FigureDTO setShapes(List<ShapeDTO> shapesDTOs) {
        this.shapesDTOs = shapesDTOs;
        return this;
    }

    public FigureDTO addShapeDTO(ShapeDTO shapeDTO) {
        this.shapesDTOs.add(shapeDTO);
        return this;
    }

}
