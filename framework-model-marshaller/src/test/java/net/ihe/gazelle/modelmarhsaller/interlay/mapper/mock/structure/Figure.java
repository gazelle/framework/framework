package net.ihe.gazelle.modelmarhsaller.interlay.mapper.mock.structure;

import java.util.ArrayList;
import java.util.List;

public class Figure {

    private final List<Shape> shapes = new ArrayList<>();

    public List<Shape> getShapes() {
        return shapes;
    }

    public Figure addShape(Shape shape) {
        this.shapes.add(shape);
        return this;
    }


}
