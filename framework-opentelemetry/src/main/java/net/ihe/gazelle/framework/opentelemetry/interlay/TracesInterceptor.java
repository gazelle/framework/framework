package net.ihe.gazelle.framework.opentelemetry.interlay;


import net.ihe.gazelle.servicemetadata.api.application.MetadataService;

import jakarta.inject.Inject;
import jakarta.interceptor.AroundInvoke;
import jakarta.interceptor.Interceptor;
import jakarta.interceptor.InvocationContext;
import java.util.logging.Logger;

@Interceptor
@TracedMethod
public class TracesInterceptor {

    @Inject
    MetadataService metadataService;

    @AroundInvoke
    public Object manageTransaction(InvocationContext ctx) throws Exception {
        Logger.getLogger("").fine("Send traces - '" + ctx.getMethod().getAnnotation(TracedMethod.class).name() + "'");
        // Create tracer from service metadata
        TracesManager tracesManager = new TracesManager(metadataService);

        // Get span name from annotation
        String spanName = ctx.getMethod().getAnnotation(TracedMethod.class).name();

        // Create span and make it current
        tracesManager.createSpanMakeItCurrent(spanName);

        // Proceed to the method
        Object result = ctx.proceed();

        // Close the current span
        tracesManager.endCurrentSpan();
        return result;
    }
}
