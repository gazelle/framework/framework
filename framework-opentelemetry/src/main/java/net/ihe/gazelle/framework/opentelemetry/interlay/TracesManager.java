package net.ihe.gazelle.framework.opentelemetry.interlay;

import io.opentelemetry.api.GlobalOpenTelemetry;
import io.opentelemetry.api.trace.Span;
import io.opentelemetry.api.trace.Tracer;
import net.ihe.gazelle.servicemetadata.api.application.MetadataService;
import net.ihe.gazelle.servicemetadata.api.domain.structure.Service;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.logging.Handler;
import java.util.logging.Logger;

/**
 * Manager for traces using OpenTelemetry API
 *
 * @author : Valentin Lorand
 * @since : 2022-09-09
 */
public class TracesManager {

    private final Tracer tracer;

    private final Map<String, Span> spanMap;

    private MetadataService metadataService;

    private Service service;

    private Span currentSpan;

    public TracesManager(MetadataService metadataService) {
        this(metadataService.getMetadata().getName() + "-tracer");
        this.metadataService = metadataService;
        this.service = metadataService.getMetadata();
    }

    public TracesManager(String tracerName) {
        tracer = GlobalOpenTelemetry.getTracer(tracerName);

        // Add TracesHandler to root logger if doesn't exist
        Optional<Handler> optional = Arrays.stream(Logger.getLogger("").getHandlers()).filter(TracesHandler.class::isInstance).findFirst();
        if (optional.isEmpty()) {
            Logger.getLogger("").addHandler(new TracesHandler());
        }

        spanMap = new HashMap<>();
    }


    /**
     * Create a new span with the given name and save it
     *
     * @param spanName the name of the span
     * @return the new span
     */
    public Span createSpanMakeItCurrent(String spanName) {
        final Span span = tracer.spanBuilder(spanName).startSpan();
        spanMap.put(spanName, span);
        span.makeCurrent();
        currentSpan = span;
        span.setAttribute("span.name", spanName);
        if (this.metadataService != null) {
            span.setAttribute("app.instanceId", service.getInstanceId());
            span.setAttribute("app.replicaId", service.getReplicaId());
            span.setAttribute("app.version", service.getVersion());
            span.setAttribute("app.name", service.getName());
        }
        return span;
    }


    public Span getCurrentSpan() {
        return currentSpan;
    }

    public void endCurrentSpan() {
        currentSpan.end();
    }
}
