package net.ihe.gazelle.framework.opentelemetry.interlay;

import io.opentelemetry.api.trace.Span;

import java.util.Objects;
import java.util.logging.Handler;
import java.util.logging.LogRecord;

/**
 * Log handler for Open Telemetry traces
 *
 * @author : Valentin Lorand
 * @since : 2021-09-09
 */
public class TracesHandler extends Handler {

    @Override
    public void publish(LogRecord logRecord) {
        String traceLog = "[" + logRecord.getLevel().getName() + "] " + logRecord.getMessage();
        int counter = 0;

        // Replace dynamic parameters
        if (logRecord.getParameters() != null) {
            for (Object parameter : logRecord.getParameters()) {
                traceLog = traceLog.replace("{" + counter + "}", Objects.toString(parameter));
                counter++;
            }
        }

        Span span = Span.current();
        if (span.isRecording()) {
            span.addEvent(traceLog);
        }
    }

    @Override
    public void flush() { /* Unused method */ }

    @Override
    public void close() throws SecurityException {  /* Unused method */ }


}