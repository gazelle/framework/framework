package net.ihe.gazelle.framework.opentelemetry.interlay;

import io.opentelemetry.api.trace.Span;
import io.opentelemetry.api.trace.StatusCode;
import net.ihe.gazelle.servicemetadata.api.application.MetadataService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

class TracesManagerTest {

    MetadataService metadataService;

    @BeforeEach
    void setUp() {
        metadataService = new MetadataServiceProviderMock();
    }

    @Test
    void createTracesManager() {
        Logger logger = Logger.getLogger("");
        TracesManager tracesManager = new TracesManager(metadataService);
        assertNotEquals(0, logger.getHandlers().length);
        Stream<Handler> result = Arrays.stream(logger.getHandlers()).filter(handler -> handler instanceof TracesHandler);
        assertEquals(1,result.count());
    }

    @Test
    void createTwoTracesManager() {
        Logger logger = Logger.getLogger("");
        new TracesManager(metadataService);
        new TracesManager(metadataService);

        assertNotEquals(0, logger.getHandlers().length);
        Stream<Handler> result = Arrays.stream(logger.getHandlers()).filter(handler -> handler instanceof TracesHandler);
        assertEquals(1,result.count());
    }

    @Test
    void createSpan() {
        Logger logger = Logger.getLogger("");
        TracesManager tracesManager = new TracesManager(metadataService);
        tracesManager.createSpanMakeItCurrent("span1");
        logger.log(Level.INFO,"This log is supposed to be in the span");
        logger.log(Level.INFO,"This log is supposed to be in the span with this {0}", new String[]{"parameter"});
        tracesManager.getCurrentSpan().setStatus(StatusCode.OK);
        tracesManager.endCurrentSpan();
        assertFalse(Span.current().isRecording());
        //TODO no way found to test if the log is in the span
    }
}
