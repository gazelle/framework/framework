package net.ihe.gazelle.framework.opentelemetry.interlay;

import net.ihe.gazelle.framework.modelvalidator.interlay.adapter.BValidatorBuilderFactory;
import net.ihe.gazelle.servicemetadata.api.application.MetadataService;
import net.ihe.gazelle.servicemetadata.api.application.builder.ServiceBuilder;
import net.ihe.gazelle.servicemetadata.api.domain.structure.Service;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MetadataServiceProviderMock implements MetadataService {

    private static final String K8S_ID_REGEXP = "^([a-z0-9\\-]+)\\-([a-z0-9]{5,})\\-([a-z0-9]{3,})$";
    private static final Pattern pattern = Pattern.compile("^([a-z0-9\\-]+)\\-([a-z0-9]{5,})\\-([a-z0-9]{3,})$");
    private static final int GROUP_DEPLOY_NAME = 1;
    private static final int GROUP_INSTANCE_ID = 2;
    private static final int GROUP_REPLICA_ID = 3;
    private String instanceId;
    private String replicaId;

    private Service service;

    public MetadataServiceProviderMock() {
        this.service = new ServiceBuilder(new BValidatorBuilderFactory())
                .setName(this.getName())
                .setVersion(this.getVersion())
                .setInstanceId(this.getInstanceId())
                .setReplicaId(this.getReplicaId())
                .build();
    }

    @Override
    public Service getMetadata() {
        return service;
    }

    private String getName() {
        return System.getProperty("it.service.name", "service-name");
    }

    private String getVersion() {
        return System.getProperty("it.service.version", "1.0.0-SNAPSHOT");
    }

    private String getInstanceId() {
        if (this.instanceId == null) {
            this.parseK8sId();
        }

        return this.instanceId;
    }

    private String getReplicaId() {
        if (this.replicaId == null) {
            this.parseK8sId();
        }

        return this.replicaId;
    }

    private void parseK8sId() {
        String k8sid = System.getProperty("it.service.k8s.id", "service-test-azerty1234-abc12");
        Matcher matcher = pattern.matcher(k8sid);
        if (matcher.find()) {
            this.instanceId = matcher.group(2);
            this.replicaId = matcher.group(3);
        } else {
            throw new IllegalStateException("Unable to parse instanceId and replicaId from k8s id. Check for " + k8sid + " property variable.");
        }
    }
}
