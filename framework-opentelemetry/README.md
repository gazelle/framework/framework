# Framework OpenTelemetry

Custom library using OpenTelemetry to manage traces in a microprofile context.

## Installation

Add the dependency

```xml

<dependency>
    <groupId>net.ihe.gazelle</groupId>
    <artifactId>framework-opentelemetry</artifactId>
    <version>${framework.version}</version>
</dependency>
```

Adjust your `beans.xml` to activate interceptors.

```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://xmlns.jcp.org/xml/ns/javaee"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xsi:schemaLocation="http://xmlns.jcp.org/xml/ns/javaee http://xmlns.jcp.org/xml/ns/javaee/beans_1_1.xsd"
       bean-discovery-mode="all">
    <interceptors>
        <class>net.ihe.gazelle.framework.opentelemetry.interlay.TracesInterceptor</class>
    </interceptors>
</beans>
```

## Usage

Use @TracedMethod annotation on your method to trace it.

```java
@TracedMethod(name = "aboutHttpMessaging")
public String aboutHttpMessaging() {
    // Doing stuff ...
    Span.current().setStatus(StatusCode.OK); // Retrieve current span and set status to OK
}
```

Use TracesManager instance to manage span (Require serviceMetadata instance) .

```java
@Inject
private ServiceMetadata serviceMetadata;

TracesManager tracesManager;

@PostConstruct
public void init(){
    this.tracesManager = new TracesManager(serviceMetadata);
}

@Override
public void initiateCommand(...) {
    tracesManager.createSpanMakeItCurrent("initiate command");
    // Doing stuff ...
    tracesManager.getCurrentSpan().setStatus(StatusCode.OK); // Retrieve current span and set status to OK
    tracesManager.endCurrentSpan();
}
```

## Configuration

To work properly, some variables need to be set in the environment.

| Variable                        | Description                                        | Required | Example             |
|---------------------------------|----------------------------------------------------|----------|---------------------|
| GZL_OPENTELEMETRY_ENABLED       | Import open-telemetry wildfly implementation       | yes      | true                |
| GZL_OPENTELEMETRY_EXPORTER_TYPE | The type of trace exporter to use.                 | yes      | jaeger              |
| GZL_OPENTELEMETRY_EXPORTER_URL  | Url for the exporter where the traces must be sent | yes      | http://jaeger:14250 |

## Testing

To test the library, you can use the [Jaeger](https://www.jaegertracing.io/) traces collector.

Example of docker-compose to deploy Jaeger (the service should be available at [http://localhost:16686](http://localhost:16686)).

⚠️Jaeger should be in the same network as the service to trace.

```yaml
version: '3.5'
services:
  jaeger:
    container_name: jaeger
    restart: always
    image: jaegertracing/all-in-one:1.16
    ports:
      - "16686:16686"
    networks:
      localit:

networks:
  localit:
    name: localit
```