# Framework

Series of usefull modules to integrate any application in Gazelle Test Bed.

_Those modules are build on Java 17, and use Microprofile 5.0_

## Model Marshaller

Service Configuration API to marshall/unmarshall structures and interfaces to integrate 
with the serialization pattern: [Model Marshalled documentation](framework-model-marshaller/README.md).

## Model Validator

A library that provides you a fluent API to build & validate your domain
objects with adapters to BValid engine: [Model Validator documentation](framework-model-validator/README.md).

## OpenTelemetry

Custom library using OpenTelemetry to manage traces in a microprofile context :
[OpenTelemetry documentation](framework-opentelemetry/README.md).

## Service Metadata

The Main Metadata Service API to integrate any application in Gazelle Test Bed: 
[Service Metadata documentation](service-metadata/README.md).

## How to build

```shell
mvn clean install
```

Packaged libraries will be available in `framework-xxxxx/target/framework-xxxxx.jar`
or in local .m2 repository.
