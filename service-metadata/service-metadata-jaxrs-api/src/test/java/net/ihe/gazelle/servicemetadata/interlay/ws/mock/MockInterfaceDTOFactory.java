package net.ihe.gazelle.servicemetadata.interlay.ws.mock;

import net.ihe.gazelle.modelmarshaller.interlay.dto.DTOFactory;
import net.ihe.gazelle.modelmarshaller.interlay.dto.DTOFactoryProvider;
import net.ihe.gazelle.modelmarshaller.interlay.dto.DTOFactoryProviderSPI;
import net.ihe.gazelle.servicemetadata.api.domain.structure.Binding;
import net.ihe.gazelle.servicemetadata.jaxrs.api.interlay.dto.BindingDTO;

public class MockInterfaceDTOFactory implements DTOFactory<MockInterfaceDTO, MockInterface> {

    private final DTOFactoryProvider<BindingDTO> factoryProvider = new DTOFactoryProviderSPI<>();


    @Override
    public MockInterfaceDTO createDTO(MockInterface object) {
        MockInterfaceDTO mockInterfaceDTO = new MockInterfaceDTO();
        mockInterfaceDTO.setInterfaceName(object.getInterfaceName());
        mockInterfaceDTO.setInterfaceVersion(object.getInterfaceVersion());
        mockInterfaceDTO.setRequired(object.isRequired());
        mockInterfaceDTO.setBindings(
                object.getBindings() != null ?
                        object.getBindings()
                                .stream()
                                .map(binding -> ((DTOFactory<BindingDTO, Binding>)factoryProvider.createFactory(binding.getClass())).createDTO(binding))
                                .toList() : null
        );
        return mockInterfaceDTO;
    }

    @Override
    public Class<MockInterface> getType() {
        return MockInterface.class;
    }
}
