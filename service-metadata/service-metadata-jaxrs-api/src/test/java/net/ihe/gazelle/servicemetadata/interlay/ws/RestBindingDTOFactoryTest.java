package net.ihe.gazelle.servicemetadata.interlay.ws;

import net.ihe.gazelle.modelmarshaller.interlay.dto.DTOFactoryProvider;
import net.ihe.gazelle.modelmarshaller.interlay.dto.DTOFactoryProviderSPI;
import net.ihe.gazelle.servicemetadata.api.domain.structure.RestBinding;
import net.ihe.gazelle.servicemetadata.jaxrs.api.interlay.dto.BindingDTO;
import net.ihe.gazelle.servicemetadata.jaxrs.api.interlay.dto.RestBindingDTO;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class RestBindingDTOFactoryTest {

    private final DTOFactoryProvider<BindingDTO> factoryProvider = new DTOFactoryProviderSPI<>();


    @Test
    public void testCreateDTO() {

        RestBindingDTO restBindingDTO = (RestBindingDTO) factoryProvider
                .createFactory(RestBinding.class)
                .createDTO(new RestBinding()
                    .setServiceUrl("http://localhost:8080")
                );
        assertEquals("http://localhost:8080", restBindingDTO.getServiceUrl());


    }
}
