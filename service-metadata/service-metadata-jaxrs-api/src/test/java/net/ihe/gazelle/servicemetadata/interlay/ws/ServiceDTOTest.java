package net.ihe.gazelle.servicemetadata.interlay.ws;

import net.ihe.gazelle.modelmarshaller.application.StructureMapper;
import net.ihe.gazelle.modelmarshaller.interlay.factory.JsonObjectMapperBuilder;
import net.ihe.gazelle.modelmarshaller.interlay.mapper.JsonStructureMapper;
import net.ihe.gazelle.modelmarshaller.interlay.mapper.UnmarshallingException;
import net.ihe.gazelle.servicemetadata.api.domain.structure.RestBinding;
import net.ihe.gazelle.servicemetadata.api.domain.structure.Service;
import net.ihe.gazelle.servicemetadata.interlay.ws.mock.MockInterface;
import net.ihe.gazelle.servicemetadata.interlay.ws.mock.MockInterfaceDTO;
import net.ihe.gazelle.servicemetadata.jaxrs.api.interlay.dto.RestBindingDTO;
import net.ihe.gazelle.servicemetadata.jaxrs.api.interlay.dto.ServiceDTO;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class ServiceDTOTest {

    StructureMapper structureFactory = new JsonStructureMapper(new JsonObjectMapperBuilder().build());

    @Test
    public void testMarshallingWithAllFields(){
        MockInterfaceDTO providedInterface = new MockInterfaceDTO()
                .setInterfaceName("interfaceName")
                .setInterfaceVersion("interfaceVersion")
                .setRequired(true)
                .setBindings(List.of(new RestBindingDTO()
                        .setServiceUrl("http://localhost:8080")))
                ;
        MockInterfaceDTO consumedInterface = new MockInterfaceDTO()
                .setInterfaceName("ConsumedInterfaceName")
                .setInterfaceVersion("ConsumedInterfaceVersion")
                .setRequired(true)
                .setBindings(List.of(new RestBindingDTO()
                        .setServiceUrl("http://localhost:8080")))
                ;
        ServiceDTO serviceDTO = new ServiceDTO()
                .setName("name")
                .setVersion("version")
                .setInstanceId("instanceId")
                .setReplicaId("replicaId")
        .setProvidedInterfaces(List.of(providedInterface))
        .setConsumedInterfaces(List.of(consumedInterface));
        String json = structureFactory.marshall(serviceDTO);
        assertEquals("{\"name\":\"name\",\"version\":\"version\",\"instanceId\":\"instanceId\",\"replicaId\":\"replicaId\",\"providedInterfaces\":[{\"type\":\"mockInterface\",\"interfaceName\":\"interfaceName\",\"interfaceVersion\":\"interfaceVersion\",\"required\":true,\"bindings\":[{\"type\":\"restBinding\",\"serviceUrl\":\"http://localhost:8080\"}]}],\"consumedInterfaces\":[{\"type\":\"mockInterface\",\"interfaceName\":\"ConsumedInterfaceName\",\"interfaceVersion\":\"ConsumedInterfaceVersion\",\"required\":true,\"bindings\":[{\"type\":\"restBinding\",\"serviceUrl\":\"http://localhost:8080\"}]}]}", json);
    }

    @Test
    public void testUnmarshallingWithNoBindings(){
        String json = "{\"name\":\"name\",\"version\":\"version\",\"instanceId\":\"instanceId\",\"replicaId\":\"replicaId\",\"providedInterfaces\":[{\"type\":\"mockInterface\",\"interfaceName\":\"interfaceName\",\"interfaceVersion\":\"interfaceVersion\",\"required\":true,\"bindings\":[{\"type\":\"restBinding\",\"serviceUrl\":\"http://localhost:8080\"}]}],\"consumedInterfaces\":[{\"type\":\"mockInterface\",\"interfaceName\":\"ConsumedInterfaceName\",\"interfaceVersion\":\"ConsumedInterfaceVersion\",\"required\":true,\"bindings\":[{\"type\":\"restBinding\",\"serviceUrl\":\"http://localhost:8080\"}]}]}";
        ServiceDTO serviceDTO = structureFactory.unmarshall(json, ServiceDTO.class);
        assertEquals("name", serviceDTO.getName());
        assertEquals("version", serviceDTO.getVersion());
        assertEquals("instanceId", serviceDTO.getInstanceId());
        assertEquals("replicaId", serviceDTO.getReplicaId());
        assertInstanceOf(MockInterfaceDTO.class, serviceDTO.getProvidedInterfaces().get(0));
        assertInstanceOf(MockInterfaceDTO.class, serviceDTO.getConsumedInterfaces().get(0));
        assertEquals("interfaceName", serviceDTO.getProvidedInterfaces().get(0).getInterfaceName());
        assertEquals("interfaceVersion", serviceDTO.getProvidedInterfaces().get(0).getInterfaceVersion());
        assertTrue(serviceDTO.getProvidedInterfaces().get(0).isRequired());
        assertInstanceOf(RestBindingDTO.class, serviceDTO.getProvidedInterfaces().get(0).getBindings().get(0));
        assertEquals(1, serviceDTO.getProvidedInterfaces().get(0).getBindings().size());
        assertEquals("http://localhost:8080", ((RestBindingDTO) serviceDTO.getProvidedInterfaces().get(0).getBindings().get(0)).getServiceUrl());
        assertEquals("ConsumedInterfaceName", serviceDTO.getConsumedInterfaces().get(0).getInterfaceName());
        assertEquals("ConsumedInterfaceVersion", serviceDTO.getConsumedInterfaces().get(0).getInterfaceVersion());
        assertTrue(serviceDTO.getConsumedInterfaces().get(0).isRequired());
        assertInstanceOf(RestBindingDTO.class, serviceDTO.getConsumedInterfaces().get(0).getBindings().get(0));
        assertEquals(1, serviceDTO.getConsumedInterfaces().get(0).getBindings().size());
        assertEquals("http://localhost:8080", ((RestBindingDTO) serviceDTO.getConsumedInterfaces().get(0).getBindings().get(0)).getServiceUrl());
    }

    @Test
    public void testUnmarshallingWithWrongJson(){
        String wrongJson = "{\"name\": ,\"version\":\"version\",\"instanceId\":\"instanceId\",\"replicaId\":\"replicaId\",\"providedInterfaces\":[{\"type\":\"mockInterface\",\"interfaceName\":\"interfaceName\",\"interfaceVersion\":\"interfaceVersion\",\"required\":true,\"bindings\":[{\"type\":\"restBinding\",\"serviceUrl\":\"http://localhost:8080\"}],\"validationProfiles\":[{\"profileID\":\"profileID\",\"profileName\":\"profileName\",\"domain\":\"domain\",\"coveredItems\":[\"item1\"]}]}],\"consumedInterfaces\":[{\"type\":\"mockInterface\",\"interfaceName\":\"ConsumedInterfaceName\",\"interfaceVersion\":\"ConsumedInterfaceVersion\",\"required\":true,\"bindings\":[{\"type\":\"restBinding\",\"serviceUrl\":\"http://localhost:8080\"}],\"validationProfiles\":[{\"profileID\":\"profileID\",\"profileName\":\"profileName\",\"domain\":\"domain\",\"coveredItems\":[\"item1\"]}]}]}";
        Throwable exception = assertThrows(UnmarshallingException.class, () -> {
            structureFactory.unmarshall(wrongJson, ServiceDTO.class);
        });
        assertEquals("Error while unmarshalling JSON to ServiceDTO", exception.getMessage());
    }

    @Test
    public void testGetServiceObjectFromJson(){
        String json = "{\"name\":\"name\",\"version\":\"version\",\"instanceId\":\"instanceId\",\"replicaId\":\"replicaId\",\"providedInterfaces\":[{\"type\":\"mockInterface\",\"interfaceName\":\"interfaceName\",\"interfaceVersion\":\"interfaceVersion\",\"required\":true,\"bindings\":[{\"type\":\"restBinding\",\"serviceUrl\":\"http://localhost:8080\"}],\"validationProfiles\":[{\"profileID\":\"profileID\",\"profileName\":\"profileName\",\"domain\":\"domain\",\"coveredItems\":[\"item1\"]}]}],\"consumedInterfaces\":[{\"type\":\"mockInterface\",\"interfaceName\":\"ConsumedInterfaceName\",\"interfaceVersion\":\"ConsumedInterfaceVersion\",\"required\":true,\"bindings\":[{\"type\":\"restBinding\",\"serviceUrl\":\"http://localhost:8080\"}],\"validationProfiles\":[{\"profileID\":\"profileID\",\"profileName\":\"profileName\",\"domain\":\"domain\",\"coveredItems\":[\"item1\"]}]}]}";
        ServiceDTO serviceDTO = structureFactory.unmarshall(json, ServiceDTO.class);
        Service service = serviceDTO.getDomainObject();
        assertInstanceOf(Service.class, service);
        assertInstanceOf(MockInterface.class, service.getProvidedInterfaces().get(0));
        assertInstanceOf(MockInterface.class, service.getConsumedInterfaces().get(0));
        assertInstanceOf(RestBinding.class, service.getProvidedInterfaces().get(0).getBindings().get(0));
        assertInstanceOf(RestBinding.class, service.getConsumedInterfaces().get(0).getBindings().get(0));
    }
}
