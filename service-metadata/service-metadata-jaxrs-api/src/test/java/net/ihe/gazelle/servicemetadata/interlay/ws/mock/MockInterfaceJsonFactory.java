package net.ihe.gazelle.servicemetadata.interlay.ws.mock;

import com.fasterxml.jackson.databind.node.ObjectNode;
import net.ihe.gazelle.servicemetadata.jaxrs.api.interlay.dto.InterfaceDTO;
import net.ihe.gazelle.servicemetadata.jaxrs.api.interlay.factory.json.AbstractInterfaceJsonFactory;

public class MockInterfaceJsonFactory extends AbstractInterfaceJsonFactory {


    @Override
    public InterfaceDTO createStructure(ObjectNode objectNode) {
        return new MockInterfaceDTO()
                .setInterfaceName(objectNode.get("interfaceName").asText())
                .setInterfaceVersion(objectNode.get("interfaceVersion").asText())
                .setRequired(objectNode.get("required").asBoolean())
                .setBindings(getBindings(objectNode))
                ;

    }

    @Override
    public String getType() {
        return "mockInterface";
    }
}
