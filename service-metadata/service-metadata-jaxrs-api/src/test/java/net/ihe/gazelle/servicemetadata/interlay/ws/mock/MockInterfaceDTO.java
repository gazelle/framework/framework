package net.ihe.gazelle.servicemetadata.interlay.ws.mock;

import net.ihe.gazelle.servicemetadata.api.domain.structure.Interface;
import net.ihe.gazelle.servicemetadata.jaxrs.api.interlay.dto.BindingDTO;
import net.ihe.gazelle.servicemetadata.jaxrs.api.interlay.dto.InterfaceDTO;

import java.util.List;

public class MockInterfaceDTO extends InterfaceDTO {

    public MockInterfaceDTO() {
        super.anInterface = new MockInterface();
        super.setType("mockInterface");
    }



    @Override
    public MockInterfaceDTO setInterfaceName(String interfaceName) {
        super.setInterfaceName(interfaceName);
        return this;
    }

    @Override
    public MockInterfaceDTO setInterfaceVersion(String interfaceVersion) {
        super.setInterfaceVersion(interfaceVersion);
        return this;
    }

    @Override
    public MockInterfaceDTO setRequired(boolean required) {
        super.setRequired(required);
        return this;
    }

    @Override
    public MockInterfaceDTO setBindings(List<BindingDTO> bindingDTOs) {
        super.setBindings(bindingDTOs);
        return this;
    }


}
