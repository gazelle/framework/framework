package net.ihe.gazelle.servicemetadata.interlay.ws;

import net.ihe.gazelle.modelmarshaller.application.StructureMapper;
import net.ihe.gazelle.modelmarshaller.interlay.factory.JsonObjectMapperBuilder;
import net.ihe.gazelle.modelmarshaller.interlay.mapper.JsonStructureMapper;
import net.ihe.gazelle.servicemetadata.jaxrs.api.interlay.dto.BindingDTO;
import net.ihe.gazelle.servicemetadata.jaxrs.api.interlay.dto.RestBindingDTO;
import net.ihe.gazelle.modelmarshaller.interlay.mapper.UnmarshallingException;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class RestBindingDTOTest {

   private static StructureMapper structureFactory = new JsonStructureMapper(new JsonObjectMapperBuilder().build());

    @Test
    public void testMarshallingRestBinding() {
        BindingDTO restBinding = new RestBindingDTO()
                .setServiceUrl("http://localhost:8080");
        String json = structureFactory.marshall(restBinding);
        assertEquals("{\"type\":\"restBinding\",\"serviceUrl\":\"http://localhost:8080\"}", json);
    }

    @Test
    public void testUnmarshallingRestBinding() throws UnmarshallingException {
        String json = "{\"type\":\"restBinding\",\"serviceUrl\":\"http://localhost:8080\"}";
        BindingDTO bindingDTO = structureFactory.unmarshall(json, BindingDTO.class);
        assertInstanceOf(RestBindingDTO.class, bindingDTO);
        assertEquals("RestBindingDTO", bindingDTO.getClass().getSimpleName());
        assertEquals("http://localhost:8080", ((RestBindingDTO) bindingDTO).getServiceUrl());
    }

    @Test
    public void testUnmarshallingWithWrongType() {
        Throwable throwable = assertThrows(IllegalArgumentException.class, () -> {
            String json = "{\"type\":\"wrongType\",\"serviceUrl\":\"http://localhost:8080\"}";
            structureFactory.unmarshall(json, BindingDTO.class);
        });
        assertEquals("No Json Factory implementation found for type wrongType", throwable.getMessage());
    }

    @Test
    public void testUnmarshallingWithMissingType() {
        Throwable throwable = assertThrows(UnmarshallingException.class, () -> {
            String json = "{\"serviceUrl\":\"http://localhost:8080\"}";
            structureFactory.unmarshall(json, BindingDTO.class);
        });
        assertEquals("No type attribute found in json", throwable.getMessage());
    }

    @Test
    public void testGetBindingObjectFromJson(){
        String json = "{\"type\":\"restBinding\",\"serviceUrl\":\"http://localhost:8080\"}";
        BindingDTO bindingDTO = structureFactory.unmarshall(json, BindingDTO.class);
        assertInstanceOf(RestBindingDTO.class, bindingDTO);
        assertEquals("RestBindingDTO", bindingDTO.getClass().getSimpleName());
        assertEquals("http://localhost:8080", ((RestBindingDTO) bindingDTO).getServiceUrl());
    }

}
