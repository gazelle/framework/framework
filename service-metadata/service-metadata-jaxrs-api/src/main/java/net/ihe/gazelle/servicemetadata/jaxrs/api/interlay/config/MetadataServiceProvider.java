package net.ihe.gazelle.servicemetadata.jaxrs.api.interlay.config;

import jakarta.enterprise.context.RequestScoped;
import net.ihe.gazelle.framework.modelvalidator.domain.ValidatorBuilderFactory;
import net.ihe.gazelle.framework.modelvalidator.interlay.adapter.BValidatorBuilderFactory;
import net.ihe.gazelle.servicemetadata.api.application.MetadataService;
import net.ihe.gazelle.servicemetadata.api.application.builder.ServiceBuilder;
import net.ihe.gazelle.servicemetadata.api.domain.structure.Service;
import org.eclipse.microprofile.config.inject.ConfigProperty;

@RequestScoped
public class MetadataServiceProvider implements MetadataService {

    public static final String GZL_SERVICE_NAME = "gzl.service.name";
    public static final String GZL_SERVICE_VERSION = "gzl.service.version";

    @ConfigProperty(name = GZL_SERVICE_NAME)
    String name;

    @ConfigProperty(name = GZL_SERVICE_VERSION)
    String version;

    private final ValidatorBuilderFactory validatorBuilderFactory;

    public MetadataServiceProvider() {
        this.validatorBuilderFactory = new BValidatorBuilderFactory();
    }

    @Override
    public Service getMetadata() {
        return new ServiceBuilder(validatorBuilderFactory)
                .setName(name)
                .setVersion(version)
                .build();
    }
}
