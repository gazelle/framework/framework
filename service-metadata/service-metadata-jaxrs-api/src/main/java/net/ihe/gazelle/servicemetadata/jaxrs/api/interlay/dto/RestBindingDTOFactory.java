package net.ihe.gazelle.servicemetadata.jaxrs.api.interlay.dto;

import net.ihe.gazelle.modelmarshaller.interlay.dto.DTOFactory;
import net.ihe.gazelle.servicemetadata.api.domain.structure.RestBinding;

public class RestBindingDTOFactory implements DTOFactory<RestBindingDTO, RestBinding> {

    @Override
    public RestBindingDTO createDTO(RestBinding object) {
        return new RestBindingDTO()
                .setServiceUrl(object.getServiceUrl());
    }

    @Override
    public Class<RestBinding> getType() {
        return RestBinding.class;
    }
}
