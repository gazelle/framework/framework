package net.ihe.gazelle.servicemetadata.jaxrs.api.interlay.factory.json;

import com.fasterxml.jackson.databind.node.ObjectNode;
import net.ihe.gazelle.modelmarshaller.interlay.mapper.StructureJsonFactory;
import net.ihe.gazelle.servicemetadata.jaxrs.api.interlay.dto.BindingDTO;
import net.ihe.gazelle.servicemetadata.jaxrs.api.interlay.dto.RestBindingDTO;

public class RestBindingJsonFactory implements StructureJsonFactory<BindingDTO> {


    @Override
    public BindingDTO createStructure(ObjectNode objectNode) {
        return new RestBindingDTO()
                .setServiceUrl(objectNode.get("serviceUrl").asText());
    }

    @Override
    public String getType() {
        return "restBinding";
    }
}
