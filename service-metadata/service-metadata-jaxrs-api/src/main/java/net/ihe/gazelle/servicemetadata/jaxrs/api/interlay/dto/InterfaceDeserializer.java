package net.ihe.gazelle.servicemetadata.jaxrs.api.interlay.dto;

import net.ihe.gazelle.modelmarshaller.interlay.mapper.StructureJsonFactoryProvider;
import net.ihe.gazelle.modelmarshaller.interlay.mapper.StructureJsonFactoryProviderSPI;
import net.ihe.gazelle.modelmarshaller.interlay.mapper.TypeDeserializer;

import java.io.Serial;

public class InterfaceDeserializer extends TypeDeserializer<InterfaceDTO> {

    @Serial
    private static final long serialVersionUID = -3324010596782142248L;

    // FIXME: 05/09/2023 This is hardcoded for now as jackson doesn't allow constructors params
    // in annotations, but should be injected somehow
    private final transient StructureJsonFactoryProvider<InterfaceDTO> interfaceJsonFactoryProvider = new StructureJsonFactoryProviderSPI<>();

    public InterfaceDeserializer() {
        super(InterfaceDTO.class);
    }

    @Override
    protected StructureJsonFactoryProvider<? extends InterfaceDTO> getStructureFactoryProvider() {
        return interfaceJsonFactoryProvider;
    }
}

