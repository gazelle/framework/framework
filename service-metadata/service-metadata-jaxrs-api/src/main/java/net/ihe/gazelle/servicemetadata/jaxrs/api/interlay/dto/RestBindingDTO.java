package net.ihe.gazelle.servicemetadata.jaxrs.api.interlay.dto;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import net.ihe.gazelle.servicemetadata.api.domain.structure.Binding;
import net.ihe.gazelle.servicemetadata.api.domain.structure.RestBinding;
import org.eclipse.microprofile.openapi.annotations.media.Schema;


@Schema(name = "RestBinding")
@JsonPropertyOrder({"type","serviceUrl"})
public class RestBindingDTO extends BindingDTO {

    public RestBindingDTO() {
        super.setType("restBinding");
        super.binding = new RestBinding();
    }


    @JsonProperty("serviceUrl")
    public String getServiceUrl() {
        return getDomainObject().getServiceUrl();
    }

    public RestBindingDTO setServiceUrl(String serviceUrl) {
        getDomainObject().setServiceUrl(serviceUrl);
        return this;
    }

    @JsonIgnore
    @Override
    public RestBinding getDomainObject() {
        return (RestBinding) super.getDomainObject();
    }


}
