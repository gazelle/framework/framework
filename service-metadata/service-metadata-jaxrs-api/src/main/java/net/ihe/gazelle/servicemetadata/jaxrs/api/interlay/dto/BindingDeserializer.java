package net.ihe.gazelle.servicemetadata.jaxrs.api.interlay.dto;

import net.ihe.gazelle.modelmarshaller.interlay.mapper.StructureJsonFactoryProvider;
import net.ihe.gazelle.modelmarshaller.interlay.mapper.StructureJsonFactoryProviderSPI;
import net.ihe.gazelle.modelmarshaller.interlay.mapper.TypeDeserializer;

import java.io.Serial;

public class BindingDeserializer extends TypeDeserializer<BindingDTO> {

    @Serial
    private static final long serialVersionUID = -6937096898000029803L;
    private final transient StructureJsonFactoryProvider<BindingDTO> bindingJsonFactoryProvider = new StructureJsonFactoryProviderSPI<>();

    protected BindingDeserializer() {
        super(BindingDTO.class);
    }

    @Override
    protected StructureJsonFactoryProvider<? extends BindingDTO> getStructureFactoryProvider() {
        return bindingJsonFactoryProvider;
    }
}
