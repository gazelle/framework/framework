package net.ihe.gazelle.servicemetadata.jaxrs.api.interlay.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import net.ihe.gazelle.servicemetadata.api.domain.structure.Interface;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

import java.util.List;



@Schema(name = "Interface")
@JsonPropertyOrder({"type", "interfaceName", "interfaceVersion", "required", "bindings"})
@JsonDeserialize(using = InterfaceDeserializer.class)
public abstract class InterfaceDTO implements DTO<Interface> {

    private String type;

    protected Interface anInterface;

    private List<BindingDTO> bindingDTOS;



    @JsonProperty("interfaceName")
    public String getInterfaceName(){
        return this.anInterface.getInterfaceName();
    }

    public InterfaceDTO setInterfaceName(String interfaceName){
        this.anInterface.setInterfaceName(interfaceName);
        return this;
    }

    @JsonProperty("interfaceVersion")
    public String getInterfaceVersion(){
        return this.anInterface.getInterfaceVersion();

    }

    public InterfaceDTO setInterfaceVersion(String interfaceVersion){
        this.anInterface.setInterfaceVersion(interfaceVersion);
        return this;
    }

    @JsonProperty("required")
    public boolean isRequired(){
        return this.anInterface.isRequired();
    }

    public InterfaceDTO setRequired(boolean required){
        this.anInterface.setRequired(required);
        return this;
    }

    @JsonProperty("bindings")
    public List<BindingDTO> getBindings() {
        return this.bindingDTOS;
    }

    public InterfaceDTO setBindings(List<BindingDTO> bindingDTOs) {
        this.bindingDTOS = bindingDTOs;
        return this;
    }

    @JsonProperty("type")
    public String getType() {
        return type;
    }

    public InterfaceDTO setType(String type) {
        this.type = type;
        return this;
    }

    @JsonIgnore
    public Interface getAnInterface() {
        return anInterface;
    }

    @JsonIgnore
    public List<BindingDTO> getBindingDTOS() {
        return bindingDTOS;
    }

    @JsonIgnore
    public Interface getDomainObject() {
        return anInterface
                .setBindings(bindingDTOS != null ?
                    bindingDTOS
                        .stream()
                        .map(BindingDTO::getDomainObject)
                        .toList() : null
                )
                ;
    }







}
