package net.ihe.gazelle.servicemetadata.jaxrs.api.interlay.dto;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.JsonTypeName;
import net.ihe.gazelle.modelmarshaller.interlay.dto.DTOFactory;
import net.ihe.gazelle.modelmarshaller.interlay.dto.DTOFactoryProviderSPI;
import net.ihe.gazelle.servicemetadata.api.domain.structure.Interface;
import net.ihe.gazelle.servicemetadata.api.domain.structure.Service;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

import java.util.ArrayList;
import java.util.List;

@Schema(name = "Service")
@JsonTypeName("service")
@JsonPropertyOrder({"name", "version", "instanceId", "replicaId", "providedInterfaces", "consumedInterfaces"})
public class ServiceDTO implements DTO<Service>{

    private Service service;

    private List<InterfaceDTO> providedInterfacesDTOs;

    private List<InterfaceDTO> consumedInterfacesDTOs;

    private final DTOFactoryProviderSPI<InterfaceDTO> factoryProvider = new DTOFactoryProviderSPI<>();




    public ServiceDTO() {
        this.service = new Service();
        this.providedInterfacesDTOs = new ArrayList<>();
        this.consumedInterfacesDTOs = new ArrayList<>();
    }

    public ServiceDTO(Service service) {
        this.service = service;
        this.providedInterfacesDTOs = service.getProvidedInterfaces() != null ? service.getProvidedInterfaces()
                    .stream()
                    .map(anInterface -> ((DTOFactory<InterfaceDTO, Interface>)factoryProvider.createFactory(anInterface.getClass())).createDTO(anInterface))
                    .toList()
            : null;
        this.consumedInterfacesDTOs = service.getConsumedInterfaces() != null ? service.getConsumedInterfaces()
                    .stream()
                    .map(anInterface -> ((DTOFactory<InterfaceDTO, Interface>)factoryProvider.createFactory(anInterface.getClass())).createDTO(anInterface))
                    .toList()
            : null;

    }

    @JsonProperty(value = "instanceId")
    public String getInstanceId() {
        return this.service.getInstanceId();
    }

    public ServiceDTO setInstanceId(String instanceId) {
        this.service.setInstanceId(instanceId);
        return this;
    }

    @JsonProperty(value = "replicaId")
    public String getReplicaId() {
        return this.service.getReplicaId();
    }

    public ServiceDTO setReplicaId(String replicaId) {
        this.service.setReplicaId(replicaId);
        return this;
    }

    @JsonProperty(value = "name")
    public String getName() {
        return this.service.getName();
    }

    public ServiceDTO setName(String name) {
        this.service.setName(name);
        return this;
    }

    @JsonProperty(value = "version")
    public String getVersion() {
        return this.service.getVersion();
    }

    public ServiceDTO setVersion(String version) {
        this.service.setVersion(version);
        return this;
    }

    @JsonProperty(value = "providedInterfaces")
    public List<InterfaceDTO> getProvidedInterfaces() {
        return this.providedInterfacesDTOs;
    }

    public ServiceDTO setProvidedInterfaces(List<InterfaceDTO> providedInterfaceDTOs) {
        this.providedInterfacesDTOs = providedInterfaceDTOs;
        return this;
    }

    @JsonProperty(value = "consumedInterfaces")
    public List<InterfaceDTO> getConsumedInterfaces() {
        return this.consumedInterfacesDTOs;
    }

    public ServiceDTO setConsumedInterfaces(List<InterfaceDTO> consumedInterfaceDTOs) {
        this.consumedInterfacesDTOs = consumedInterfaceDTOs;
        return this;
    }

    @JsonIgnore
    public Service getDomainObject() {
        return this.service
                .setConsumedInterfaces(this.consumedInterfacesDTOs
                        .stream()
                        .map(InterfaceDTO::getDomainObject)
                        .toList()
                )
                .setProvidedInterfaces(
                        this.providedInterfacesDTOs
                                .stream()
                                .map(InterfaceDTO::getDomainObject)
                                .toList()
                );
    }


    @JsonIgnore
    public Service getService() {
        return service;
    }

    @JsonIgnore
    public List<InterfaceDTO> getProvidedInterfacesDTOs() {
        return providedInterfacesDTOs;
    }

    @JsonIgnore
    public List<InterfaceDTO> getConsumedInterfacesDTOs() {
        return consumedInterfacesDTOs;
    }

    @JsonIgnore
    public DTOFactoryProviderSPI<InterfaceDTO> getFactoryProvider() {
        return factoryProvider;
    }
}
