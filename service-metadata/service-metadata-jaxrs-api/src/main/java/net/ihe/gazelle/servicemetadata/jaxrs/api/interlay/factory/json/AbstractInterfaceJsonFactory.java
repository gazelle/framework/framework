package net.ihe.gazelle.servicemetadata.jaxrs.api.interlay.factory.json;

import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import net.ihe.gazelle.modelmarshaller.interlay.mapper.StructureJsonFactory;
import net.ihe.gazelle.modelmarshaller.interlay.mapper.StructureJsonFactoryProvider;
import net.ihe.gazelle.modelmarshaller.interlay.mapper.StructureJsonFactoryProviderSPI;
import net.ihe.gazelle.servicemetadata.jaxrs.api.interlay.dto.BindingDTO;
import net.ihe.gazelle.servicemetadata.jaxrs.api.interlay.dto.InterfaceDTO;

import java.util.ArrayList;
import java.util.List;

public abstract class AbstractInterfaceJsonFactory implements StructureJsonFactory<InterfaceDTO> {

    public static final String BINDINGS = "bindings";
    private final StructureJsonFactoryProvider<BindingDTO> bindingJsonFactoryProviderSPI = new StructureJsonFactoryProviderSPI<>();

    protected List<BindingDTO> getBindings(ObjectNode objectNode) {
        if(!objectNode.has(BINDINGS) || objectNode.get(BINDINGS).isNull()){
            return null;
        }
        ArrayNode bindingArray = (ArrayNode) objectNode.get(BINDINGS);
        List<BindingDTO> bindings = new ArrayList<>();
        for (int i = 0; i < bindingArray.size(); i++) {
            ObjectNode bindingNode = (ObjectNode) bindingArray.get(i);
            BindingDTO binding = bindingJsonFactoryProviderSPI.getFactory(bindingNode.get("type").asText())
                    .createStructure(bindingNode);
            bindings.add(binding);
        }

        return bindings;

    }

}
