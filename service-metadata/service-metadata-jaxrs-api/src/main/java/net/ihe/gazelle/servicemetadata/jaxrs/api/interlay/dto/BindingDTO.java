package net.ihe.gazelle.servicemetadata.jaxrs.api.interlay.dto;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import net.ihe.gazelle.servicemetadata.api.domain.structure.Binding;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

@Schema(oneOf = {RestBindingDTO.class}, name = "Binding")
@JsonDeserialize(using = BindingDeserializer.class)
public abstract class BindingDTO implements DTO<Binding>{

    protected Binding binding;

    private String type;


    @JsonIgnore
    public Binding getDomainObject() {
        return getBinding();
    }





    @JsonIgnore
    public Binding getBinding() {
        return binding;
    }

    @JsonProperty("type")
    public String getType() {
        return type;
    }

    public BindingDTO setType(String type) {
        this.type = type;
        return this;
    }

}
