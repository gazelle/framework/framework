package net.ihe.gazelle.servicemetadata.jaxrs.api.interlay.ws;

import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.Response;
import net.ihe.gazelle.servicemetadata.jaxrs.api.interlay.dto.ServiceDTO;
import org.eclipse.microprofile.openapi.annotations.Operation;
import org.eclipse.microprofile.openapi.annotations.media.Content;
import org.eclipse.microprofile.openapi.annotations.media.Schema;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponse;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;
import org.eclipse.microprofile.openapi.annotations.tags.Tags;


public interface MetadataApiWS {

    @Tags(
            {@Tag(name = "Metadata Service API")}
    )

    @Operation(
            summary = "Get Validation Service Metadata",
            description = "Get Validation Service Metadata and Capabilities"
    )
    @GET
    @Path("/")
    @Produces({"application/json"})
    @APIResponse(
            responseCode = "200",
            description = "Service Validation Metadata",
            content = @Content(
                    mediaType = "application/json",
                    schema = @Schema(implementation = ServiceDTO.class),
                    example = "{\"name\":\"name\",\"version\":\"version\",\"instanceId\":\"instanceId\",\"replicaId\":\"replicaId\",\"providedInterfaces\":[{\"type\":\"validationInterface\",\"interfaceName\":\"interfaceName\",\"interfaceVersion\":\"interfaceVersion\",\"required\":true,\"bindings\":[{\"type\":\"restBinding\",\"serviceUrl\":\"http://localhost:8080\"}],\"validationProfiles\":[{\"profileID\":\"profileID\",\"profileName\":\"profileName\",\"domain\":\"domain\",\"coveredItems\":[\"item1\"]}]}],\"consumedInterfaces\":[{\"type\":\"validationInterface\",\"interfaceName\":\"ConsumedInterfaceName\",\"interfaceVersion\":\"ConsumedInterfaceVersion\",\"required\":true,\"bindings\":[{\"type\":\"restBinding\",\"serviceUrl\":\"http://localhost:8080\"}],\"validationProfiles\":[{\"profileID\":\"profileID\",\"profileName\":\"profileName\",\"domain\":\"domain\",\"coveredItems\":[\"item1\"]}]}]}"
            )
    )
    @APIResponse(
            responseCode = "500",
            description = "Server Error, cannot get metadata"
    )
    Response getServiceMetadata();
}
