package net.ihe.gazelle.servicemetadata.api.application;

public interface MetadataServiceFactory {

    MetadataService getMetadataService();
}
