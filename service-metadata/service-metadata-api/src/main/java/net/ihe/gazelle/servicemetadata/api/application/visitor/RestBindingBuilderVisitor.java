package net.ihe.gazelle.servicemetadata.api.application.visitor;

import net.ihe.gazelle.framework.modelvalidator.domain.BuilderVisitor;
import net.ihe.gazelle.framework.modelvalidator.domain.RootValidatorBuilder;
import net.ihe.gazelle.framework.modelvalidator.domain.ValidatorBuilderFactory;
import net.ihe.gazelle.servicemetadata.api.domain.structure.RestBinding;
import net.ihe.gazelle.servicemetadata.api.domain.validator.supplier.RestBindingValidatorBuilderSupplier;

public class RestBindingBuilderVisitor implements BuilderVisitor {

    public static final RestBindingBuilderVisitor INSTANCE = new RestBindingBuilderVisitor();

    @Override
    public void visit(RootValidatorBuilder<?> rootValidatorBuilder, ValidatorBuilderFactory validatorBuilderFactory) {
        rootValidatorBuilder.addValidatorBuilder(RestBinding.class, new RestBindingValidatorBuilderSupplier(validatorBuilderFactory));
    }
}
