package net.ihe.gazelle.servicemetadata.api.application.validator;

import net.ihe.gazelle.framework.modelvalidator.domain.RootValidatorBuilder;
import net.ihe.gazelle.framework.modelvalidator.domain.StructureValidator;
import net.ihe.gazelle.framework.modelvalidator.domain.Validator;
import net.ihe.gazelle.framework.modelvalidator.domain.ValidatorBuilderFactory;
import net.ihe.gazelle.framework.modelvalidator.domain.result.ObjectResult;
import net.ihe.gazelle.servicemetadata.api.application.visitor.ServiceBuilderVisitor;
import net.ihe.gazelle.servicemetadata.api.domain.structure.Service;

public class ServiceValidator implements StructureValidator<Service> {


    private final Validator<Service> validator;

    public ServiceValidator(ValidatorBuilderFactory validatorBuilderFactory) {
        RootValidatorBuilder<Service> validatorBuilder = new RootValidatorBuilder<>(Service.class);
        ServiceBuilderVisitor.INSTANCE.visit(validatorBuilder, validatorBuilderFactory);
        this.validator = validatorBuilder.build();
    }

    @Override
    public ObjectResult validate(Service object) {
        return validator.validate(object);
    }

    @Override
    public void assertValid(Service object) {
        validator.assertValid(object);
    }

    @Override
    public boolean isValid(Service object) {
        return validator.isValid(object);
    }
}
