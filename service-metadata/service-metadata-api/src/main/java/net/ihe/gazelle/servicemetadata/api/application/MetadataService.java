package net.ihe.gazelle.servicemetadata.api.application;

import net.ihe.gazelle.lang.UnexpectedInternalErrorException;
import net.ihe.gazelle.servicemetadata.api.domain.structure.Service;

/**
 * This interface is used to get the metadata of a Gazelle Service.
 * Every service in the Gazelle Test Bed should implement this interface.
 */
public interface MetadataService {

    /**
     * The implementation of this method should return the metadata of the corresponding service.
     * @return {@link Service} The metadata of the corresponding service.
     * @throws UnexpectedInternalErrorException if an unexpected error occurs during the retrieval of the {@link Service} metadata.
     */
    Service getMetadata();

}
