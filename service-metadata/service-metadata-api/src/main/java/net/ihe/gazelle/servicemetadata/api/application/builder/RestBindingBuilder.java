package net.ihe.gazelle.servicemetadata.api.application.builder;

import net.ihe.gazelle.framework.modelvalidator.domain.AbstractBuilder;
import net.ihe.gazelle.framework.modelvalidator.domain.BuilderVisitor;
import net.ihe.gazelle.framework.modelvalidator.domain.ValidatorBuilderFactory;
import net.ihe.gazelle.servicemetadata.api.application.visitor.RestBindingBuilderVisitor;
import net.ihe.gazelle.servicemetadata.api.domain.structure.Binding;
import net.ihe.gazelle.servicemetadata.api.domain.structure.RestBinding;

public class RestBindingBuilder extends AbstractBuilder<Binding> {

    private String serviceUrl;

    public RestBindingBuilder(ValidatorBuilderFactory validatorBuilderFactory) {
        super(validatorBuilderFactory);
    }

    public RestBindingBuilder setServiceUrl(String serviceUrl) {
        this.serviceUrl = serviceUrl;
        return this;
    }

    @Override
    protected Class<? extends Binding> getParameterClass() {
        return RestBinding.class;
    }

    @Override
    protected BuilderVisitor getBuilderVisitor() {
        return RestBindingBuilderVisitor.INSTANCE;
    }

    @Override
    protected void make() {
        this.<RestBinding>getObjectToBuild()
                .setServiceUrl(serviceUrl);
    }
}
