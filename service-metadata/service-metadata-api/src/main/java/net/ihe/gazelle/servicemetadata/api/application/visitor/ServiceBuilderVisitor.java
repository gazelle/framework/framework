package net.ihe.gazelle.servicemetadata.api.application.visitor;

import net.ihe.gazelle.framework.modelvalidator.domain.BuilderVisitor;
import net.ihe.gazelle.framework.modelvalidator.domain.RootValidatorBuilder;
import net.ihe.gazelle.framework.modelvalidator.domain.ValidatorBuilderFactory;
import net.ihe.gazelle.servicemetadata.api.domain.structure.Service;
import net.ihe.gazelle.servicemetadata.api.domain.validator.supplier.ServiceValidatorBuilderSupplier;

public class ServiceBuilderVisitor implements BuilderVisitor {

    public static final ServiceBuilderVisitor INSTANCE = new ServiceBuilderVisitor();

    @Override
    public void visit(RootValidatorBuilder<?> rootValidatorBuilder, ValidatorBuilderFactory validatorBuilderFactory) {
        rootValidatorBuilder.addValidatorBuilder(Service.class, new ServiceValidatorBuilderSupplier(validatorBuilderFactory));
        InterfaceBuilderVisitor.INSTANCE.visit(rootValidatorBuilder, validatorBuilderFactory);

    }
}
