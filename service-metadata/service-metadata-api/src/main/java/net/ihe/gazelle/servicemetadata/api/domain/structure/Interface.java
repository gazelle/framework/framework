package net.ihe.gazelle.servicemetadata.api.domain.structure;

import java.util.ArrayList;
import java.util.List;

public abstract class Interface {

    private String interfaceName;
    private String interfaceVersion;
    private boolean required;
    private List<Binding> bindings = new ArrayList<>();

    public String getInterfaceName() {
        return interfaceName;
    }

    public Interface setInterfaceName(String interfaceName) {
        this.interfaceName = interfaceName;
        return this;
    }

    public String getInterfaceVersion() {
        return interfaceVersion;
    }

    public Interface setInterfaceVersion(String interfaceVersion) {
        this.interfaceVersion = interfaceVersion;
        return this;
    }

    public boolean isRequired() {
        return required;
    }

    public Interface setRequired(boolean required) {
        this.required = required;
        return this;
    }

    public List<Binding> getBindings() {
        return bindings;
    }

    public Interface setBindings(List<Binding> bindings) {
        this.bindings = bindings;
        return this;
    }

    public Interface addBinding(Binding binding) {
        this.bindings.add(binding);
        return this;
    }

    public boolean isInterfaceNameValid(){
       return interfaceName != null && !interfaceName.isBlank();
    }

    public boolean isInterfaceVersionValid(){
       return interfaceVersion != null && !interfaceVersion.isBlank();
    }

    public boolean isBindingsValid() {
        return bindings != null && !bindings.isEmpty();
    }

}
