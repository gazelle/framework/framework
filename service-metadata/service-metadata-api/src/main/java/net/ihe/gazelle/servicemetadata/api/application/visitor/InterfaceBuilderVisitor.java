package net.ihe.gazelle.servicemetadata.api.application.visitor;

import net.ihe.gazelle.framework.modelvalidator.domain.BuilderVisitor;
import net.ihe.gazelle.framework.modelvalidator.domain.RootValidatorBuilder;
import net.ihe.gazelle.framework.modelvalidator.domain.ValidatorBuilderFactory;
import net.ihe.gazelle.servicemetadata.api.domain.structure.Interface;
import net.ihe.gazelle.servicemetadata.api.domain.validator.supplier.InterfaceValidatorBuilderSupplier;

public class InterfaceBuilderVisitor implements BuilderVisitor {

    public static final InterfaceBuilderVisitor INSTANCE = new InterfaceBuilderVisitor();

    @Override
    public void visit(RootValidatorBuilder<?> rootValidatorBuilder, ValidatorBuilderFactory validatorBuilderFactory) {
        rootValidatorBuilder.addValidatorBuilder(Interface.class, new InterfaceValidatorBuilderSupplier(validatorBuilderFactory));
        RestBindingBuilderVisitor.INSTANCE.visit(rootValidatorBuilder, validatorBuilderFactory);
    }
}
