package net.ihe.gazelle.servicemetadata.api.domain.validator.supplier;

import net.ihe.gazelle.framework.modelvalidator.domain.AbstractValidatorBuilderSupplier;
import net.ihe.gazelle.framework.modelvalidator.domain.ValidatorBuilder;
import net.ihe.gazelle.framework.modelvalidator.domain.ValidatorBuilderFactory;
import net.ihe.gazelle.servicemetadata.api.domain.structure.Interface;
import net.ihe.gazelle.servicemetadata.api.domain.structure.Service;

public class ServiceValidatorBuilderSupplier extends AbstractValidatorBuilderSupplier<Service> {


    public ServiceValidatorBuilderSupplier(ValidatorBuilderFactory validatorBuilderFactory) {
        super(validatorBuilderFactory);
    }

    @Override
    protected Class<? extends Service> getType() {
        return Service.class;
    }

    @Override
    protected ValidatorBuilder<Service> supplyRulesAndMembers(ValidatorBuilder<Service> validatorBuilder) {
        return validatorBuilder
                .addRule("nameValid", Service::isNameValid, "Name must be set and not empty")
                .addRule("versionValid", Service::isVersionValid, "Version must be set and not empty")
                .addRule("instanceIdValid", Service::isInstanceIdValid, "Instance Id must be set and not empty")
                .addRule("replicaIdValid", Service::isReplicaIdValid, "Replica Id must be non empty if present")
                .addRule("providedInterfacesValid", Service::isProvidedInterfacesValid, "Provided Interfaces must be not empty if present")
                .addRule("consumedInterfacesValid", Service::isConsumedInterfacesValid, "Consumed Interfaces must be not empty if present")
                .addMember("providedInterfaces", Service::getProvidedInterfaces, Interface.class)
                .addMember("consumedInterfaces", Service::getConsumedInterfaces,Interface.class);
    }
}
