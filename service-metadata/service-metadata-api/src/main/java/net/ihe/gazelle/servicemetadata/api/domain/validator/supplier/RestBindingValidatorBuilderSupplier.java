package net.ihe.gazelle.servicemetadata.api.domain.validator.supplier;

import net.ihe.gazelle.framework.modelvalidator.domain.AbstractValidatorBuilderSupplier;
import net.ihe.gazelle.framework.modelvalidator.domain.ValidatorBuilder;
import net.ihe.gazelle.framework.modelvalidator.domain.ValidatorBuilderFactory;
import net.ihe.gazelle.servicemetadata.api.domain.structure.Binding;
import net.ihe.gazelle.servicemetadata.api.domain.structure.RestBinding;

public class RestBindingValidatorBuilderSupplier extends AbstractValidatorBuilderSupplier<Binding> {

    public RestBindingValidatorBuilderSupplier(ValidatorBuilderFactory validatorBuilderFactory) {
        super(validatorBuilderFactory);
    }

    @Override
    protected Class<? extends Binding> getType() {
        return RestBinding.class;
    }

    @Override
    protected ValidatorBuilder<Binding> supplyRulesAndMembers(ValidatorBuilder<Binding> validatorBuilder) {
        return validatorBuilder
                .addRule("serviceUrlValid", c -> ((RestBinding) c).isServiceUrlValid(), "Service Url must be set and not empty");
    }
}
