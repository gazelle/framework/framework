package net.ihe.gazelle.servicemetadata.api.application.validator;

import net.ihe.gazelle.framework.modelvalidator.domain.RootValidatorBuilder;
import net.ihe.gazelle.framework.modelvalidator.domain.StructureValidator;
import net.ihe.gazelle.framework.modelvalidator.domain.Validator;
import net.ihe.gazelle.framework.modelvalidator.domain.ValidatorBuilderFactory;
import net.ihe.gazelle.framework.modelvalidator.domain.result.ObjectResult;
import net.ihe.gazelle.servicemetadata.api.application.visitor.InterfaceBuilderVisitor;
import net.ihe.gazelle.servicemetadata.api.domain.structure.Interface;

public class InterfaceValidator implements StructureValidator<Interface> {

    private final Validator<Interface> validator;


    public InterfaceValidator(ValidatorBuilderFactory validatorBuilderFactory) {
        RootValidatorBuilder<Interface> validatorBuilder = new RootValidatorBuilder<>(Interface.class);
        InterfaceBuilderVisitor.INSTANCE.visit(validatorBuilder, validatorBuilderFactory);
        this.validator = validatorBuilder.build();
    }


    @Override
    public ObjectResult validate(Interface object) {
        return validator.validate(object);
    }

    @Override
    public void assertValid(Interface object) {
        validator.assertValid(object);
    }

    @Override
    public boolean isValid(Interface object) {
        return validator.isValid(object);
    }
}
