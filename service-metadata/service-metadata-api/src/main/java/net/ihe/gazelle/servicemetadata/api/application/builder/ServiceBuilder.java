package net.ihe.gazelle.servicemetadata.api.application.builder;

import net.ihe.gazelle.framework.modelvalidator.domain.AbstractBuilder;
import net.ihe.gazelle.framework.modelvalidator.domain.BuilderVisitor;
import net.ihe.gazelle.framework.modelvalidator.domain.ValidatorBuilderFactory;
import net.ihe.gazelle.servicemetadata.api.application.visitor.ServiceBuilderVisitor;
import net.ihe.gazelle.servicemetadata.api.domain.structure.Interface;
import net.ihe.gazelle.servicemetadata.api.domain.structure.Service;

import java.util.ArrayList;
import java.util.List;

public class ServiceBuilder extends AbstractBuilder<Service> {

    private String name;
    private String version;
    private String instanceId;
    private String replicaId;
    private List<Interface> providedInterfaces;
    private List<Interface> consumedInterfaces;

    public ServiceBuilder(ValidatorBuilderFactory validatorBuilderFactory) {
        super(validatorBuilderFactory);
    }

    public ServiceBuilder setName(String name) {
        this.name = name;
        return this;
    }

    public ServiceBuilder setVersion(String version) {
        this.version = version;
        return this;
    }

    public ServiceBuilder setInstanceId(String instanceId) {
        this.instanceId = instanceId;
        return this;
    }

    public ServiceBuilder setReplicaId(String replicaId) {
        this.replicaId = replicaId;
        return this;
    }

    public ServiceBuilder addProvidedInterface(Interface providedInterface) {
        if(this.providedInterfaces == null) this.providedInterfaces = new ArrayList<>();
        this.providedInterfaces.add(providedInterface);
        return this;
    }

    public ServiceBuilder addConsumedInterface(Interface consumedInterface) {
        if(this.consumedInterfaces == null) this.consumedInterfaces = new ArrayList<>();
        this.consumedInterfaces.add(consumedInterface);
        return this;
    }

    @Override
    protected Class<? extends Service> getParameterClass() {
        return Service.class;
    }

    @Override
    protected BuilderVisitor getBuilderVisitor() {
        return ServiceBuilderVisitor.INSTANCE;
    }

    @Override
    protected void make() {
        getObjectToBuild()
                .setName(name)
                .setVersion(version)
                .setInstanceId(instanceId)
                .setReplicaId(replicaId)
                .setProvidedInterfaces(providedInterfaces)
                .setConsumedInterfaces(consumedInterfaces);
    }
}
