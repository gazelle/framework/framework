package net.ihe.gazelle.servicemetadata.api.domain.validator.supplier;

import net.ihe.gazelle.framework.modelvalidator.domain.AbstractValidatorBuilderSupplier;
import net.ihe.gazelle.framework.modelvalidator.domain.ValidatorBuilder;
import net.ihe.gazelle.framework.modelvalidator.domain.ValidatorBuilderFactory;
import net.ihe.gazelle.servicemetadata.api.domain.structure.Binding;
import net.ihe.gazelle.servicemetadata.api.domain.structure.Interface;

public class InterfaceValidatorBuilderSupplier extends AbstractValidatorBuilderSupplier<Interface> {


    public InterfaceValidatorBuilderSupplier(ValidatorBuilderFactory validatorBuilderFactory) {
        super(validatorBuilderFactory);

    }

    @Override
    protected Class<? extends Interface> getType() {
        return Interface.class;
    }

    @Override
    protected ValidatorBuilder<Interface> supplyRulesAndMembers(ValidatorBuilder<Interface> validatorBuilder) {
        return validatorBuilder
                    .addRule("interfaceNameValid", Interface::isInterfaceNameValid, "Interface name must be set and not empty")
                    .addRule("interfaceVersionValid", Interface::isInterfaceVersionValid, "Interface version must be set and not empty")
                    .addRule("bindings", Interface::isBindingsValid, "Bindings must have at least one element")
                    .addMember("bindings", Interface::getBindings, Binding.class);

    }
}
