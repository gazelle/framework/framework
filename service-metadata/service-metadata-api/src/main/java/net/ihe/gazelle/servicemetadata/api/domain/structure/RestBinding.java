package net.ihe.gazelle.servicemetadata.api.domain.structure;

public class RestBinding implements Binding {

    private String serviceUrl;

    public String getServiceUrl() {
        return serviceUrl;
    }

    public RestBinding setServiceUrl(String serviceUrl) {
        this.serviceUrl = serviceUrl;
        return this;
    }

    public boolean isServiceUrlValid(){
        return serviceUrl != null && !serviceUrl.isBlank();
    }

}
