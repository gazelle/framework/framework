package net.ihe.gazelle.servicemetadata.api.application.builder;

import net.ihe.gazelle.framework.modelvalidator.domain.AbstractBuilder;
import net.ihe.gazelle.framework.modelvalidator.domain.BuilderVisitor;
import net.ihe.gazelle.framework.modelvalidator.domain.ValidatorBuilderFactory;
import net.ihe.gazelle.servicemetadata.api.application.visitor.InterfaceBuilderVisitor;
import net.ihe.gazelle.servicemetadata.api.domain.structure.Binding;
import net.ihe.gazelle.servicemetadata.api.domain.structure.Interface;

import java.util.ArrayList;
import java.util.List;

public abstract class InterfaceBuilder extends AbstractBuilder<Interface> {

    private String interfaceName;
    private String interfaceVersion;
    private boolean required;
    private List<Binding> bindings;

    public InterfaceBuilder(ValidatorBuilderFactory validatorBuilderFactory) {
        super(validatorBuilderFactory);
    }

    public InterfaceBuilder setInterfaceName(String interfaceName) {
        this.interfaceName = interfaceName;
        return this;
    }

    public InterfaceBuilder setInterfaceVersion(String interfaceVersion) {
        this.interfaceVersion = interfaceVersion;
        return this;
    }

    public InterfaceBuilder setRequired(boolean required) {
        this.required = required;
        return this;
    }

    public InterfaceBuilder addBinding(Binding binding) {
        if (this.bindings == null) {
            this.bindings = new ArrayList<>();
        }
        this.bindings.add(binding);
        return this;
    }

    public InterfaceBuilder setBindings(List<Binding> bindings) {
        this.bindings = bindings;
        return this;
    }

    @Override
    protected BuilderVisitor getBuilderVisitor() {
        return InterfaceBuilderVisitor.INSTANCE;
    }

    @Override
    protected Class<? extends Interface> getParameterClass() {
        return Interface.class;
    }

    protected void make(){
        super.getObjectToBuild()
                .setInterfaceName(interfaceName)
                .setInterfaceVersion(interfaceVersion)
                .setRequired(required)
                .setBindings(bindings);
    }
}
