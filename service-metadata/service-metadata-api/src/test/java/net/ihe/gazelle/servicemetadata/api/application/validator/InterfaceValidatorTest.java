package net.ihe.gazelle.servicemetadata.api.application.validator;

import net.ihe.gazelle.framework.modelvalidator.domain.result.ObjectResult;
import net.ihe.gazelle.framework.modelvalidator.interlay.adapter.BValidatorBuilderFactory;
import net.ihe.gazelle.servicemetadata.api.domain.MockInterface;
import net.ihe.gazelle.servicemetadata.api.domain.structure.Interface;
import net.ihe.gazelle.servicemetadata.api.domain.structure.RestBinding;
import net.ihe.gazelle.servicemetadata.api.domain.structure.Service;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class InterfaceValidatorTest {



    private final InterfaceValidator interfaceValidator = new InterfaceValidator(new BValidatorBuilderFactory());

    @Test
    public void testValidateValid(){
        ObjectResult result = interfaceValidator.validate(getInterface());
        assertTrue(result.isValid());
    }

    @Test
    public void testAssertValidValid(){

        interfaceValidator.assertValid(getInterface());
    }

    @Test
    public void testIsValidValid(){
        assertTrue(interfaceValidator.isValid(getInterface()));
    }

    private Interface getInterface(){
        return new MockInterface()
                .setInterfaceName("consumedInterface")
                .setInterfaceVersion("0.1")
                .setRequired(false)
                .addBinding(new RestBinding()
                        .setServiceUrl("http://localhost:8080"));

    }
}
