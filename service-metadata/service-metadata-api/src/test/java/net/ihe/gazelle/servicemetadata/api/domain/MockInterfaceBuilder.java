package net.ihe.gazelle.servicemetadata.api.domain;

import net.ihe.gazelle.framework.modelvalidator.domain.BuilderVisitor;
import net.ihe.gazelle.framework.modelvalidator.domain.ValidatorBuilderFactory;
import net.ihe.gazelle.servicemetadata.api.application.builder.InterfaceBuilder;
import net.ihe.gazelle.servicemetadata.api.domain.structure.Interface;

public class MockInterfaceBuilder extends InterfaceBuilder {


    public MockInterfaceBuilder(ValidatorBuilderFactory validatorBuilderFactory) {
        super(validatorBuilderFactory);
    }

    @Override
    protected Class<? extends Interface> getParameterClass() {
        return MockInterface.class;
    }

    @Override
    protected BuilderVisitor getBuilderVisitor() {
        return MockInterfaceBuilderVisitor.INSTANCE;
    }


}
