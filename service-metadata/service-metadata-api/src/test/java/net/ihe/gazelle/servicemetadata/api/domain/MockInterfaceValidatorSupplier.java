package net.ihe.gazelle.servicemetadata.api.domain;

import net.ihe.gazelle.framework.modelvalidator.domain.ValidatorBuilder;
import net.ihe.gazelle.framework.modelvalidator.domain.ValidatorBuilderFactory;
import net.ihe.gazelle.servicemetadata.api.domain.structure.Interface;
import net.ihe.gazelle.servicemetadata.api.domain.validator.supplier.InterfaceValidatorBuilderSupplier;

public class MockInterfaceValidatorSupplier extends InterfaceValidatorBuilderSupplier {

    public MockInterfaceValidatorSupplier(ValidatorBuilderFactory validatorBuilderFactory) {
        super(validatorBuilderFactory);
    }

    @Override
    protected Class<? extends Interface> getType() {
        return MockInterface.class;
    }

    @Override
    protected ValidatorBuilder<Interface> supplyRulesAndMembers(ValidatorBuilder<Interface> validatorBuilder) {
        return super
                .supplyRulesAndMembers(validatorBuilder)
                .addRule("nothing", (mockInterface -> true), "nothing");

    }
}
