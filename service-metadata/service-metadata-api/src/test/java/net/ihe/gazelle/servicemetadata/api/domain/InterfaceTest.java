package net.ihe.gazelle.servicemetadata.api.domain;

import net.ihe.gazelle.framework.modelvalidator.domain.ValidationException;
import net.ihe.gazelle.framework.modelvalidator.domain.ValidatorBuilderFactory;
import net.ihe.gazelle.framework.modelvalidator.interlay.adapter.BValidatorBuilderFactory;
import net.ihe.gazelle.servicemetadata.api.domain.structure.Interface;
import net.ihe.gazelle.servicemetadata.api.domain.structure.RestBinding;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class InterfaceTest {

    private final ValidatorBuilderFactory validatorBuilderFactory = new BValidatorBuilderFactory();

    @Test
    public void testAllValid(){
        Interface interfaceObject = new MockInterfaceBuilder(validatorBuilderFactory)
                .setInterfaceName("consumedInterface")
                .setInterfaceVersion("0.1")
                .setRequired(false)
                .addBinding(new RestBinding()
                        .setServiceUrl("http://localhost:8080"))
                .build();
        assertEquals("consumedInterface", interfaceObject.getInterfaceName());
        assertEquals("0.1", interfaceObject.getInterfaceVersion());
        assertFalse(interfaceObject.isRequired());
        assertEquals(1, interfaceObject.getBindings().size());
        assertEquals("http://localhost:8080", ((RestBinding) interfaceObject.getBindings().get(0)).getServiceUrl());
    }

    @Test
    public void testInvalid(){
        Throwable throwable = assertThrows(ValidationException.class, () -> new MockInterfaceBuilder(validatorBuilderFactory)
                .setInterfaceName(null)
                .setInterfaceVersion("0.1")
                .setRequired(false)
                .addBinding(new RestBinding()
                        .setServiceUrl("http://localhost:8080"))
                .build());
        assertTrue(throwable.getMessage().contains("MockInterface [interfaceNameValid] Interface name must be set and not empty => invalid\n"));

    }

}
