package net.ihe.gazelle.servicemetadata.api.domain;

import net.ihe.gazelle.framework.modelvalidator.domain.ValidationException;
import net.ihe.gazelle.framework.modelvalidator.interlay.adapter.BValidatorBuilderFactory;
import net.ihe.gazelle.servicemetadata.api.application.builder.RestBindingBuilder;
import net.ihe.gazelle.servicemetadata.api.domain.structure.RestBinding;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class RestBindingTest {


    private static BValidatorBuilderFactory validatorBuilderFactory;

    @BeforeAll
    public static void setUp(){
        validatorBuilderFactory = new BValidatorBuilderFactory();
    }

    @Test
    void testBuilderWithAllValid(){
        RestBinding restBinding = new RestBindingBuilder(validatorBuilderFactory)
                .setServiceUrl("http://localhost:8080")
                .build();
        assertEquals("http://localhost:8080", restBinding.getServiceUrl());
    }

    @Test
    void testBuilderInvalidUrl(){
        Throwable throwable = assertThrows(ValidationException.class, () -> new RestBindingBuilder(validatorBuilderFactory)
                .setServiceUrl(" ")
                .build());
        System.out.println(throwable.getMessage());
        assertTrue(throwable.getMessage().contains("Binding [serviceUrlValid] Service Url must be set and not empty => invalid"));
    }
}
