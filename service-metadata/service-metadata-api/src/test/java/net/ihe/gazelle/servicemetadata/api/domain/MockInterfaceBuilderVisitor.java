package net.ihe.gazelle.servicemetadata.api.domain;

import net.ihe.gazelle.framework.modelvalidator.domain.RootValidatorBuilder;
import net.ihe.gazelle.framework.modelvalidator.domain.ValidatorBuilderFactory;
import net.ihe.gazelle.servicemetadata.api.application.visitor.InterfaceBuilderVisitor;

public class MockInterfaceBuilderVisitor extends InterfaceBuilderVisitor {

    public static final MockInterfaceBuilderVisitor INSTANCE = new MockInterfaceBuilderVisitor();

    @Override
    public void visit(RootValidatorBuilder<?> rootValidatorBuilder, ValidatorBuilderFactory validatorBuilderFactory) {
        super.visit(rootValidatorBuilder, validatorBuilderFactory);
        rootValidatorBuilder.addValidatorBuilder(MockInterface.class, new MockInterfaceValidatorSupplier(validatorBuilderFactory));
    }
}