package net.ihe.gazelle.servicemetadata.api.domain;

import net.ihe.gazelle.framework.modelvalidator.domain.ValidationException;
import net.ihe.gazelle.framework.modelvalidator.domain.ValidatorBuilderFactory;
import net.ihe.gazelle.framework.modelvalidator.interlay.adapter.BValidatorBuilderFactory;
import net.ihe.gazelle.servicemetadata.api.application.builder.ServiceBuilder;
import net.ihe.gazelle.servicemetadata.api.domain.structure.RestBinding;
import net.ihe.gazelle.servicemetadata.api.domain.structure.Service;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class ServiceTest {

    private static ValidatorBuilderFactory validatorBuilderFactory;

    @BeforeAll
    public static void setUp() {
        validatorBuilderFactory = new BValidatorBuilderFactory();
    }

    @Test
    void testBuilderMinimumValid() {
        Service service = new ServiceBuilder(validatorBuilderFactory)
                .setName("serviceName")
                .setInstanceId("instanceId")
                .setReplicaId("replicaId")
                .setVersion("0.1")
                .build();
        assertEquals("serviceName", service.getName());
        assertEquals("instanceId", service.getInstanceId());
        assertEquals("replicaId", service.getReplicaId());
        assertEquals("0.1", service.getVersion());
    }

    @Test
    void testBuilderWithAllValid() {
        Service service = new ServiceBuilder(validatorBuilderFactory)
                .setName("serviceName")
                .setInstanceId("instanceId")
                .setReplicaId("replicaId")
                .setVersion("0.1")
                .addProvidedInterface(new MockInterface()
                        .setInterfaceName("providedInterface")
                        .setInterfaceVersion("0.1")
                        .setRequired(false)
                        .addBinding(new RestBinding()
                                .setServiceUrl("http://localhost:8080")))
                .addConsumedInterface(new MockInterface()
                        .setInterfaceName("consumedInterface")
                        .setInterfaceVersion("0.1")
                        .setRequired(true)
                        .addBinding(new RestBinding()
                                .setServiceUrl("http://localhost:8080"))
                )
                .build();
        assertEquals("serviceName", service.getName());
        assertEquals("instanceId", service.getInstanceId());
        assertEquals("replicaId", service.getReplicaId());
        assertEquals("0.1", service.getVersion());
        assertEquals(1, service.getProvidedInterfaces().size());
        assertEquals("providedInterface", service.getProvidedInterfaces().get(0).getInterfaceName());
        assertEquals("0.1", service.getProvidedInterfaces().get(0).getInterfaceVersion());
        assertFalse(service.getProvidedInterfaces().get(0).isRequired());
        assertInstanceOf(RestBinding.class, service.getProvidedInterfaces().get(0).getBindings().get(0));
        assertEquals(1, service.getProvidedInterfaces().get(0).getBindings().size());
        assertEquals("http://localhost:8080", ((RestBinding) service.getProvidedInterfaces().get(0).getBindings().get(0)).getServiceUrl());
        assertEquals(1, service.getConsumedInterfaces().size());
        assertEquals("consumedInterface", service.getConsumedInterfaces().get(0).getInterfaceName());
        assertEquals("0.1", service.getConsumedInterfaces().get(0).getInterfaceVersion());
        assertTrue(service.getConsumedInterfaces().get(0).isRequired());
        assertInstanceOf(MockInterface.class, service.getConsumedInterfaces().get(0));
        MockInterface mockInterface = (MockInterface) service.getConsumedInterfaces().get(0);
        assertEquals(1, mockInterface.getBindings().size());

    }

    @Test
    void testInvalidName() {
        Throwable throwable = assertThrows(ValidationException.class, () ->
                new ServiceBuilder(validatorBuilderFactory)
                        .setName(" ")
                        .setVersion("1.2")
                        .setInstanceId("service-id")
                        .setReplicaId("service-replica-id")
                        .build());
        assertCountErrors(throwable.getMessage(), 1);
        assertTrue(throwable.getMessage().contains("Service [nameValid] Name must be set and not empty => invalid"));
    }

    @Test
    void testInvalidVersion() {
        Throwable throwable = assertThrows(ValidationException.class, () ->
                new ServiceBuilder(validatorBuilderFactory)
                        .setName("service-name")
                        .setVersion(" ")
                        .setInstanceId("service-id")
                        .setReplicaId("service-replica-id")
                        .build());
        assertCountErrors(throwable.getMessage(), 1);
        assertTrue(throwable.getMessage().contains("Service [versionValid] Version must be set and not empty => invalid"));
    }

    @Test
    void testInvalidInstanceId() {
        Throwable throwable = assertThrows(ValidationException.class, () ->
                new ServiceBuilder(validatorBuilderFactory)
                        .setName("service-name")
                        .setVersion("1.2")
                        .setInstanceId(" ")
                        .setReplicaId("service-replica-id")
                        .build());
        assertCountErrors(throwable.getMessage(), 1);
        assertTrue(throwable.getMessage().contains("Service [instanceIdValid] Instance Id must be set and not empty => invalid"));
    }

    @Test
    void testInvalidReplicaId() {
        Throwable throwable = assertThrows(ValidationException.class, () -> new ServiceBuilder(validatorBuilderFactory)
                .setName("service-name")
                .setVersion("1.2")
                .setInstanceId("service-id")
                .setReplicaId(" ")
                .build());
        assertCountErrors(throwable.getMessage(), 1);
        assertTrue(throwable.getMessage().contains("Service [replicaIdValid] Replica Id must be non empty if present => invalid"));
    }

    @Test
    void testInvalidServiceUrl() {

        Throwable throwable = assertThrows(ValidationException.class, () ->
                new ServiceBuilder(validatorBuilderFactory)
                        .setName("service-name")
                        .setVersion("1.2")
                        .setInstanceId("service-id")
                        .setReplicaId("service-replica-id")
                        .addProvidedInterface(new MockInterface()
                                .setInterfaceName("providedInterface")
                                .setInterfaceVersion("0.1")
                                .setRequired(false)
                                .addBinding(new RestBinding()
                                        .setServiceUrl(" ")))
                        .build());
        System.out.println(throwable.getMessage());
        assertTrue(throwable.getMessage().contains("Service.providedInterfaces[0].bindings[0] [serviceUrlValid] Service Url must be set and not empty => invalid"));
    }


    @Test
    void testInvalidConsumedInterface() {
        Throwable throwable = assertThrows(ValidationException.class, () ->
                new ServiceBuilder(validatorBuilderFactory)
                        .setName("service-name")
                        .setVersion("1.2")
                        .setInstanceId("service-id")
                        .setReplicaId("service-replica-id")
                        .addConsumedInterface(new MockInterface()
                                .setInterfaceName("interface-name")
                                .addBinding(new RestBinding()
                                        .setServiceUrl("http://localhost:8080")))
                        .build());
        assertTrue(throwable.getMessage().contains("Service.consumedInterfaces[0] [interfaceVersionValid] Interface version must be set and not empty => invalid"));
        assertCountErrors(throwable.getMessage(), 1);
    }


    @Test
    void testInvalidProvidedAndConsumedInterface() {
        Throwable throwable = assertThrows(ValidationException.class, () ->
                new ServiceBuilder(validatorBuilderFactory)
                        .setName("service-name")
                        .setVersion("1.2")
                        .setInstanceId("service-id")
                        .setReplicaId("service-replica-id")
                        .addConsumedInterface(new MockInterface()
                                .setInterfaceName("pp"))
                        .addProvidedInterface(new MockInterface()
                                .setInterfaceName("cc"))
                        .build());
        assertTrue(throwable.getMessage().contains("Service.providedInterfaces[0] [bindings] Bindings must have at least one element => invalid"));
        assertTrue(throwable.getMessage().contains("Service.providedInterfaces[0] [interfaceVersionValid] Interface version must be set and not empty => invalid"));
        assertTrue(throwable.getMessage().contains("Service.consumedInterfaces[0] [bindings] Bindings must have at least one element => invalid"));
        assertCountErrors(throwable.getMessage(), 4);
    }

    @Test
    public void testEqualAndHashCode() {
        Service service1 = new Service()
                .setName("service-name")
                .setVersion("1.0");
        Service service2 = new Service()
                .setName("service-name")
                .setVersion("1.0");
        assertEquals(service1, service2);
        Service service3 = new Service()
                .setName("service-name")
                .setVersion("1.1");
        assertNotEquals(service1, service3);

        assertEquals(service1.hashCode(), service2.hashCode());
        assertNotEquals(service1.hashCode(), service3.hashCode());
    }

    @Test
    public void testFullStructure() {
        Service service = new Service()
                .setName("service-name")
                .setVersion("1.0")
                .setProvidedInterfaces(List.of(
                        new MockInterface()
                                .setBindings(List.of(
                                        new RestBinding()
                                                .setServiceUrl("http://localhost:8080")
                                ))
                                .setInterfaceName("providedInterface")
                                .setInterfaceVersion("0.1")
                                .setRequired(false)
                ))
                .setConsumedInterfaces(Collections.emptyList())
                .setInstanceId("service-id")
                .setReplicaId("service-replica-id");
        assertEquals("service-name", service.getName());
        assertEquals("1.0", service.getVersion());
        assertEquals("service-id", service.getInstanceId());
        assertEquals("service-replica-id", service.getReplicaId());
        assertEquals(Collections.emptyList(), service.getConsumedInterfaces());
        assertEquals(1, service.getProvidedInterfaces().size());
        assertEquals("providedInterface", service.getProvidedInterfaces().get(0).getInterfaceName());
        assertEquals("0.1", service.getProvidedInterfaces().get(0).getInterfaceVersion());
        assertFalse(service.getProvidedInterfaces().get(0).isRequired());
        assertEquals(1, service.getProvidedInterfaces().get(0).getBindings().size());
        assertEquals("http://localhost:8080", ((RestBinding) service.getProvidedInterfaces().get(0).getBindings().get(0)).getServiceUrl());


    }

    private void assertCountErrors(String message, int count) {
        assertEquals(count, Arrays.stream(message.split("[\\s\\n]")).filter("invalid"::equals).count());
    }


}
