package net.ihe.gazelle.servicemetadata.api.application.validator;

import net.ihe.gazelle.framework.modelvalidator.domain.result.ObjectResult;
import net.ihe.gazelle.framework.modelvalidator.interlay.adapter.BValidatorBuilderFactory;
import net.ihe.gazelle.servicemetadata.api.domain.MockInterface;
import net.ihe.gazelle.servicemetadata.api.domain.structure.RestBinding;
import net.ihe.gazelle.servicemetadata.api.domain.structure.Service;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class ServiceValidatorTest {

    private final ServiceValidator serviceValidator = new ServiceValidator(new BValidatorBuilderFactory());

    @Test
    public void testValidateValid(){
        ObjectResult result = serviceValidator.validate(getService());
        assertTrue(result.isValid());
    }

    @Test
    public void testAssertValidValid(){

        serviceValidator.assertValid(getService());
    }

    @Test
    public void testIsValidValid(){
        assertTrue(serviceValidator.isValid(getService()));
    }

    private Service getService(){
        return new Service()
                .setName("serviceName")
                .setInstanceId("instanceId")
                .setReplicaId("replicaId")
                .setVersion("0.1")
                .setConsumedInterfaces(List.of(new MockInterface()
                        .setInterfaceName("consumedInterface")
                        .setInterfaceVersion("0.1")
                        .setRequired(false)
                        .addBinding(new RestBinding()
                                .setServiceUrl("http://localhost:8080")))
                )
                .setProvidedInterfaces(List.of(new MockInterface()
                                .setInterfaceName("providedInterface")
                                .setInterfaceVersion("0.1")
                                .setRequired(true)
                                .addBinding(new RestBinding()
                                        .setServiceUrl("http://localhost:8080"))
                        )
                );
    }
}
