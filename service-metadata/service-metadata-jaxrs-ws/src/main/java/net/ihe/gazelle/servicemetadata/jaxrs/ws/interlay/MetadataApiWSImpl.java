package net.ihe.gazelle.servicemetadata.jaxrs.ws.interlay;

import jakarta.inject.Inject;
import jakarta.ws.rs.core.Response;
import net.ihe.gazelle.lang.UnexpectedInternalErrorException;
import net.ihe.gazelle.servicemetadata.api.application.MetadataServiceFactory;
import net.ihe.gazelle.servicemetadata.api.application.MetadataService;
import net.ihe.gazelle.servicemetadata.jaxrs.api.interlay.dto.ServiceDTO;
import net.ihe.gazelle.servicemetadata.jaxrs.api.interlay.ws.MetadataApiWS;
import org.slf4j.Logger;

public class MetadataApiWSImpl implements MetadataApiWS {


    private static final Logger logger = org.slf4j.LoggerFactory.getLogger(MetadataApiWSImpl.class);

    @Inject
    MetadataService metadataServiceProvider;


    @Override
    public Response getServiceMetadata() {
        try{
            return Response.ok(new ServiceDTO(getMetadataServiceFactory().getMetadataService().getMetadata())).build();
        }
        catch (UnexpectedInternalErrorException e){
            logger.error("Error while retrieving metadata service", e);
            return Response.serverError().build();
        }
    }


    protected MetadataServiceFactory getMetadataServiceFactory() {
        return () -> metadataServiceProvider;
    }
}
