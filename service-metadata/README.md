# Metadata Service

The main purpose of the Metadata Service project is to share identification information, versions
and available interfaces with the Gazelle Test Bed.

This library contain the service metadata API and provides some default implementations.

The default implementation is injected in the default implementation of the service metadata  REST API 
in `net.ihe.gazelle.servicemetadata.jaxrs.ws.interlay.MetadataApiWSImpl`.

All provided implementations could be override, see next sections.


## 1. Service Metadata API

This submodule contains the service metadata API `net.ihe.gazelle.servicemetadata.api.application.ServiceMetadata` 
and all metadata structures in `net.ihe.gazelle.servicemetadata.api.domain.structure.*`.

It provides also the structure validators & builders based on `framework/framework-model-validator`.

The API is used by the service metadata REST API and could be used by any other application.

### 1.1. Implementation

To implement the service metadata API, you have to implement the interface `net.ihe.gazelle.servicemetadata.api.application.ServiceMetadata`

```java
package org.example.interlay;

import net.ihe.gazelle.servicemetadata.api.domain.structure.Service;
import net.ihe.gazelle.servicemetadata.api.application.MetadataService;

public class ServiceMetadataImpl implements MetadataService {

    @Override
    public Service getMetadata() {
        return new Service()
                .setName("serviceName")
                .setVersion("1.0.0")
                .setInstanceId("instanceId")
                .setReplicaId("replicaId")
                ;
    }
}

```

### 1.2. Validation

To implement the service correctly and ensure the well construction of the metadata,
you have to use the provided builders.

The builders are based on `framework/framework-model-validator` and provide a fluent API to build the metadata.

```java
package org.example.interlay;

import net.ihe.gazelle.servicemetadata.api.domain.structure.Service;
import net.ihe.gazelle.servicemetadata.api.application.MetadataService;
import net.ihe.gazelle.servicemetadata.api.application.builder.ServiceBuilder;

import net.ihe.gazelle.framework.modelvalidator.domain.ValidatorBuilderFactory;
import net.ihe.gazelle.framework.modelvalidator.interlay.adapter.BValidatorBuilderFactory;


public class ServiceMetadataImpl implements MetadataService {

    private final ValidatorBuilderFactory validatorBuilderFactory;

    public ServiceMetadataProvider() {
        this.validatorBuilderFactory = new BValidatorBuilderFactory();
    }

    @Override
    public Service getMetadata() {
        return new ServiceBuilder(validatorBuilderFactory)
                .setName(name)
                .setVersion(version)
                .build();
    }
}

```

This leads to an automatic validation of the metadata structure.

## 2. Service Metadata REST API

This submodule contains the service metadata REST API `net.ihe.gazelle.servicemetadata.jaxrs.api.interlay.ws.MetadataApiWS`
annotated with JAX-RS annotations and openapi annotations to generate the swagger documentation later.

In addition, it provides the default implementation of the `ServiceMetadata` (from the previous API submodule)
based on the MicroProfile Config API in `net.ihe.gazelle.servicemetadata.jaxrs.api.interlay.config.ServiceMetadataProvider`.

This implementation will be later injected in the default implementation of the service metadata REST API.

> This implementation uses Microprofile Config API to retrieve the **name** and the **version** of the service. 
> This requires the file META-INF[or WEB-INF]/microprofile-config.properties on
the classpath with the following properties :

```properties
# Service name
gzl.service.name=serviceName
# Service version
gzl.service.version=1.0.0
```

Please refer to the [MicroProfile Config API](https://download.eclipse.org/microprofile/microprofile-config-3.0/microprofile-config-spec-3.0.html)
for more details about the configuration injection and the lookup order.

It provides also the required DTOs in `net.ihe.gazelle.servicemetadata.jaxrs.api.interlay.dto.*`.

### 2.1. Implementation with CDI

To implement the service metadata REST API, you have to implement the interface `net.ihe.gazelle.servicemetadata.jaxrs.api.interlay.ws.MetadataApiWS`

```java

package org.example.interlay;

import net.ihe.gazelle.servicemetadata.jaxrs.api.interlay.ws.MetadataApiWS;
import net.ihe.gazelle.servicemetadata.jaxrs.api.interlay.dto.ServiceMetadataDTO;

import jakarta.ws.rs.Path;
import javax.inject.Inject;

@Path("/metadata")
public class MetadataApiWSImpl implements MetadataApiWS {

    @Inject
    private MetadataService metadataService;

    @Override
    public ServiceMetadataDTO getMetadata() {
        return new ServiceMetadataDTO(metadataService.getMetadata());
    }
}

```

This example assumes that you have implemented the service metadata API in the previous section and 
expose it as a CDI bean.

### 2.2. Implementation native

If you don't want to use CDI, you can inject the service metadata API directly in the REST API implementation.

```java

package org.example.interlay;

import net.ihe.gazelle.servicemetadata.api.application.MetadataService;
import net.ihe.gazelle.servicemetadata.jaxrs.api.interlay.ws.MetadataApiWS;
import net.ihe.gazelle.servicemetadata.jaxrs.api.interlay.dto.ServiceMetadataDTO;

import jakarta.ws.rs.Path;

@Path("/metadata")
public class MetadataApiWSImpl implements MetadataApiWS {

    private final MetadataService metadataService;

    public MetadataApiWSImpl() {
        this.metadataService = new MetadataServiceImpl();
    }

    @Override
    public ServiceMetadataDTO getMetadata() {
        return new ServiceMetadataDTO(metadataService.getMetadata());
    }
}

```

### 3. Service Metadata REST WebService

This submodule contains the service metadata REST WebService `net.ihe.gazelle.servicemetadata.jaxrs.ws.interlay.MetadataApiWSImpl`,
a default implementation of the `MetadataApiWS`.

This implementation inject the found candidate of the `ServiceMetadata` in the CDI container.
By default, it will be the `ServiceMetadataProvider`. If more than one candidate is found,
CDI throws an exception.

To override the default ServiceMetadata implementation, you have to 
override the `ServiceMetadataFactory getServiceMetadataFactory()` method in the `MetadataApiWSImpl` class.

And you can use CDI Qualifiers to select the right implementation.

```java
package org.example.interlay;

import net.ihe.gazelle.servicemetadata.jaxrs.ws.interlay.MetadataApiWSImpl;
import net.ihe.gazelle.servicemetadata.api.application.MetadataService;

import org.example.interlay.qualifier.CustomQualifier;

public class MetadataApiWSImpl extends MetadataApiWSImpl {

    @CustomQualifier
    MetadataService validationMetadataService;

    @Override
    protected ServiceMetadataFactory getServiceMetadataFactory() {
        return () -> validationMetadataService;
    }
}

```

Or inject the right implementation directly in the REST WebService implementation.

```java
package org.example.interlay;

import net.ihe.gazelle.servicemetadata.jaxrs.ws.interlay.MetadataApiWSImpl;
import net.ihe.gazelle.servicemetadata.api.application.ServiceMetadata;

public class MetadataApiWSImpl extends MetadataApiWSImpl {

    MetadataService validationMetadataService = new MetadataServiceImpl();

    @Override
    protected MetadataServiceFactory getMetadataServiceFactory() {
        return () -> validationMetadataService;
    }
}

```

Even if the REST WS default implementation is provided, you need to create your own implementation to specify the right
path to the REST API.

```java
package org.example.interlay;

import net.ihe.gazelle.servicemetadata.jaxrs.ws.interlay.MetadataApiWSImpl;

import jakarta.ws.rs.Path;

@Path("/metadata")
public class MetadataApiWS extends MetadataApiWSImpl {
}

```

This is the default implementation of the REST WebService.